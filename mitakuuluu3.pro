TEMPLATE = subdirs

SUBDIRS = \
    android \
    libcurve25519 \
    libaxolotl \
    libwa \
    mitakuuluu \
    $${NULL}

OTHER_FILES = \
    $${NULL} \
    rpm/mitakuuluu3.spec \
    .qmake.conf
