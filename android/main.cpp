#include <QFile>
#include <QDir>
#include <stdio.h>
#include <sys/stat.h>
#include <QProcess>
#include <QFile>

#define WA_BASE "/data/data/com.whatsapp"
#define WA_PW WA_BASE"/files/pw"
#define WA_ME WA_BASE"/files/me"
#define WA_DB WA_BASE"/databases/msgstore.db"
#define WA_PREF WA_BASE"/shared_prefs/com.whatsapp_preferences.xml"

int main(int argc, char *argv[])
{
    setuid(0);

    if (argc > 1) {
        printf("arg: %s\n", argv[1]);
        if (strcmp(argv[1], "check") == 0) {
            if (QFile(WA_PW).exists() && QFile(WA_ME).exists() && QFile(WA_DB).exists()) {
                chmod(WA_PW, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                chmod(WA_ME, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                QString newDbPath("/home/nemo/.local/share/mitakuuluu3/mitakuuluu3/msgstore.db");
                QFile(WA_DB).copy(newDbPath);
                chmod(newDbPath.toUtf8().constData(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
                chmod(WA_PREF, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                return 0;
            }
            return 1;
        }
        else if (strcmp(argv[1], "remove_android") == 0) {
            if (QFile(WA_PW).exists()) {
                QFile(WA_PW).remove();
            }
            if (QFile(WA_ME).exists()) {
                QFile(WA_ME).remove();
            }

            return 0;
        }
        else if (strcmp(argv[1], "get_logs") == 0) {
            QFile log("/tmp/mitakuuluu3_journal.log");
            if (log.open(QFile::WriteOnly)) {
                log.resize(0);
                log.seek(0);

                QProcess journal;
                QStringList args;
                args << "_COMM=mitakuuluu3";
                args << "-a";
                if (argc > 2) {
                    args << QString("--since=%1").arg(argv[2]);
                }
                journal.start("/bin/journalctl", args);
                journal.waitForFinished(-1);
                if (journal.exitCode() == 0) {
                    log.write(journal.readAll());
                    QFile zipFile("/tmp/mitakuuluu3log.zip");
                    if (zipFile.exists()) {
                        zipFile.remove();
                    }

                    QProcess zip;
                    zip.start("/usr/bin/zip", QStringList() << zipFile.fileName() << log.fileName());
                    zip.waitForFinished(-1);
                    if (zip.exitCode() == 0) {
                        printf("log saved to: /tmp/mitakuuluu3log.zip\n");
                    }
                    else {
                        return 4;
                    }
                }
                else {
                    return 3;
                }
                log.close();
                log.remove();

                return 0;
            }
            return 2;
        }
    }

    return 5;
}


