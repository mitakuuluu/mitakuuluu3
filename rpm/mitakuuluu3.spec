Name:       mitakuuluu3

# >> macros
%define __provides_exclude_from ^%{_datadir}/.*$
%define __requires_exclude ^libwa-qt5.*|libaxolotl.*|libcurve25519.*|libprotobuf.*$
# << macros

%{!?qtc_qmake:%define qtc_qmake %qmake}
%{!?qtc_qmake5:%define qtc_qmake5 %qmake5}
%{!?qtc_make:%define qtc_make make}
%{?qtc_builddir:%define _builddir %qtc_builddir}
Summary:    Mitakuuluu
Version:    0.9.0
Release:    1
Group:      Qt/Qt
License:    WTFPL
URL:        https://bitbucket.org/mitakuuluu/mitakuuluu3
Source0:    %{name}-%{version}.tar.bz2
Requires:   sailfishsilica-qt5 >= 0.10.9
Requires:   zip
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Contacts)
BuildRequires:  pkgconfig(Qt5Sensors)
BuildRequires:  pkgconfig(Qt5Multimedia)
BuildRequires:  pkgconfig(Qt5XmlPatterns)
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2
BuildRequires:  pkgconfig(nemotransferengine-qt5)
BuildRequires:  pkgconfig(gstreamer-0.10)
BuildRequires:  pkgconfig(contactcache-qt5)
BuildRequires:  pkgconfig(qtcontacts-sqlite-qt5-extensions)
BuildRequires:  pkgconfig(keepalive)
BuildRequires:  pkgconfig(dconf)
BuildRequires:  pkgconfig(protobuf) = 2.6.1
BuildRequires:  protobuf-static = 2.6.1
BuildRequires:  pkgconfig(openssl)
BuildRequires:  pkgconfig(libsystemd-journal)
BuildRequires:  desktop-file-utils
BuildRequires:  qt5-qttools-linguist

%description
SailfishOS client to WA


%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%qtc_qmake5  \
    VERSION=%{version}

%qtc_make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%qmake5_install

# >> install post
# << install post

desktop-file-install --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
   %{buildroot}%{_datadir}/applications/*.desktop

%pre
if /sbin/pidof mitakuuluu3 > /dev/null; then
killall mitakuuluu3
fi

%preun
if /sbin/pidof mitakuuluu3 > /dev/null; then
killall mitakuuluu3
fi

%post
systemctl-user restart ngfd.service
systemctl restart mce.service

%files
%defattr(-,root,root,-)
%attr(4755, root, root) %{_bindir}/*
%config %{_sysconfdir}/mce/*.ini
%config %{_sysconfdir}/profiled/*.ini
%config %{_datadir}/ngfd/events.d/*.ini
%config %{_datadir}/lipstick/notificationcategories/*.conf
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/86x86/apps/%{name}.png
%{_datadir}/dbus-1/services/org.mitakuuluu.service
# >> files
# << files
