TARGET = mitakuuluu3

QT += dbus sql xmlpatterns

CONFIG += link_pkgconfig sailfishapp
PKGCONFIG += dconf keepalive openssl gstreamer-0.10 libsystemd-journal

countries.files = src/countries.tsv
countries.path  = /usr/share/mitakuuluu3/data/countries

dbus.files = dbus/org.mitakuuluu.service
dbus.path = /usr/share/dbus-1/services

images.files = images
images.path = /usr/share/mitakuuluu3

emoticons.files = emoji
emoticons.path = /usr/share/mitakuuluu3

notification.files = notification/org.mitakuuluu3.message.conf \
                     notification/org.mitakuuluu3.notification.conf \
                     notification/org.mitakuuluu3.private.conf \
                     notification/org.mitakuuluu3.group.conf \
                     notification/org.mitakuuluu3.media.conf
notification.path = /usr/share/lipstick/notificationcategories/

patterns.files = patterns/60-mitakuuluu_led.ini
patterns.path = /etc/mce

events.files = events/org_mitakuuluu_group.ini \
               events/org_mitakuuluu_media.ini \
               events/org_mitakuuluu_private.ini
events.path = /usr/share/ngfd/events.d/

profiled.files = profiled/60.org.mitakuuluu.ini
profiled.path = /etc/profiled

INSTALLS += countries dbus images emoticons notification patterns events profiled

system(qdbusxml2cpp -p src/platform/notificationmanagerproxy.h:src/platform/notificationmanagerproxy.cpp -c NotificationManagerProxy -i src/platform/notification.h src/platform/org.freedesktop.Notifications.xml)

SOURCES += \
    src/main.cpp \
    src/dbusmain.cpp \
    src/dconf/mdconf.cpp \
    src/dconf/mdconfagent.cpp \
    src/settings/appsettings.cpp \
    src/models/countriesmodel.cpp \
    src/platform/simparameters.cpp \
    src/settings/accountsettings.cpp \
    src/waclasses/mainconnection.cpp \
    src/waclasses/regtools.cpp \
    src/waclasses/registration.cpp \
    src/settings/serverproperties.cpp \
    src/threadworker/queryexecutor.cpp \
    src/threadworker/threadworker.cpp \
    src/models/contactsbasemodel.cpp \
    src/models/contactsfiltermodel.cpp \
    src/models/conversationfiltermodel.cpp \
    src/models/conversationbasemodel.cpp \
    src/platform/smslistener.cpp \
    src/waclasses/emoji.cpp \
    src/providers/mediapreviewprovider.cpp \
    src/providers/imagebytesprovider.cpp \
    src/providers/videothumbnailprovider.cpp \
    src/platform/notifier.cpp \
    src/platform/notification.cpp \
    src/platform/notificationmanagerproxy.cpp \
    src/platform/nativewindowhelper.cpp \
    src/models/messagesbasemodel.cpp \
    src/models/messagesfiltermodel.cpp

HEADERS = \
    src/dbusmain.h \
    src/constants.h \
    src/dconf/mdconf_p.h \
    src/dconf/mdconfagent.h \
    src/settings/appsettings.h \
    src/models/countriesmodel.h \
    src/platform/simparameters.h \
    src/settings/accountsettings.h \
    src/waclasses/mainconnection.h \
    src/waclasses/regtools.h \
    src/waclasses/registration.h \
    src/settings/serverproperties.h \
    src/threadworker/queryexecutor.h \
    src/threadworker/threadworker.h \
    src/models/contactsbasemodel.h \
    src/models/contactsfiltermodel.h \
    src/models/conversationfiltermodel.h \
    src/models/conversationbasemodel.h \
    src/platform/smslistener.h \
    src/waclasses/emoji.h \
    src/providers/mediapreviewprovider.h \
    src/providers/imagebytesprovider.h \
    src/providers/videothumbnailprovider.h \
    src/platform/notifier.h \
    src/platform/notification.h \
    src/platform/notificationmanagerproxy.h \
    src/platform/qpa_native.h \
    src/platform/nativewindowhelper.h \
    src/models/messagesbasemodel.h \
    src/models/messagesfiltermodel.h


DEFINES += APP_VERSION=\\\"$$VERSION\\\"

QMAKE_RPATHDIR += $$CURRENT_RPATH_DIR
LIBS += -L../libwa -lwa-qt5
LIBS += -L../libaxolotl -laxolotl
LIBS += -L../libcurve25519 -lcurve25519

OTHER_FILES += \
    mitakuuluu3.desktop \
    mitakuuluu3.png \
    qml/cover/CoverPage.qml \
    qml/components/Popup.qml \
    qml/main.qml \
    qml/js/global.js \
    qml/pages/SelectPhonebook.qml \
    qml/components/FastScroll.js \
    qml/components/FastScroll.qml \
    qml/components/AvatarHolder.qml \
    qml/pages/UserProfile.qml \
    qml/pages/GroupProfile.qml \
    qml/pages/Conversation.qml \
    qml/pages/Registration.qml \
    qml/pages/Chats.qml \
    qml/pages/Contacts.qml \
    qml/delegates/ClassicDelegate.qml \
    qml/delegates/ClassicText.qml \
    qml/components/ConversationHeader.qml \
    qml/pages/LeftMenu.qml \
    qml/components/MenuIconItem.qml \
    qml/delegates/ClassicInformation.qml \
    qml/components/Wallpaper.qml \
    qml/pages/Settings.qml \
    qml/components/Background.qml \
    qml/components/EmojiTextArea.qml \
    qml/delegates/ClassicImage.qml \
    qml/delegates/ClassicVideo.qml \
    qml/delegates/ClassicAudio.qml \
    qml/delegates/ClassicLocation.qml \
    qml/delegates/ClassicVCard.qml \
    qml/pages/Account.qml \
    src/platform/org.freedesktop.Notifications.xml \
    qml/overlay.qml \
    qml/pages/About.qml \
    qml/components/ToolbarButton.qml \
    qml/pages/AvatarPicker.qml \
    qml/components/AvatarToolbar.qml \
    qml/pages/SendLogs.qml

