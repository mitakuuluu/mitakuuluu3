import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import QtSensors 5.1
import QtFeedback 5.0
import "components"

Wallpaper {
    id: root

    property bool transposed: false//orientationSensor.angle % 180 != 0
    property int viewHeight: transposed ? Screen.width : Screen.height
    property int viewWidth: transposed ? Screen.height : Screen.width
    property int visibleHeight: Math.min(view.contentHeight, view.height)

    width: viewWidth
    height: visibleHeight + buttonsItem.height

    onWidthChanged: updateTouchRegion()
    onHeightChanged: updateTouchRegion()

    Component.onCompleted: {
        updateTouchRegion()
    }

    function updateTouchRegion() {
        nativeWindow.setTouchRegion(Qt.rect(0, 0, root.width, root.height))
    }

    Connections {
        target: MessagesBaseModel
        onLastMessageRemoved: {
            window.hide()
        }
    }

    SilicaListView {
        id: view
        width: root.width
        height: viewHeight - viewHeight / 3
        model: messagesModel
        delegate: delegateComponent

        header: Item { width: 1; height: Theme.paddingMedium }

        clip: true
        spacing: Theme.paddingMedium

        VerticalScrollDecorator {}
    }

    Item {
        id: buttonsItem
        y: visibleHeight
        width: parent.width
        height: Theme.itemSizeSmall
        Button {
            anchors {
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: parent.width / 4
            }
            text: qsTr("Close")
            onClicked: {
                window.hide()
            }
        }
        Button {
            anchors {
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: -parent.width / 4
            }
            text: qsTr("Open app")
            onClicked: {
                main.showUI()
                window.hide()
            }
        }
    }

    Component {
        id: delegateComponent
        BackgroundItem {
            id: delegate
            width: ListView.view.width
            height: content.height

            property bool leftActive: x < (-width / 2)
            property bool rightActive: x > (width / 2)

            onLeftActiveChanged: themeEffect.play()
            onRightActiveChanged: themeEffect.play()

            drag.target: delegate
            drag.axis: Drag.XAxis
            drag.minimumX:  -width
            drag.maximumX: width
            drag.onActiveChanged: {
                if (!drag.active) {
                    if (leftActive) {
                        main.showUIAsync(model.jid)
                    }
                    else if (rightActive) {
                        MessagesBaseModel.clearMessagesForJid(model.jid)
                    }
                    else {
                        behaviour.enabled = true
                        x = 0
                    }
                }
                else {
                    behaviour.enabled = false
                }
            }

            Behavior on x {
                id: behaviour
                NumberAnimation { duration: 300 }
            }

            Row {
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.right
                }
                height: Theme.itemSizeSmall

                Image {
                    anchors.verticalCenter: parent.verticalCenter
                    source: "image://theme/icon-m-back" + (delegate.leftActive ? ("?" + Theme.highlightColor) : "")
                }

                Label {
                    text: qsTr("Slide left to open")
                    color: leftActive ? Theme.highlightColor : Theme.primaryColor
                }
            }

            Row {
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.left
                }
                height: Theme.itemSizeSmall

                Label {
                    text: qsTr("Slide right to delete")
                    color: (delegate.x > (delegate.width / 2)) ? Theme.highlightColor : Theme.primaryColor
                }

                Image {
                    anchors.verticalCenter: parent.verticalCenter
                    source: "image://theme/icon-m-forward" + (delegate.rightActive ? ("?" + Theme.highlightColor) : "")
                }
            }

            Column {
                id: content
                width: parent.width
                spacing: Theme.paddingMedium

                Item {
                    width: parent.width
                    height: Theme.itemSizeExtraSmall

                    Image {
                        id: sectionAvatar
                        x: Theme.paddingLarge
                        height: parent.height
                        width: height
                        sourceSize.height: height
                        sourceSize.width: width
                        source: model.avatar //ContactsBaseModel.getAvatarByJid(model.jid)
                    }

                    Label {
                        id: sectionNickname
                        anchors {
                            left: sectionAvatar.right
                            right: parent.right
                            margins: Theme.paddingMedium
                            verticalCenter: parent.verticalCenter
                        }
                        truncationMode: TruncationMode.Fade
                        textFormat: Text.RichText
                        text: Emoji.waTextToHtml(model.nickname) //ContactsBaseModel.getNicknameByJid(model.jid)
                    }
                }

                Repeater {
                    width: parent.width
                    model: messages.length
                    delegate: Loader {
                        width: content.width
                        property int sidePadding: Theme.itemSizeLarge + Theme.paddingLarge
                        property int lowPadding: Theme.paddingLarge
                        property int cornerPadding: Math.sqrt(lowPadding * lowPadding + lowPadding * lowPadding)
                        //property color backgroundColor: "#80808080"
                        property color backgroundColor: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                        property var model: messages[index]
                        enabled: false
                        source: Qt.resolvedUrl("delegates/ClassicDelegate.qml")
                    }
                }
            }
        }
    }

    MessagesFilterModel {
        id: messagesModel
    }

    ThemeEffect {
        id: themeEffect
        effect: ThemeEffect.DragCrossBoundary
    }

    OrientationSensor {
        id: orientationSensor
        active: true
        property int angle: reading.orientation ? _getOrientation(reading.orientation) : 0
        function _getOrientation(value) {
            switch (value) {
                case 2:
                    return 180
                case 3:
                    return -90
                case 4:
                    return 90
                default:
                    return 0
            }
        }
    }
}
