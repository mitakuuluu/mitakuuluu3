import QtQuick 2.1
import Sailfish.Silica 1.0

Label {
    id: messageText
    height: implicitHeight
    wrapMode: Text.Wrap

    font.pixelSize: Theme.fontSizeMedium
    horizontalAlignment: Qt.AlignHCenter
    verticalAlignment: Qt.AlignBottom

    property int activeWidth: paintedWidth

    text: model.data
}
