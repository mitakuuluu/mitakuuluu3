import QtQuick 2.1
import Sailfish.Silica 1.0
import "../js/global.js" as Utilities

Item {
    property int activeWidth: width
    height: locationContent.height

    function getText(caption, url) {
        var result = caption
        var address = ""
        if (result.indexOf("\n") > 0) {
            address = result.split("\n")[1]
            result = result.split("\n")[0]
        }

        if (url != "undefined" && url.length > 0) {
            result = "<a href=\"" + url + "\">" + result + "</a>"
        }
        if (address.length > 0) {
            result += "<br>" + address
        }

        return result
    }

    Column {
        id: locationContent
        width: parent.width

        Image {
            width: parent.width
            height: Theme.itemSizeHuge
            source: Utilities.locationPreview(width, height, model.params.latitude, model.params.longitude, 15)
            cache: true

            Image {
                anchors.centerIn: parent
                source: "../../images/location-marker.png"
                width: Theme.iconSizeSmall
                fillMode: Image.PreserveAspectFit
                cache: true
                layer.effect: ShaderEffect {
                    property color color: Theme.highlightColor

                    fragmentShader: "
                        varying mediump vec2 qt_TexCoord0;
                        uniform highp float qt_Opacity;
                        uniform lowp sampler2D source;
                        uniform highp vec4 color;
                        void main() {
                            highp vec4 pixelColor = texture2D(source, qt_TexCoord0);
                            gl_FragColor = vec4(mix(pixelColor.rgb/max(pixelColor.a, 0.00390625), color.rgb/max(color.a, 0.00390625), color.a) * pixelColor.a, pixelColor.a) * qt_Opacity;
                        }
                    "
                }
                layer.enabled: true
                layer.samplerName: "source"
            }
        }

        Label {
            width: parent.width
            visible: "name" in model.params
            text: visible ? getText(model.params.name, model.params.url) : ""
            textFormat: Text.StyledText
            linkColor: Theme.highlightColor
            color: highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeSmall
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}
