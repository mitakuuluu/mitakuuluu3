import QtQuick 2.1
import Sailfish.Silica 1.0
import Sailfish.TextLinking 1.0

Item {
    id: root
    height: messageText.height
    property alias text: messageText.text
    property alias color: messageText.color
    property int activeWidth: messageText.paintedWidth

    Label {
        id: messageText

        anchors {
            left: inbound ? undefined: parent.left
            right: inbound ? parent.right : undefined
        }

        wrapMode: Text.NoWrap
        textFormat: Text.RichText

        font.pixelSize: root.inbound ? Theme.fontSizeMedium : Theme.fontSizeSmall
        horizontalAlignment: inbound ? Qt.AlignRight : Qt.AlignLeft

        text: Emoji.waTextToHtml(model.data, 32, true, Theme.highlightColor)
        color: highlighted ? Theme.highlightColor : Theme.primaryColor

        onLinkActivated: {
            handler.handleLink(link)
        }

        LinkHandler {
            id: handler
        }

        Component.onCompleted: {
            if (paintedWidth > root.width) {
                messageText.wrapMode = Text.WrapAtWordBoundaryOrAnywhere
                messageText.width = root.width + 1
            }
        }
    }
}

