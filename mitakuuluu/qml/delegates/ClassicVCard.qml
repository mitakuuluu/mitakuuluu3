import QtQuick 2.1
import Sailfish.Silica 1.0
import QtMultimedia 5.0

Item {
    id: contactDelegate
    property int activeWidth: width
    height: contactItem.height

    Row {
        id: contactItem
        height: Math.max(contactImage.height, contactName.height)
        spacing: Theme.paddingMedium

        Image {
            id: contactImage
            anchors.verticalCenter: parent.verticalCenter
            width: Theme.itemSizeMedium
            height: Theme.itemSizeMedium
            source: "image://theme/icon-m-service-generic" + (highlighted ? ("?" + Theme.highlightColor) : "")
            cache: true
        }

        Label {
            id: contactName
            anchors {
                top: contactName.height > contactImage.height ? parent.top : undefined
                verticalCenter: contactName.height <= contactImage.height ? parent.verticalCenter : undefined
            }
            width: contactDelegate.width - contactImage.width - parent.spacing
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            text: model.params.name
            color: highlighted ? Theme.highlightColor : Theme.primaryColor
        }
    }
}
