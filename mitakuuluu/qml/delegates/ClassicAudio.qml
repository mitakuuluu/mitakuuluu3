import QtQuick 2.1
import Sailfish.Silica 1.0
import QtMultimedia 5.0

Item {
    id: playerDelegate
    property int activeWidth: width
    height: Theme.itemSizeSmall

    Audio {
        id: player
        onDurationChanged: {
            playerSeek.maximumValue = duration
        }
        onPositionChanged: {
            playerSeek.value = position
        }
        onStatusChanged: {
            if (status == Audio.EndOfMedia)
                playerSeek.value = player.duration
        }
    }

    Row {
        id: playerItem
        height: parent.height

        IconButton {
            id: playButton
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "local" in model.params ? (player.playbackState == Audio.PlayingState ? "image://theme/icon-m-pause"
                                                                                               : "image://theme/icon-m-play")
                                                 : ((model.progress > 0 && model.progress < 1) ? "image://theme/icon-m-clear"
                                                                                               : "image://theme/icon-m-down")
            icon.height: Theme.iconSizeMedium
            icon.width: Theme.iconSizeMedium
            onClicked: {
                if ("local" in model.params)
                {
                    if (player.status == Audio.NoMedia)
                        player.source = model.params.local
                    if (player.playbackState == Audio.PlayingState)
                        player.pause()
                    else
                        player.play()
                }
                else if (model.author !== connection.myJid) {
                    connection.downloadMedia(jid, model.params.url, model.params.file, model.msgid)
                }
                else {

                }
            }
        }

        Slider {
            id: playerSeek
            property bool ready: model.progress == 0 || model.progress == 1
            anchors.verticalCenter: parent.verticalCenter
            width: playerDelegate.width - playButton.width
            minimumValue: 0
            maximumValue: 100
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            stepSize: 1
            value: ready ? player.position : (model.progress * 100)
            enabled: "local" in model.params
            onReleased: player.seek(value)

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                    bottom: parent.bottom
                }
                text: playerSeek.ready ? Format.formatDuration(player.position / 1000, Format.DurationShort) : (parseInt(model.progress * 100) + "%")
                color: highlighted ? Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
            }

            Label {
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                    bottom: parent.bottom
                }
                text: Format.formatDuration(model.params.seconds, Format.DurationShort)
                color: highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
            }
        }
    }
}
