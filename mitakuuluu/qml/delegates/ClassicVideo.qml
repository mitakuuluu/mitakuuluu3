import QtQuick 2.1
import Sailfish.Silica 1.0
import org.nemomobile.thumbnailer 1.0

Item {
    property int activeWidth: width
    height: delegateContent.height

    Column {
        id: delegateContent
        width: parent.width

        Image {
            id: img
            width: parent.width
            height: model.params.height * width / model.params.width
            fillMode: Image.PreserveAspectCrop
            smooth: true
            cache: true
            source: {
                if ("local" in model.params) {
                    return "image://thumbnail/" + model.params.local
                }
                else {
                    return "image://base64/" + model.data
                    //return "data:" + model.params.mimetype + ";base64," + model.data
                }
            }
            sourceSize.width: width
            sourceSize.height: height

            Rectangle {
                anchors {
                    left: parent.left
                    top: parent.top
                    margins: Theme.paddingSmall
                }
                width: progressLabel.width + Theme.paddingMedium
                height: progressLabel.height + Theme.paddingMedium
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                visible: !("local" in model.params) && model.progress > 0

                Label {
                    id: progressLabel
                    x: Theme.paddingSmall
                    y: Theme.paddingSmall
                    text: parseInt(model.progress * 100) + "%"
                    color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    font.pixelSize: Theme.fontSizeSmall
                }
            }

            Rectangle {
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                    margins: Theme.paddingSmall
                }
                width: durationLabel.width + Theme.paddingMedium
                height: durationLabel.height + Theme.paddingMedium
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)

                Label {
                    id: durationLabel
                    x: Theme.paddingSmall
                    y: Theme.paddingSmall
                    text: Format.formatDuration(model.params.seconds, Format.DurationShort)
                    color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    font.pixelSize: Theme.fontSizeSmall
                }
            }

            Image {
                anchors.centerIn: parent
                source: "image://theme/icon-m-play" + (highlighted ? ("?" + Theme.highlightColor) : "")
                visible: "local" in model.params
            }

            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                width: model.progress == 0 ? parent.width : (parent.width * model.progress)
                height: model.progress == 0 ? (icon.height + Theme.paddingMedium) : parent.height
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                visible: !("local" in model.params)
                Row {
                    anchors.centerIn: parent
                    height: icon.height
                    visible: model.progress == 0
                    Image {
                        id: icon
                        source: "image://theme/icon-m-download" + (highlighted ? ("?" + Theme.highlightColor) : "")
                    }
                    Label {
                        text: model.progress > 1 ? qsTr("Download again") : qsTr("Download")
                        color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    }
                }
            }
        }

        Label {
            visible: "caption" in model.params
            text: Emoji.waTextToHtml(model.params.caption, 32)
            color: highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeSmall
        }
    }
}
