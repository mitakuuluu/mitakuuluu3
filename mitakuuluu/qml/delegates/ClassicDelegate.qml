import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import Sailfish.TextLinking 1.0

ListItem {
    id: message

    //contentHeight: content.height
    contentHeight: content.height// + Theme.paddingSmall * 2

    property var stamp: new Date(model.timestamp * 1000)

    property bool isGroup: model.jid.indexOf("@g.us") > 0
    property bool isBroadcast: model.jid.indexOf("@broadcast") > 0
    property bool isPersonal: model.jid.indexOf("@s.whatsapp.net") > 0

    property int deliveredCount: Object.keys(model.deliveryreceipts).length
    property int readCount: Object.keys(model.readreceipts).length
    property bool delivered: isPersonal ? (deliveredCount > 0) : (deliveredCount >= model.recepients)
    property bool received: isPersonal ? (readCount > 0) : (readCount >= model.recepients)

    property bool inbound: model.author !== connection.myJid
    property bool information: model.msgtype == ConversationBaseModel.InformationMessage

    property bool loaderReady: messageLoader.status == Loader.Ready
    property int messageWidth: loaderReady ? Math.max(nickLabel.width, messageLoader.item.activeWidth, statusRow.width) : 0

    //property int sidePadding: view.sidePadding
    //property int lowPadding: view.lowPadding
    //property int cornerPadding: view.cornerPadding

    onPressAndHold: {
        //console.log(ListView.section, ListView.nextSection)
        //console.log(model.index)
        //console.log(model.msgtype)
        //console.log(model.data)
        //console.log(Emoji.waTextToHtml(model.data))
        //console.log(messageLoader.item.activeWidth)
    }

    onClicked: {
        switch (model.msgtype) {
        case ConversationBaseModel.TextMessage:
            break;
        case ConversationBaseModel.ImageMessage:
        case ConversationBaseModel.VideoMessage:
            if ("local" in model.params) {
                Qt.openUrlExternally(model.params.local)
            }
            else {
                connection.downloadMedia(jid, model.params.url, model.params.file, model.msgid)
            }
            break;
        case ConversationBaseModel.AudioMessage:
            break;
        case ConversationBaseModel.LocationMessage:
            Qt.openUrlExternally("geo:" + model.params.latitude + "," + model.params.longitude)
            break;
        case ConversationBaseModel.VCardMessage:
            connection.openVCardData(model.name, model.data)
            break;
        default:
            break;
        }
    }

    Image {
        id: avaAuthor
        source: visible ? ContactsBaseModel.getAvatarByJid(model.author) : ""
        visible: loaderReady && !information && isGroup && inbound
        height: Math.min(mainRect.height, Theme.itemSizeSmall)
        width: height
        anchors {
            top: mainRect.top
            right: mainRect.left
        }
    }

    Rectangle {
        id: mainRect
        x: information ? ((message.width - messageLoader.item.activeWidth - Theme.paddingMedium) / 2)
                       : (inbound ? (sidePadding + (messageLoader.width - messageWidth))
                                  : (lowPadding))
           - Theme.paddingSmall
        height: content.height
        width: messageWidth + Theme.paddingMedium
        color: backgroundColor
        visible: loaderReady
    }

    Item {
        clip: true
        anchors {
            top: mainRect.top
            left: inbound ? mainRect.right : parent.left
            right: inbound ? parent.right : mainRect.left
        }
        width: lowPadding
        height: lowPadding
        visible: !information && loaderReady

        Rectangle {
            anchors {
                verticalCenter: parent.top
                horizontalCenter: parent.left
                horizontalCenterOffset: inbound ? -Theme.paddingSmall : Theme.paddingLarge
            }
            property int side: lowPadding - Theme.paddingSmall
            width: cornerPadding
            height: width
            rotation: 45
            color: backgroundColor
        }
    }

    Column {
        id: content
        y: Theme.paddingSmall
        width: parent.width

        Label {
            id: nickLabel
            text: Emoji.waTextToHtml(ContactsBaseModel.getNicknameByJid(model.author))
            textFormat: Text.RichText
            visible: loaderReady && !information && isGroup && inbound
            width: visible ? paintedWidth :  0
            x: content.width - width - lowPadding
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.secondaryColor
            wrapMode: Text.NoWrap
        }

        Loader {
            id: messageLoader
            active: source !== ""
            source: {
                switch (model.msgtype) {
                case ConversationBaseModel.TextMessage:
                    return "ClassicText.qml"
                case ConversationBaseModel.ImageMessage:
                    return "ClassicImage.qml"
                case ConversationBaseModel.VideoMessage:
                    return "ClassicVideo.qml"
                case ConversationBaseModel.AudioMessage:
                    return "ClassicAudio.qml"
                case ConversationBaseModel.LocationMessage:
                    return "ClassicLocation.qml"
                case ConversationBaseModel.VCardMessage:
                    return "ClassicVCard.qml"
                default:
                    return "ClassicInformation.qml"
                }
            }
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: (inbound && !information) ? sidePadding : lowPadding
                rightMargin: (!inbound && !information) ? sidePadding : lowPadding
            }
        }

        Row {
            id: statusRow
            x: visible ? (information ? ((content.width - messageLoader.item.activeWidth) / 2)
                                      : (inbound ? (content.width - width - lowPadding)
                                                 : (Math.max(messageLoader.item.activeWidth - width + lowPadding, lowPadding))))
                       : 0
            height: timestampLabel.height
            visible: loaderReady

            Label {
                id: timestampLabel
                color: highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall

                text: Format.formatDate(stamp, Format.TimeValueTwentyFourHours)
            }

            Item {
                id: deliveryItem
                anchors.verticalCenter: parent.verticalCenter
                width: visible ? (msgSentTick.width * 1.5) : 0
                height: msgSentTick.height
                visible: model.author === connection.myJid

                GlassItem {
                    id: msgSentTick
                    width: Theme.fontSizeSmall
                    height: Theme.fontSizeSmall
                    anchors.right: parent.right
                    falloffRadius: 0.3
                    radius: 0.4
                    color: (model.status === 0) ? "#80ff0000" : (received ? "#8000ffff" : (delivered ? "#8000ff00" : "#80ffff00"))
                }

                GlassItem {
                    id: msgDeliveredTick
                    width: Theme.fontSizeSmall
                    height: Theme.fontSizeSmall
                    anchors.left: parent.left
                    falloffRadius: 0.3
                    radius: 0.4
                    color: received ? "#8000ffff" : (delivered ? "#8000ff00" : "#80ffff00")
                    visible: delivered || received
                }
            }
        }

        Item { width: 1; height: Theme.paddingSmall }
    }
}
