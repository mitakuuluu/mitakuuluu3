.pragma library

function test() {

}

function findFlickable(item) {
    var parentItem = item.parent
    while (parentItem) {
        if (parentItem.maximumFlickVelocity && !parentItem.hasOwnProperty('__silica_hidden_flickable')) {
            return parentItem
        }
        parentItem = parentItem.parent
    }
    return null
}

function findListView(item) {
    var parentItem = item.parent
    while (parentItem) {
        if (parentItem.maximumFlickVelocity && parentItem.hasOwnProperty("model") && !parentItem.hasOwnProperty('__silica_hidden_flickable')) {
            return parentItem
        }
        parentItem = parentItem.parent
    }
    return null
}

function findPage(item) {
    var parentItem = item.parent
    while (parentItem) {
        if (parentItem.hasOwnProperty('__silica_page')) {
            return parentItem
        }
        parentItem = parentItem.parent
    }
    return null
}

function locationPreview(w, h, lat, lon, z, source) {
    if (!source || source === undefined || typeof(source) === "undefined")
        source = "here"

    if (source === "here") {
        return "https://maps.nlp.nokia.com/mia/1.6/mapview?app_id=ZXpeEEIbbZQHDlyl5vEn&app_code=GQvKkpzHomJpzKu-hGxFSQ&nord&f=0&poithm=1&poilbl=0&ctr="
                + lat
                + ","
                + lon
                + "&w=" + w
                + "&h=" + h
                //+ "&poix0="
                //+ lat
                //+ ","
                //+ lon
                //+ ";red;white;20;.;"
                + "&z=" + z
    }
    else if (source === "osm") {
        return "https://coderus.openrepos.net/staticmaplite/staticmap.php?maptype=mapnik&center="
                + lat
                + ","
                + lon
                + "&size=" + w
                + "x" + h
                //+ "&markers="
                //+ lat
                //+ ","
                //+ lon
                //+ ",ol-marker"
                + "&zoom=" + z
    }
    else if (source === "google") {
        return "http://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&sensor=false&"
                + "&size=" + w
                + "x" + h
                //+ "&markers=color:red|label:.|"
                //+ lat
                //+ ","
                //+ lon
                + "&center="
                + lat
                + ","
                + lon
                + "&zoom=" + z
    }
    else if (source === "nokia") {
        return "http://m.nok.it/?nord&f=0&poithm=1&poilbl=0&ctr="
                + lat
                + ","
                + lon
                + "&w=" + w
                + "&h=" + h
                //+ "&poix0="
                //+ lat
                //+ ","
                //+ lon
                //+ ";red;white;20;.;"
                + "&z=" + z
    }
    else if (source === "bing") {
        return "http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/"
                + lat
                + ","
                + lon
                + "/"
                + z
                + "?mapSize=" + w
                + "," + h
                + "&key=AvkH1TAJ9k4dkzOELMutZbk_t3L4ImPPW5LXDvw16XNRd5U36a018XJo2Z1jsPbW"
    }
    else if (source === "mapquest") {
        return "http://www.mapquestapi.com/staticmap/v4/getmap?key=Fmjtd%7Cluur2q0y2q%2Cbw%3Do5-9abn5f"
                + "&center="+ lat
                + "," + lon
                + "&zoom=" + z
                + "&size=" + w
                + "," + h
                + "&type=map&imagetype=png"
    }
    else if (source === "yandexuser") {
        return "http://static-maps.yandex.ru/1.x/"
                + "?ll=" + lon
                + "," + lat
                + "&z=" + z
                + "&l=pmap&size=" + Math.min(w, 450)
                + "," + Math.min(h, 450)
    }
    else if (source === "yandex") {
        return "http://static-maps.yandex.ru/1.x/"
                + "?ll=" + lon
                + "," + lat
                + "&z=" + z
                + "&l=map&size=" + Math.min(w, 450)
                + "," + Math.min(h, 450)
    }
    else if (source === "2gis") {
        return "http://static.maps.api.2gis.ru/1.0"
                + "?center=" + lon
                + "," + lat
                + "&zoom=" + z
                + "&size=" + w
                + "," + h
    }
}
