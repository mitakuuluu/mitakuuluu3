import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "../components"

Page {
    id: page
    objectName: "contactsPage"

    property string searchPattern: ""

    onStatusChanged: {
        if (status == PageStatus.Active) {
            var firstUseCounter = appSettings.value("firstUseContactsCounter", 0)
            if (firstUseCounter < 3) {
                console.log("start hint")
                touchInteractionHint.start()
                appSettings.setValue("firstUseContactsCounter", firstUseCounter + 1)
            }
        }
    }

    SilicaListView {
        id: view
        anchors.fill: page
        PullDownMenu {
            visible: connection.connectionStatus == Connection.LoggedIn
            MenuItem {
                text: qsTr("Add contacts")
                onClicked: {
                    var selectPhonebook = pageStack.push(Qt.resolvedUrl("SelectPhonebook.qml"))
                    selectPhonebook.accepted.connect(function() { connection.syncContacts(selectPhonebook.selectedContacts) })
                    selectPhonebook.syncAll.connect(function() { connection.syncAllPhonebook() })
                }
            }
        }
        header: PageHeader {
            title: qsTr("Contacts")
        }
        model: contactsModel
        delegate: contactsDelegate
        ViewPlaceholder {
            enabled: view.count == 0
            text: connection.connectionStatus == Connection.LoggedIn
                  ? qsTr("No contacts. Use Add contacts from pulldown menu to sync contacts.")
                  : qsTr("No contacts. Connect to server using left menu before adding new contacts.")
        }
        VerticalScrollDecorator {}
    }

    InteractionHintLabel {
        text: qsTr("Swipe right to access menu")
        anchors.bottom: parent.bottom
        opacity: touchInteractionHint.running ? 1.0 : 0.0
        Behavior on opacity { FadeAnimation { duration: 1000 } }
    }

    TouchInteractionHint {
        id: touchInteractionHint
        direction: TouchInteraction.Right
        anchors.verticalCenter: parent.verticalCenter
        loops: 6
    }

    Component {
        id: contactsDelegate
        ListItem {
            id: item
            width: ListView.view.width
            contentHeight: Theme.itemSizeMedium
            ListView.onRemove: animateRemoval(item)
            menu: contextMenu
            property bool muted: false
            property bool secure: false
            property bool isGroup: model.jid.indexOf("@g.us") > 0
            property bool isBroadcast: model.jid.indexOf("@broadcast") > 0
            property bool isPersonal: model.jid.indexOf("@s.whatsapp.net") > 0

            function removeContact() {
                banner.na()
                return
                remorseAction(qsTr("Delete", "Delete contact remorse action text"),
                function() {
                    ContactsBaseModel.deleteContact(model.jid)
                })
            }

            Rectangle {
                id: presence
                height: ava.height
                anchors.left: parent.left
                anchors.right: ava.left
                anchors.verticalCenter: ava.verticalCenter
                color: model.blocked ? Theme.rgba("red", 0.6) : (connection.connectionStatus === Connection.LoggedIn ? (model.available ? Theme.rgba(Theme.highlightColor, 0.6) : "transparent") : "transparent")
                border.width: model.blocked ? 1 : 0
                border.color: (connection.connectionStatus === Connection.LoggedIn && model.blocked) ? Theme.rgba(Theme.highlightColor, 0.6) : "transparent"
                smooth: true
            }

            AvatarHolder {
                id: ava
                source: model.avatar.length > 0 ? model.avatar[0] : model.phoneavatar
                emptySource: {
                    if (item.isPersonal) {
                        return "../../images/avatar-empty.png"
                    }
                    else if (item.isGroup) {
                        return "../../images/avatar-empty-group.png"
                    }
                    else if (item.isBroadcast) {
                        return "../../images/avatar-broadcast.png"
                    }
                    else {
                        return ""
                    }
                }
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.top: parent.top
                anchors.topMargin: Theme.paddingSmall / 2
                width: Theme.iconSizeLarge
                height: Theme.iconSizeLarge

                /*Rectangle {
                    id: unreadCount
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: model.unread > 0
                    anchors.right: parent.right
                    anchors.top: parent.top

                    Label {
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeExtraSmall
                        text: model.unread
                        color: Theme.primaryColor
                    }
                }

                Rectangle {
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: item.muted
                    anchors.left: parent.left
                    anchors.top: parent.top

                    Image {
                        source: "image://theme/icon-m-speaker-mute"
                        smooth: true
                        width: Theme.iconSizeSmall
                        height: Theme.iconSizeSmall
                        anchors.centerIn: parent
                    }
                }

                Rectangle {
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: item.secure
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom

                    Image {
                        source: "image://theme/icon-s-secure"
                        smooth: true
                        width: Theme.iconSizeSmall
                        height: Theme.iconSizeSmall
                        anchors.centerIn: parent
                    }
                }*/
            }

            Column {
                id: dataColumn

                anchors {
                    left: ava.right
                    leftMargin: Theme.paddingMedium
                    verticalCenter: ava.verticalCenter
                    right: parent.right
                    rightMargin: Theme.paddingSmall
                }
                clip: true
                spacing: Theme.paddingSmall

                Label {
                    id: nickname
                    font.pixelSize: Theme.fontSizeLarge
                    width: parent.width
                    text: Theme.highlightText(Emoji.waTextToHtml(model.nickname), searchPattern, Theme.highlightColor)
                    wrapMode: Text.NoWrap
                    elide: Text.ElideRight
                    color: item.highlighted ? Theme.highlightColor : Theme.primaryColor
                    textFormat: Text.StyledText
                }

                Row {
                    height: status.height
                    spacing: Theme.paddingLarge

                    Label {
                        id: status
                        width: dataColumn.width - parent.spacing - unreadCount.width - Theme.paddingLarge
                        text: model.jid.indexOf("-") > 0 ? qsTr("Group chat", "Contacts group page text in status message line")
                                                         : Emoji.waTextToHtml(model.message)
                        wrapMode: Text.NoWrap
                        elide: Text.ElideRight
                        color: item.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        textFormat: Text.StyledText
                        font.pixelSize: Theme.fontSizeExtraSmall
                    }

                    Rectangle {
                        id: unreadCount
                        anchors.verticalCenter: parent.verticalCenter
                        width: visible ? Theme.iconSizeSmall : 0
                        height: Theme.iconSizeSmall
                        color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                        visible: model.unread > 0
                        smooth: true

                        Label {
                            anchors.centerIn: parent
                            font.pixelSize: Theme.fontSizeExtraSmall
                            text: model.unread
                            color: Theme.primaryColor
                        }
                    }
                }
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Conversation.qml"), {"jid": model.jid})
            }

            Component {
                id: contextMenu
                ContextMenu {
                    MenuItem {
                        text: qsTr("Profile", "Contact context menu profile item")
                        onClicked: {
                            if (model.jid.indexOf("-") > 0) {
                                pageStack.push(Qt.resolvedUrl("GroupProfile.qml"), {"jid": model.jid})
                            }
                            else {
                                if (model.jid === connection.myJid) {
                                    pageStack.push(Qt.resolvedUrl("Account.qml"))
                                }
                                else {
                                    pageStack.push(Qt.resolvedUrl("UserProfile.qml"), {"jid": model.jid})
                                }
                            }
                        }
                    }

                    MenuItem {
                        text: qsTr("Rename", "Contact context menu profile item")
                        visible: model.jid.indexOf("-") < 0 && model.jid !== connection.myJid
                        onClicked: {
                            banner.na()
                            return
                            pageStack.push(Qt.resolvedUrl("RenameContact.qml"), {"jid": model.jid})
                        }
                    }

                    MenuItem {
                        text: qsTr("Delete", "Contact context menu delete contact item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        onClicked: {
                            banner.na()
                            return
                            removeContact()
                        }
                    }

                    /*MenuItem {
                        text: model.jid.indexOf("-") > 0
                        //% "Contact context menu contact mute item"
                                ? (model.blocked ? qsTr("Unmute")
                        //% "Contact context menu contact unmute item"
                                                 : qsTr("Mute"))
                        //% "Contact context menu contact block item"
                                : (model.blocked ? qsTr("Unblock")
                        //% "Contact context menu contact unblock item"
                                                 : qsTr("Block"))
                        enabled: Mitakuuluu.connectionStatus === Mitakuuluu.LoggedIn
                        onClicked: {
                            if (model.jid.indexOf("-") > 0)
                                Mitakuuluu.muteOrUnmuteGroup(model.jid)
                            else
                                Mitakuuluu.blockOrUnblockContact(model.jid)
                        }
                    }*/
                }
            }
        }
    }

    ContactsFilterModel {
        id: contactsModel
        showActive: false
        hideGroups: true
    }
}
