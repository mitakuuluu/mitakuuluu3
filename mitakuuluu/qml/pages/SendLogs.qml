import QtQuick 2.1
import Sailfish.Silica 1.0
import Sailfish.Email 1.1
import org.mitakuuluu3 1.0

EmailComposerPage {
    id: sharePage
    objectName: "sendLogs"

    emailTo: "osfinitiative@gmail.com"
    emailSubject: "Mitakuuluu v" + Qt.application.version + " bug"
    emailBody: "Please enter bug description here. In english. Step-by-step:\n\n1. Started Mitakuuluu at {time}\n2. Did something at {time}\n3. Expected some behaviour\n4. Got wrong result at {time}\n\nMessages with this line will be deleted without checking."

    Component.onCompleted: {
        if (connection.compressLogs()) {
            attachmentsModel.append({"url": 'file:///tmp/mitakuuluu3log.zip', "title": "mitakuuluu3log.zip", "mimeType": "application/x-zip-compressed"})
        }
        else {
            emailBody = "Error reading application logs. Please report to developer."
        }
    }
}
