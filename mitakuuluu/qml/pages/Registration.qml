import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0

Page {
    id: page
    objectName: "registrationPage"
    property string method: "sms"

    Component.onCompleted: {
        connection.checkImports()
    }

    function doRegister(value) {
        if (busy.running) {
            console.warn("Calling doRegister while operation in progress")
        }
        else if (phoneField.text.length === 0) {
            phoneField.forceActiveFocus()
        }
        else if (pushname.text.length === 0) {
            pushname.forceActiveFocus()
        }
        else if (countrySelect.currentIndex >= 0) {
            method = value
            registration.codeRequest(countrySelect.currentItem.cc, phoneField.text.trim(), simParameters.mcc, simParameters.mnc, value)
            busy.show(qsTr("Checking account...", "Registration checking account text"))
        }
        else {
            banner.notify(qsTr("You should select country first!", "Registration banner text"))
        }
    }

    function enterCode(code) {
        registration.enterCode(countrySelect.currentItem.cc, phoneField.text.trim(), simParameters.mcc, simParameters.mnc, code)
    }

    function parseServerReply(reply) {
        var text = ""
        if (reply.status === "sent") {
            text += qsTr("Code successfully requested.", "Registration information constructor")
        }
        else {
            var reason = reply.reason
            var param = reply.param
            if (reply.param === 'in')
                param = qsTr("phone number", "Registration information constructor")
            if (reply.param === 'token')
                param = qsTr("secure token", "Registration information constructor")

            if (reply.reason === "blocked")
                reason = qsTr("Your number is blocked. Please install official android WhatsApp and request support from application. Never tell them you used 3rd party application")
            else if (reply.reason === "too_recent")
                reason = qsTr("Too frequent attempts to request the code.", "Registration information constructor")
            else if (reply.reason === "too_many_guesses")
                reason = qsTr("Too many wrong code guesses.", "Registration information constructor")
            else if (reply.reason === "too_many")
                reason = qsTr("Too many attempts. Try again tomorrow.", "Registration information constructor")
            else if (reply.reason === 'old_version' || reply.reason === 'bad_token')
                reason = qsTr("Protocol version outdated, sorry. Please report this issue and wait for application update.", "Registration information constructor")
            else if (reply.reason === "stale")
                reason = qsTr("Registration code expired. You need to request a new one.", "Registration information constructor")
            else if (reply.reason === "missing") {
                if (typeof(reply.param) === "undefined")
                    reason = qsTr("Registration code expired. You need to request a new one.", "Registration information constructor")
                else
                    reason = qsTr("Missing request param: %1", "Registration information constructor").arg(param)
            }
            else if (reply.reason === "mismatch") {
                reason = qsTr("Invalid registration code entered. Please try again.", "Registration information constructor")
            }
            else if (reply.reason === "bad_param") {
                reason = qsTr("Bad parameters passed to code request: %1", "Registration information constructor").arg(param)
            }
            else if (reply.reason === "no_routes")
                reason = qsTr("No cell routes for %1 caused by your operator. Please try other method [sms/voice]", "Registration information constructor")
                              .arg(reply.method === 'voice'
                                   ? qsTr("making call", "Registration information constructor")
                                   : qsTr("sending sms", "Registration information constructor"))
            text += qsTr("Reason: %1").arg(reason)
        }
        if (reply.retry_after > 0) {
            var retry = reply.retry_after / 60
            var hours = Math.abs(retry / 60)
            var mins = Math.abs(retry % 60)
            var after = ""
            if (hours > 0) {
                after += qsTr("%n hours", "Registration information constructor", hours)
                after += " "
            }
            after += qsTr("%n minutes", "Registration information constructor", mins)
            text += "\n"
            text += qsTr("You can retry requesting code after %1", "Registration information constructor").arg(after)
        }
        return text
    }

    Registration {
        id: registration
        onRegistrationSuccessful: {
            busy.hide()
            banner.notify(qsTr("Successfully registered!", "Registered successfully banner"))
            codeField.visible = false
            accountSettings.setValue("pushname", pushname.text)
            pageStack.replace(Qt.resolvedUrl("LeftMenu.qml"), {"registered": true})
        }
        onRegistrationReply: {
            if (reply.status === "sent") {
                busy.show(qsTr("Waiting for confirmation code..."))
                banner.notify(qsTr("Activation code requested. Wait for %1 soon", "Activation code requested text")
                                   .arg(page.method === "sms"
                                        ? qsTr("sms message", "Activation code requested text information")
                                        : qsTr("voice call", "Activation code requested text information")))
                codeField.visible = true
                codeField.forceActiveFocus()
            }
            else {
                errorArea.show(parseServerReply(reply))
                busy.hide()
                page.forceActiveFocus()
                codeField.visible = false
            }
        }
        onCodeReceived: {
            console.log("code received:", code)
            page.enterCode(code)
        }
    }

    Connections {
        target: connection
        onImportFound: {
            //console.log("import type:", importData["type"])
            var modelData = importData
            importModel.append(modelData)
            page.forceActiveFocus()
        }
    }

    ListModel {
        id: importModel
    }

    Loader {
        id: countriesLoader
        active: false
        asynchronous: true
        sourceComponent: Component {
            CountriesModel {
                simMcc: simParameters.mcc
            }
        }
        onLoaded: {
            countrySelect._updating = false
            countrySelect.currentIndex = countriesLoader.item.simIndex
        }
    }

    SimParameters {
        id: simParameters
        property string mcc
        property string mnc
        onParametersChanged: {
            if ("MobileCountryCode" in parameters) {
                mcc = parameters["MobileCountryCode"]
                accountSettings.setValue("mcc", mcc)
                countriesLoader.active = true
            }
            if ("MobileNetworkCode" in parameters) {
                mnc = parameters["MobileNetworkCode"]
                accountSettings.setValue("mnc", mnc)
            }
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("About.qml"))
                }
            }
            MenuItem {
                text: qsTr("Enter code")
                onClicked: {
                    codeField.visible = true
                    codeField.forceActiveFocus()
                }
            }
        }

        contentHeight: column.height

        Column {
            id: column

            width: page.width

            PageHeader {
                title: qsTr("Registration")
            }

            ComboBox {
                id: countrySelect
                width: parent.width
                label: qsTr("Country:")
                currentIndex: -1
                value: currentIndex >= 0 ? currentItem.text : qsTr("Loading...")
                menu: ContextMenu {
                    Repeater {
                        width: parent.width
                        model: countriesLoader.item
                        delegate: MenuItem {
                            text: model.name
                            property string cc: model.cc
                        }
                    }
                }
                onCurrentIndexChanged: {
                    ccLabel.text = "+" +  currentItem.cc
                    if (importModel.count == 0) {
                        phoneField.forceActiveFocus()
                    }
                }
            }

            TextField {
                id: phoneField
                width: parent.width
                enabled: !codeField.visible
                errorHighlight: text.length === 0
                textLeftMargin: ccLabel.paintedWidth + Theme.paddingLarge
                inputMethodHints: Qt.ImhDialableCharactersOnly
                validator: RegExpValidator{ regExp: /[0-9]*/;}
                label: qsTr("In international format", "Registration phone number text field label")
                _labelItem.anchors.leftMargin: Theme.paddingLarge
                placeholderText: "123456789"
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: pushname.forceActiveFocus()
                property string number: ccLabel.text + phoneField.text

                Component.onCompleted: {
                    _backgroundItem.x = Theme.paddingLarge
                    _backgroundItem.width = _contentItem.width
                }

                Label {
                    id: ccLabel
                    anchors {
                        right: parent.left
                        top: parent.top
                    }
                }
            }

            TextField {
                id: pushname
                enabled: !busy.running
                width: parent.width
                label: qsTr("Profile nickname", "Registration nickname text field label")
                placeholderText: qsTr("Enter your nickname", "Registration nickname text field placeholder")
                errorHighlight: text.length === 0
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: focus = false
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Verify number via")
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: Theme.paddingSmall
                visible: !codeField.visible

                Button {
                    text: qsTr("Sms", "Sms registration button text")
                    enabled: !busy.running
                    onClicked: {
                        doRegister("sms")
                    }
                }

                Button {
                    text: qsTr("Voice", "Voice registration button text")
                    enabled: !busy.running
                    onClicked: {
                        doRegister("voice")
                    }
                }
            }

            TextField {
                id: codeField
                visible: false
                enabled: !busy.running
                width: parent.width
                errorHighlight: text.length === 0
                validator: RegExpValidator { regExp: /^[0-9]{6}$/; }
                inputMethodHints: Qt.ImhDigitsOnly
                label: qsTr("Sms code", "Registration registration code label")
                placeholderText: "123456"
                onTextChanged: {
                    if (acceptableInput) {
                        busy.show(qsTr("Registering account...", "Registering account text"))
                        page.enterCode(codeField.text.trim())
                    }
                }
            }

            Row {
                height: busy.height
                spacing: Theme.paddingMedium
                x: Theme.paddingLarge

                BusyIndicator {
                    id: busy
                    size: BusyIndicatorSize.Medium
                    visible: running
                    running: false
                    function show(data) {
                        actionLabel.text = data
                        running = true
                    }
                    function hide() {
                        running = false
                    }
                }

                Label {
                    id: actionLabel
                    anchors.verticalCenter: parent.verticalCenter
                    visible: busy.visible
                }
            }

            SectionHeader {
                text: "Available to import"
                visible: importModel.count > 0
            }

            Repeater {
                width: parent.width
                model: importModel
                delegate: BackgroundItem {
                    id: delegateItem
                    height: Theme.itemSizeMedium
                    Column {
                        id: content
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                            verticalCenter: parent.verticalCenter
                        }

                        Label {
                            text: model.phone
                            color: delegateItem.highlighted ? Theme.highlightColor : Theme.primaryColor
                        }

                        Label {
                            text: model.name
                            font.pixelSize: Theme.fontSizeSmall
                            color: delegateItem.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        }
                    }
                    onClicked: {
                        pageStack.push(importDialog, {"modelIndex": model.index})
                        /*pageStack.push(importDialog, {"phoneNumber": model.phone,
                                                      "importPass": model.pass,
                                                      "importNextCh": model.nextch,
                                                      "importType": model.type})*/
                    }
                }
            }
        }
    }

    Rectangle {
        id: errorArea
        anchors.horizontalCenter: parent.horizontalCenter
        y: 0 - height
        height: errorContent.height + Theme.paddingLarge * 2
        width: page.width - Theme.paddingLarge * 2
        opacity: 0.0
        color: Theme.highlightDimmerColor
        border.width: 2
        border.color: "red"

        NumberAnimation {
            id: appearAnimation
            target: errorArea
            properties: "y"
            from: 0 - errorArea.height
            to: page.height / 2 - errorArea.height / 2
            duration: 500
            easing.type: Easing.OutBounce
        }

        ParallelAnimation {
            id: disappearAnimation
            NumberAnimation {
                target: errorArea
                properties: "opacity"
                from: 1.0
                to: 0.0
                duration: 300
            }
            NumberAnimation {
                target: errorArea
                properties: "y"
                from: page.height / 2 - errorArea.height / 2
                to: page.height
                duration: 300
            }
        }

        function show(reason) {
            reasonLabel.text = reason
            opacity = 1.0
            appearAnimation.start()
        }

        function hide() {
            reasonLabel.text = ""
            disappearAnimation.start()
        }

        Column {
            id: errorContent
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
            }

            spacing: Theme.paddingLarge

            Label {
                width: parent.width
                text: qsTr("Registration error")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }

            Label {
                id: reasonLabel
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: errorArea.opacity > 0
        onClicked: {
            errorArea.hide()
        }
    }

    Dialog {
        id: importDialog
        property int modelIndex: -1

        onStatusChanged: {
            if (status == DialogStatus.Opening) {
                importConvSwitch.checked = true
                deactivateAccSwitch.checked = true
            }
        }

        onAccepted: {
            var model = importModel.get(importDialog.modelIndex)

            var jids
            if (importConvSwitch.checked) {
                jids = ContactsBaseModel.importConversations(model.type, model.phone)
            }

            if (deactivateAccSwitch.checked) {
                connection.deactivateAccount(model.type)
            }

            accountSettings.setValue("login", model.phone);
            accountSettings.setValue("password", model.pass);
            //accountSettings.setValue("nextChallenge", model.nextch);
            accountSettings.setValue("pushname", model.pushname);

            pageStack.pop(page, PageStackAction.Immediate)
            pageStack.replace(Qt.resolvedUrl("LeftMenu.qml"), {"registered": true, "syncJids": jids})
        }

        DialogHeader {
            id: dialogHeader
            title: qsTr("Account import")
        }

        Column {
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
                top: dialogHeader.bottom
            }

            Label {
                text: qsTr("Phone number: %1").arg(importModel.get(importDialog.modelIndex).phone)
            }

            Label {
                text: qsTr("Account type: %1").arg(importModel.get(importDialog.modelIndex).name)
            }

            TextSwitch {
                id: importConvSwitch
                text: qsTr("Import conversations")
                checked: true
            }

            TextSwitch {
                id: deactivateAccSwitch
                text: qsTr("Deactivate old account")
                checked: true
            }
        }
    }
}


