import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0

Page {
    id: page
    objectName: "userProfile"

    property string jid
    onJidChanged: {
        var model = ContactsBaseModel.getModel(jid)
        props["name"] = model.name
        props["pushname"] = model.pushname
        props["nickname"] = model.nickname
        props["message"] = model.message
        props["phoneavatar"] = model.phoneavatar
        props["avatar"] = model.avatar
        props["timestamp"] = model.timestamp
        props["subtimestamp"] = model.subtimestamp
    }

    Connections {
        target: ContactsBaseModel
        onPropertyChanged: {
            if (pjid === page.jid && props.hasOwnProperty(pname)) {
                props[pname] = pvalue
            }
        }
    }

    QtObject {
        id: props
        property string name
        property string pushname
        property string nickname
        property string message
        property string phoneavatar
        property var avatar
        property int timestamp
        property int subtimestamp
    }

    SilicaFlickable {
        id: flick
        anchors.fill: page
        contentHeight: content.height

        Column {
            id: content
            width: parent.width

            PageHeader {
                title: qsTr("Contact details")
            }

            Row {
                id: avaNameRow

                x: Theme.paddingLarge
                height: Theme.itemSizeMedium
                spacing: Theme.paddingLarge

                Image {
                    width: Theme.itemSizeMedium
                    height: Theme.itemSizeMedium
                    smooth: true
                    source: props.avatar[0]

                    Image {
                        anchors {
                            right: parent.right
                            bottom: parent.bottom
                            margins: -Theme.paddingSmall
                        }

                        width: parent.width / 2
                        height: parent.height / 2

                        source: props.phoneavatar
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {

                        }
                    }
                }

                Column {
                    width: content.height - parent.spacing - parent.height - parent.x * 2
                    spacing: Theme.paddingSmall

                    Label {
                        text: props.nickname
                        font.pixelSize: Theme.fontSizeLarge
                    }

                    Label {
                        text: Format.formatDate(new Date(props.timestamp * 1000), Format.DateFull) + " | " + Format.formatDate(new Date(props.timestamp * 1000), Format.TimeValue)
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.secondaryColor
                    }
                }
            }

            SectionHeader {
                text: qsTr("Status message")
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
                text: Format.formatDate(new Date(props.subtimestamp * 1000), Format.DateLong)
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
                text: props.message
            }
        }

        VerticalScrollDecorator {}
    }
}
