import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import org.nemomobile.configuration 1.0
import "../components"

Page {
    id: page
    objectName: "conversationPage"

    property string jid
    onJidChanged: {
        contactModel = ContactsBaseModel.getModel(jid)
    }

    property var contactModel

    property bool isGroup: jid.indexOf("@g.us") > 0
    property bool isBroadcast: jid.indexOf("@broadcast") > 0
    property bool isPersonal: jid.indexOf("@s.whatsapp.net") > 0

    property bool canSend: !isGroup || contactModel.participants.indexOf(connection.myJid) >= 0

    property TextArea sendBox

    onStatusChanged: {
        if (status == PageStatus.Deactivating) {
            console.log(pageStack.depth)
            if (pageStack.depth <= 3) {
                ContactsBaseModel.activeJid = ""
            }
        }
        else if (status == PageStatus.Activating) {
            ContactsBaseModel.activeJid = jid
        }
        else if (status == PageStatus.Active) {
            if (pageStack._currentContainer.attachedContainer == null) {
                console.log("Attaching profile page")
                if (isGroup) {
                    pageStack.pushAttached(Qt.resolvedUrl("GroupProfile.qml"), {"jid": jid})
                }
                else if (isBroadcast) {
                    //pageStack.pushAttached(Qt.resolvedUrl("BroadcastProfile.qml"), {"jid": jid})
                }
                else if (isPersonal) {
                    pageStack.pushAttached(Qt.resolvedUrl("UserProfile.qml"), {"jid": jid})
                }
            }
        }
    }

    Connections {
        target: appWindow
        onApplicationActiveChanged: {
            if (appWindow.applicationActive) {
                ContactsBaseModel.activeJid = jid
            }
            else {
                ContactsBaseModel.activeJid = ""
            }
        }
    }

    Connections {
        target: ContactsBaseModel
        onPropertyChanged: {
            if (pjid == page.jid) {
                console.log(pname)
                var temp = contactModel
                temp[pname] = pvalue
                contactModel = temp
            }
        }
    }

    Connections {
        target: connection
        onEncryptionStatus: {
            if (pjid === page.jid) {
                encryption = penc
            }
        }
    }
    property bool encryption: false

    SilicaListView {
        id: view
        anchors.fill: page
        clip: true
        spacing: Theme.paddingMedium
        verticalLayoutDirection: ListView.BottomToTop
        currentIndex: -1
        model: conversationModel
        section.property: "section"

        delegate: Column {
            id: wrapper
            width: ListView.view.width

            property bool sectionBoundary: ListView.section != ListView.nextSection
            property Item sectionItem

            onSectionBoundaryChanged: {
                if (sectionBoundary) {
                    if (!sectionItem) {
                        var datestamp = new Date(model.timestamp * 1000)
                        var text
                        var daysBetween = Math.round(Math.abs((new Date().getTime()) - (model.timestamp * 1000))/8.64e7);
                        //if (daysBetween < 1) {
                        //    text = Format.formatDate(datestamp, Format.TimeValue)
                        //}
                        //else
                        if (daysBetween < 7) {
                            text = Format.formatDate(datestamp, Format.WeekdayNameStandalone)
                        }
                        else {
                            text = Format.formatDate(datestamp, Formatter.TimepointRelative)
                        }
                        /*var now = new Date().getTime() / 1000
                        if (now - timestamp < 86400) {
                            text = Format.formatDate(datestamp, Format.TimeValue)
                        }
                        else if (now - timestamp < 604800) {
                            text = Format.formatDate(datestamp, Format.WeekdayNameStandalone)
                        }
                        else {
                            text = Format.formatDate(datestamp, Formatter.TimepointRelative)
                        }*/
                        sectionItem = sectionHeader.createObject(sectionPlaceholder, {"text": text})
                    }
                } else {
                    if (sectionItem) {
                        sectionItem.destroy()
                        sectionItem = null
                    }
                }
            }

            ListView.onRemove: animateRemoval(wrapper)

            Item {
                id: sectionPlaceholder
                width: parent.width
                height: wrapper.sectionItem ? Theme.itemSizeSmall : 0
            }

            Loader {
                width: parent.width
                //property ListView view: ListView.view

                property int sidePadding: Theme.itemSizeLarge + Theme.paddingLarge
                property int lowPadding: Theme.paddingLarge
                property int cornerPadding: Math.sqrt(lowPadding * lowPadding + lowPadding * lowPadding)

                property color backgroundColor: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)

                source: Qt.resolvedUrl("../delegates/ClassicDelegate.qml")
            }
        }
        header: headerComponent
        footer: footerComponent
        /*section.property: "timestamp"
        section.criteria: ViewSection.FullString
        section.labelPositioning: ViewSection.NextLabelAtEnd
        section.delegate: SectionHeader {
            text: Format.formatDate(new Date(section * 1000), Format.DateFull)
        }*/

        property real yTop: view.contentY - view.originY
        property real yBottom: Math.max(view.contentHeight, view.height) - view.height + view.originY

        property int lastIndex: -1

        ViewPlaceholder {
            id: placeholder
            enabled: view.count == 0
            text: qsTr("Empty conversation")
            Component.onCompleted: {
                _content = Qt.createQmlObject("import QtQuick 2.0; Item{}", placeholder)
            }
        }

        PullDownMenu {
            visible: view.count < conversationBaseModel.messagesCount
            MenuItem {
                text: qsTr("Load old conversation")
                onClicked: {
                    view.lastIndex = view.count - 1
                    view.interactive = false
                    conversationBaseModel.loadNext()
                }
            }
        }

        PushUpMenu {
            visible: connection.connectionStatus == Connection.LoggedIn && canSend
            MenuItem {
                text: "Send media"
                onClicked: banner.na()
            }
        }

        function scrollToTop() {
            if (!view.atYBeginning)
                scrollUpTimer.start()
        }
        function scrollToBottom() {
            if (!view.atYEnd)
                scrollDownTimer.start()
        }

        /*onContentHeightChanged: console.log(contentHeight)
        onContentYChanged: console.log(contentY)
        onOriginYChanged: console.log(originY)
        onHeightChanged: console.log(height)
        onYTopChanged: console.log(yTop)
        onYBottomChanged: console.log(yBottom)*/

        Component {
            id: sectionHeader

            Label {
                width: parent.width - (2 * Theme.horizontalPageMargin)
                height: text.length ? implicitHeight : 0
                x: Theme.horizontalPageMargin

                horizontalAlignment: Text.AlignRight
                color: Theme.highlightColor
            }
        }

        Component {
            id: footerComponent
            Item {
                height: page.isLandscape ? Theme.itemSizeSmall : Theme.itemSizeLarge
                width: view.width
                z: parent ? (parent.z + 2) : 9999
            }
        }

        Component {
            id: headerComponent
            Item {
                height: canSend ? (sendField.height - Theme.paddingLarge) : Theme.paddingLarge
                width: view.width
                z: parent ? (parent.z + 2) : 9999
                InverseMouseArea {
                    id: clipFooter

                    y: view.contentY - view.yBottom + yOffset + (view.contentHeight < view.height ? (view.height - view.contentHeight) : 0)
                    width: parent.width
                    height: canSend ? parent.height : 0
                    property real yOffset: view.contentY > view.yBottom ? (view.yBottom - view.contentY)
                                                                        : (view.contentY < view.originY ? (view.originY - view.contentY) : 0)

                    onClickedOutside: sendField.focus = false

                    Background {
                        yOffset: clipFooter.yOffset + (view.height - clipFooter.height)
                    }

                    TextArea {
                        id: sendField
                        y: Theme.paddingMedium
                        width: view.width - sendBtn.width - Theme.paddingLarge
                        height: Math.min(200, implicitHeight)
                        background: Item {}
                        focusOutBehavior: FocusBehavior.KeepFocus
                        enabled: canSend
                        //EnterKey.enabled: text.length > 0
                        //EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                        //EnterKey.onClicked: {
                            //
                        //}
                        onTextChanged: {
                            if (page.status == PageStatus.Active && text.length > 0) {
                                if (!typingTimer.running) {
                                    typingTimer.startTyping()
                                    typingTimer.start()
                                }
                                else {
                                    typingTimer.restart()
                                }
                            }
                        }

                        onFocusChanged: {
                            if (!focus) {
                                saveText()
                            }
                            else {
                                //
                            }
                        }

                        Component.onCompleted: {
                           _editor.textFormat = Text.RichText
                        }

                        function saveText() {
                            //console.log("saving", page.jid, "text")
                            typingConfig.value = Emoji.htmlToWaText(sendField.text).trim()
                        }

                        function sendMessage() {
                            if (typingTimer.running) {
                                typingTimer.stop()
                                typingTimer.stopTyping()
                            }
                            if (connection.connectionStatus == Connection.LoggedIn) {
                                if (isBroadcast) {
                                    //
                                }
                                else {
                                    sendField.deselect()
                                    sendField.cursorPosition = 0
                                    //console.log(sendField.text)
                                    var plain = Emoji.htmlToWaText(sendField.text)
                                    if (plain.trim().length > 0) {
                                        //console.log(plain)
                                        sendField.text = " "
                                        sendField.text = ""
                                        connection.sendText(jid, plain)
                                    }
                                }
                            }
                        }

                        ConfigurationValue {
                            id: typingConfig
                            key: "/apps/mitakuuluu3/typing/" + page.jid
                            defaultValue: ""
                            onValueChanged: {
                                sendField.text = Emoji.waTextToHtml(typingConfig.value)
                            }
                        }
                        /*action.enabled: true
                        action.onClicked: {
                            console.log("action clicked")
                        }
                        actionImage.source: "image://theme/icon-m-message"*/

                        Label {
                            id: placeholderTextLabel

                            text: qsTr("Tap to enter message...")
                            color: sendField._editor.activeFocus ? Theme.secondaryHighlightColor : Theme.secondaryColor

                            opacity: (Emoji.htmlToWaText(sendField.text).length === 0 && !sendField._editor.inputMethodComposing) ? 1.0 : 0.0
                            Behavior on opacity { FadeAnimation {} }
                            elide: Text.ElideRight
                            anchors {
                                left: parent.left; top: parent.top; right: parent.right
                                //topMargin: -sendField.y
                            }
                        }
                    }

                    Image {
                        id: sendBtn
                        width: Theme.itemSizeExtraSmall
                        height: Theme.itemSizeExtraSmall
                        anchors {
                            top: sendField.top
                            right: parent.right
                            rightMargin: Theme.paddingMedium
                        }
                        source: "image://theme/icon-m-message" + (sendBtnArea.down ? ("?" + Theme.highlightColor) : "")
                        visible: canSend

                        MouseArea {
                            id: sendBtnArea
                            anchors.fill: parent
                            property bool down: pressed && containsMouse
                            enabled: parent.visible
                            onClicked: {
                                sendField.sendMessage()
                            }
                        }
                    }
                }
                Item {
                    id: clipHeader

                    y: clipFooter.y - view.height + clipFooter.height
                    width: parent.width
                    height: someHeader.height

                    Background {
                        yOffset: clipFooter.yOffset
                    }

                    ConversationHeader {
                        id: someHeader
                        iconSource: contactModel.avatar.length > 0 ? contactModel.avatar[0] : contactModel.phoneavatar
                        nameText: (encryption ? "<img src=\"image://theme/icon-lock-locked\" />" : "") + Emoji.waTextToHtml(contactModel.nickname)
                        //onNameTextChanged: console.log(nameText)
                        detailsText: isGroup ? Emoji.waTextToHtml(contactModel.message)
                                             : (isPersonal ? (contactModel.available ? Emoji.waTextToHtml(contactModel.message)
                                                                                     : (contactModel.timestamp > 0 ? qsTr("seen %1").arg(formatLastSeen(contactModel.timestamp))
                                                                                                                   : qsTr("last seen time unavailable")))
                                                           : qsTr("broadcast"))
                        clickable: false
                        iconClickable: !isBroadcast

                        onExpandedChanged: {
                            view.interactive = !expanded
                        }
                    }
                }
            }
        }

        VerticalScrollDecorator {
            id: vscroll
            flickable: view
            opacity: (timer.moving && _inBounds) || timer.running || scrollUpTimer.running || scrollDownTimer.running ? 1.0 : 0.0

            Timer {
                id: timer
                property bool moving: view.movingVertically
                onMovingChanged: if (!moving && vscroll._inBounds) restart()
                interval: 300
            }
        }
    }

    MouseArea {
        id: stopScroll
        enabled: scrollUpTimer.running || scrollDownTimer.running
        anchors.fill: view
        onPressed: {
            if (scrollUpTimer.running)
                scrollUpTimer.stop()
            if (scrollDownTimer.running)
                scrollDownTimer.stop()
        }
    }

    Timer {
        id: scrollDownTimer
        interval: 1
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            view.contentY += 10
            if (view.atYEnd) {
                scrollDownTimer.stop()
                view.returnToBounds()
            }
        }
    }

    Timer {
        id: scrollUpTimer
        interval: 1
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            view.contentY -= 10
            if (view.atYBeginning) {
                scrollUpTimer.stop()
                view.returnToBounds()
            }
        }
    }

    Timer {
        id: typingTimer
        interval: 2000
        triggeredOnStart: false
        repeat: false
        onTriggered: stopTyping()
        function startTyping() { connection.sendTyping(page.jid, true) }
        function stopTyping() { connection.sendTyping(page.jid, false) }
    }

    ConversationFilterModel {
        id: conversationModel
        model: ConversationBaseModel {
            id: conversationBaseModel
            jid: page.jid
            limit: 10
            onLoadingCompleted: {
                console.log("loaded next")
                view.positionViewAtIndex(view.lastIndex, ListView.Beginning)
                view.interactive = true
            }
        }
    }
}
