import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "../components"

Page {
    id: page

    SilicaFlickable {
        id: flick

        anchors.fill: parent
        contentHeight: content.height

        PullDownMenu {
            MenuItem {
                text: qsTr("Change account")
                onClicked: {
                    accountSettings.setValue("login", "")
                    accountSettings.setValue("password", "")
                    while (pageStack.depth > 0) {
                        pageStack.navigateBack(PageStackAction.Immediate)
                    }
                    pageStack.push(Qt.resolvedUrl("Registration.qml"), {}, PageStackAction.Immediate)
                }
            }
        }

        Flow {
            id: content
            width: parent.width - Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingMedium

            PageHeader {
                id: header
                title: qsTr("Account")
            }

            Rectangle {
                id: avaRect
                width: (page.isPortrait ? Screen.width : (Screen.width - header.height)) - Theme.paddingLarge
                height: width
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                clip: true

                Image {
                    id: avaImage
                    anchors.fill: parent
                    source: settings.avatar
                    cache: true
                    smooth: true
                }



                AvatarToolbar {
                    jid: connection.myJid
                }
            }

            Column {
                width: content.width - (page.isPortrait ? 0 : avaRect.width) - Theme.paddingLarge
                spacing: Theme.paddingMedium

                Item { width: 1; height: Theme.paddingMedium }

                TextField {
                    id: pushnameField
                    width: parent.width
                    text: Emoji.waTextToHtml(settings.pushname)
                    placeholderText: qsTr("Tap to enter pushname")
                    label: qsTr("Pushname")
                    onFocusChanged: {
                        if (loggedIn && !focus && text.length > 0) {
                            var pushname = Emoji.htmlToWaText(pushnameField.text)
                            accountSettings.setValue("pushname", pushname)
                            if (accountSettings.value("available", true)) {
                                connection.presenceAvailable(pushname)
                            }
                            else {
                                connection.presenceUnavailable(pushname)
                            }
                        }
                        else {
                            text = Emoji.waTextToHtml(settings.pushname)
                        }
                    }
                }

                TextField {
                    id: messageField
                    width: parent.width
                    text: Emoji.waTextToHtml(settings.message)
                    placeholderText: qsTr("Tap to enter status message")
                    label: qsTr("Status message")
                    onFocusChanged: {
                        if (loggedIn && !focus && text.length > 0) {
                            var message = Emoji.htmlToWaText(messageField.text)
                            accountSettings.setValue("message", message)
                        }
                        else {
                            text = Emoji.waTextToHtml(settings.message)
                        }
                    }
                }

                SectionHeader {
                    text: qsTr("Account information")
                }

                Label {
                    width: parent.width
                    x: Theme.paddingMedium
                    text: {
                        var date = new Date(settings.creation * 1000)
                        return qsTr("Creation: %1, %2").arg(Format.formatDate(date, Formatter.TimeValue))
                                                       .arg(Format.formatDate(date, Formatter.DateMedium))
                    }
                }

                Label {
                    width: parent.width
                    x: Theme.paddingMedium
                    text: {
                        var date = new Date(settings.expiration * 1000)
                        return qsTr("Expiration: %1, %2").arg(Format.formatDate(date, Formatter.TimeValue))
                                                         .arg(Format.formatDate(date, Formatter.DateMedium))
                    }
                }

                Label {
                    width: parent.width
                    x: Theme.paddingMedium
                    text: qsTr("Account type: %1").arg(qsTr(settings.kind))
                }
            }
        }

        VerticalScrollDecorator {}
    }

    function tr_noop() {
        qsTr("free", "Account type")
        qsTr("paid", "Account type")
        qsTr("blocked", "Account type")
        qsTr("expired", "Account type")
    }

    Connections {
        target: accountSettings
        onValueChanged: {
            if (key in settings) {
                settings[key] = accountSettings.value(key)
                console.log(key + ":", settings[key])
            }
            else {
                console.log(key)
            }
        }
    }

    QtObject {
        id: settings
        property string avatar
        property string pushname
        property string message
        property int expiration
        property int creation
        property string kind

        Component.onCompleted: {
            avatar = accountSettings.value("avatar", "")
            pushname = accountSettings.value("pushname", "")
            message = accountSettings.value("message", "")
            expiration = accountSettings.value("expiration", 0)
            creation = accountSettings.value("creation", 0)
            kind = accountSettings.value("kind", "expired")
        }
    }
}
