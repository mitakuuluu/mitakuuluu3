import QtQuick 2.1
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    SilicaFlickable {
        id: flick

        anchors.fill: parent
        contentHeight: content.height

        PullDownMenu {
            MenuItem {
                text: qsTr("Send logs")
                onClicked: pageStack.push(Qt.resolvedUrl("SendLogs.qml"))
            }
        }

        Column {
            id: content
            width: parent.width

            PageHeader {
                title: qsTr("Settings")
            }
        }

        ViewPlaceholder {
            parent: flick.contentItem
            flickable: flick
            text: qsTr("No settings are implemented yet. Please come back later.")
            enabled: true
        }

        VerticalScrollDecorator {}
    }
}
