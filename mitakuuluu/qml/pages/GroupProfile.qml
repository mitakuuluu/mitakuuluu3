import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "../components"

Page {
    id: page
    objectName: "groupProfile"

    property string jid
    onJidChanged: {
        var model = ContactsBaseModel.getModel(jid)
        props["name"] = model.name
        props["avatar"] = model.avatar
        props["owner"] = model.owner
        props["timestamp"] = model.timestamp
        props["subowner"] = model.subowner
        props["subtimestamp"] = model.subtimestamp
        props["participants"] = model.participants
        props["admins"] = model.admins
    }

    onStatusChanged: {
        if (status == PageStatus.Activating) {
            connection.sendGetGroupInfo(jid)
        }
    }

    Connections {
        target: ContactsBaseModel
        onPropertyChanged: {
            if (pjid === page.jid && props.hasOwnProperty(pname)) {
                props[pname] = pvalue
            }
        }
    }

    QtObject {
        id: props
        property string name
        property var avatar
        property string owner
        property int timestamp
        property string subowner
        property int subtimestamp
        property var participants
        property var admins

        property bool imParticipant: participants.indexOf(connection.myJid) >= 0
        property bool imAdmin: admins.indexOf(connection.myJid) >= 0
    }

    SilicaFlickable {
        id: flick
        anchors {
            top: parent.top
            topMargin: page.isPortrait ? Theme.itemSizeExtraLarge : 0
            right: parent.right
            bottom: parent.bottom
        }
        width: Screen.width

        contentHeight: content.height

        Column {
            id: content
            width: parent.width

            Column {
                width: parent.width - Theme.paddingLarge * 2
                x: Theme.paddingLarge
                spacing: Theme.paddingSmall

                MouseArea {
                    width: parent.width
                    height: Theme.itemSizeSmall
                    clip: true
                    enabled: props.imParticipant

                    onClicked: subjectField.enabled = true

                    TextField {
                        id: subjectField
                        x: - Theme.paddingLarge
                        width: parent.width + Theme.paddingLarge * 2
                        background: Component {
                            Item {
                                width: parent.width
                                height: parent.height

                                Image {
                                    anchors {
                                        right: parent.right
                                        rightMargin: Theme.paddingLarge
                                    }

                                    source: "image://theme/icon-m-edit"
                                    visible: props.imParticipant && !subjectField.focus
                                }
                            }
                        }
                        enabled: false
                        focus: enabled
                        text: props.name
                        font.pixelSize: Theme.fontSizeLarge
                        onFocusChanged: {
                            if (!focus) {
                                enabled = false
                                var subject = text
                                if (acceptableInput && subject != props.name) {
                                    connection.sendSetGroupSubject(jid, subject)
                                }
                            }
                        }
                        maximumLength: serverProperties.value("max_Subject", 25)
                        EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                        EnterKey.onClicked: focus = false
                    }
                }

                Label {
                    width: parent.width
                    text: qsTr("Created: %1").arg(Format.formatDate(new Date(props.timestamp * 1000), Format.DateMedium) + " | " + Format.formatDate(new Date(props.timestamp * 1000), Format.TimeValue))
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    visible: props.imParticipant
                }

                Label {
                    width: parent.width
                    text: qsTr("Created by: %1").arg(ContactsBaseModel.getNicknameByJid(props.owner))
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    visible: props.imParticipant
                }

                Label {
                    width: parent.width
                    text: qsTr("Subject set: %1").arg(Format.formatDate(new Date(props.subtimestamp * 1000), Format.DateMedium) + " | " + Format.formatDate(new Date(props.timestamp * 1000), Format.TimeValue))
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    visible: props.imParticipant
                }

                Label {
                    width: parent.width
                    text: qsTr("Subject by: %1").arg(ContactsBaseModel.getNicknameByJid(props.subowner))
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    visible: props.imParticipant
                }

                Label {
                    width: parent.width
                    text: qsTr("You not a participant of this group anymore")
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    visible: !props.imParticipant
                }
            }

            SectionHeader {
                text: qsTr("Participants %1/%2").arg(props.participants.length).arg(serverProperties.value("max_participants", 101))
            }

            Repeater {
                width: parent.width
                model: props.participants
                delegate: BackgroundItem {
                    id: dlg
                    width: parent.width
                    height: Theme.itemSizeSmall
                    property var model: ContactsBaseModel.getModel(modelData)
                    property bool isadmin: props.admins.indexOf(modelData) >= 0
                    property bool isyou: modelData == connection.myJid

                    AvatarHolder {
                        id: ava
                        anchors {
                            left: parent.left
                            leftMargin: Theme.paddingLarge
                            verticalCenter: parent.verticalCenter
                        }
                        height: parent.height - Theme.paddingMedium
                        width: height

                        source: isyou ? (accountSettings.value("avatar", "")) : (model.avatar.length > 0 ? model.avatar[0] : model.phoneavatar)
                        emptySource: "../../images/avatar-empty.png"
                    }

                    Label {
                        anchors {
                            left: ava.right
                            leftMargin: Theme.paddingMedium
                            right: dlg.isadmin ? adminImg.left : parent.right
                            rightMargin: Theme.paddingMedium
                            verticalCenter: parent.verticalCenter
                        }

                        truncationMode: TruncationMode.Fade
                        text: isyou ? qsTr("You") : model.nickname
                    }

                    Image {
                        id: adminImg
                        anchors {
                            right: parent.right
                            rightMargin: visible ? Theme.paddingMedium : 0
                            verticalCenter: parent.verticalCenter
                        }
                        source: "image://theme/icon-s-favorite?" + Theme.highlightColor
                        visible: dlg.isadmin
                    }
                }
            }
        }

        VerticalScrollDecorator {}
    }

    MouseArea {
        id: avatarExtender
        property bool extended: false
        width: page.isPortrait ? Screen.width : (extended ? Screen.height : (Screen.height - Screen.width))
        height: page.isLandscape ? Screen.width : (extended ? Screen.height : Theme.itemSizeMedium)
        clip: true

        Item {
            id: avatarClip
            anchors.centerIn: parent
            width: Math.min(Screen.width, parent.width)
            height: Math.min(Screen.width, parent.height)
            clip: true

            Image {
                id: avatarImage
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                cache: true
                smooth: true
                source: props.avatar[0]
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            AvatarToolbar {
                jid: page.jid
                visible: props.imParticipant
            }
        }

        Behavior on width {
            SmoothedAnimation { duration: 300 }
        }

        Behavior on height {
            SmoothedAnimation { duration: 300 }
        }

        onClicked: extended = !extended
    }
}
