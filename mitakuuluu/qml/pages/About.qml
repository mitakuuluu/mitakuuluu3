import QtQuick 2.1
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: content.height

        Column {
            id: content
            width: parent.width

            PageHeader {
                title: qsTr("About")
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                text: "Mitakuuluu v" + Qt.application.version
            }
        }

        VerticalScrollDecorator {}
    }
}
