import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "../components"

Page {
    id: page
    property bool registered: false
    property bool completed: false
    property var syncJids: []

    Component.onCompleted: {
        if (connection.connectionStatus == Connection.LoggedIn) {
            qGuiApp.quitOnLastWindowClosed = false
        }
    }

    onStatusChanged: {
        if (status == PageStatus.Active) {
            if (!pageStack._currentContainer.attachedContainer) {
                console.log("Attaching Chats page")
                pushPage("Chats.qml", true)
            }
            if (registered) {
                connection.login()
                registered = false
            }
            else {
                if (pageStack.depth == 2) {
                    connection.login()
                }
            }
            if (!completed) {
                if (notificationActive.length > 0) {
                    pageStack.push(Qt.resolvedUrl("Conversation.qml"), {"jid": notificationActive}, PageStackAction.Immediate)
                }
                completed = true
            }
        }
    }

    function pushPage(name, immediate) {
        pageStack.pushAttached(Qt.resolvedUrl(name))
        pageStack.navigateForward(immediate ? PageStackAction.Immediate : PageStackAction.Animated)
    }

    Connections {
        target: connection
        onConnectionStatusChanged: {
            statusCombo.currentIndex = connection.connectionStatus == Connection.LoggedIn ? connection.available : 2
            if (connection.connectionStatus == Connection.Disconnected) {
                qGuiApp.quitOnLastWindowClosed = true
            }
            else if (connection.connectionStatus == Connection.LoggedIn) {
                qGuiApp.quitOnLastWindowClosed = false
            }
        }
        onAuthSuccess: {
            if (ContactsBaseModel.count == 0) {
                var selectedNumbers = []
                if (syncJids.length > 0) {
                    for (var i = 0; i < syncJids.length; i++) {
                        selectedNumbers.splice(0, 0, syncJids[i].split("@")[0])
                    }
                }

                var selectPhonebook = pageStack.push(Qt.resolvedUrl("SelectPhonebook.qml"), {"selectedNumbers": selectedNumbers})
                selectPhonebook.accepted.connect(function() { connection.syncContacts(selectPhonebook.selectedContacts) })
                selectPhonebook.rejected.connect(function() { connection.logout() })
                selectPhonebook.syncAll.connect(function() { connection.syncAllPhonebook() })
            }
        }
        onAuthFailed: {
            banner.notify(qsTr("Authentication failed!"))
            pageStack.pop(page, PageStackAction.Immediate)
            pageStack.replace(Qt.resolvedUrl("Registration.qml"), {}, PageStackAction.Immediate)
        }
    }

    Connections {
        target: ContactsBaseModel
        onBusyChanged: {
            if (!ContactsBaseModel.busy) {
                connection.login()
            }
        }
    }

    Connections {
        target: main
        onNotificationActivate: {
            console.log(jid)
            pageStack.pop(page, PageStackAction.Immediate)
            pageStack.pushAttached(Qt.resolvedUrl("Chats.qml"))
            pageStack.navigateForward(PageStackAction.Immediate)
            pageStack.push(Qt.resolvedUrl("Conversation.qml"), {"jid": jid}, PageStackAction.Immediate)
        }
    }

    PageHeader {
        id: header
        title: qsTr("Mitakuuluu")
    }

    Column {
        id: serviceContent
        anchors {
            top: page.isPortrait ? pagesContent.bottom : header.bottom
            left: page.left
        }
        width: page.isPortrait ? Screen.width : (Screen.height / 2)

        ComboBox {
            id: statusCombo
            label: qsTr("Status:")
            value: connection.connectionStatus == Connection.LoggedIn ? (connection.available == Connection.Available ? qsTr("Available") : qsTr("Unavailable"))
                                                                      : connectionString
            description: qsTr("You need to disconnect before exiting. While you connected, closing application window not exit application.")
            property bool complete: false
            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Available")
                }
                MenuItem {
                    text: qsTr("Unavailable")
                }
                MenuItem {
                    text: qsTr("Offline")
                    onClicked: {
                        if (connection.connectionStatus == Connection.LoggedIn) {
                            connection.logout()
                        }
                        else {
                            statusCombo.currentIndex = connection.available
                        }
                    }
                }
            }
            onCurrentIndexChanged: {
                if (!complete)
                    return
                if (statusCombo.currentIndex != 2) {
                    if (connection.connectionStatus == Connection.Disconnected) {
                        connection.login(true)
                    }
                    else if (connection.connectionStatus == Connection.LoggedIn) {
                        connection.available = statusCombo.currentIndex
                    }
                }
            }
            Component.onCompleted: {
                currentIndex = connection.connectionStatus == Connection.LoggedIn ? connection.available : 2
                complete = true
            }
        }
    }

    Column {
        id: pagesContent
        anchors {
            top: header.bottom
            right: page.right
        }
        width: page.isPortrait ? Screen.width : (Screen.height / 2)

        MenuIconItem {
            id: convMenuItem
            label: qsTr("Conversations")
            iconSource: "image://theme/icon-m-chat"
            onClicked: pushPage("Chats.qml")

            Rectangle {
                id: unreadCount
                parent: convMenuItem.extraItem
                anchors.verticalCenter: parent.verticalCenter
                width: visible ? Theme.iconSizeSmall : 0
                height: Theme.iconSizeSmall
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                visible: ContactsBaseModel.totalUnread > 0
                smooth: true

                Label {
                    anchors.centerIn: parent
                    font.pixelSize: Theme.fontSizeExtraSmall
                    text: ContactsBaseModel.totalUnread
                    color: Theme.primaryColor
                }
            }
        }

        MenuIconItem {
            label: qsTr("Contacts")
            iconSource: "image://theme/icon-m-people"
            onClicked: pushPage("Contacts.qml")
        }

        MenuIconItem {
            label: qsTr("Account")
            iconSource: "image://theme/icon-m-region"
            onClicked: pushPage("Account.qml")
        }

        MenuIconItem {
            label: qsTr("Settings")
            iconSource: "image://theme/icon-m-developer-mode"
            onClicked: pushPage("Settings.qml")
        }

        MenuIconItem {
            label: qsTr("About")
            iconSource: "image://theme/icon-m-about"
            onClicked: pushPage("About.qml")
        }
    }
}
