import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "../components"

Page {
    id: page
    objectName: "conversationPage"

    onStatusChanged: {
        if (status == PageStatus.Active) {
            var firstUseCounter = appSettings.value("firstUseChatsCounter", 0)
            if (firstUseCounter < 3) {
                console.log("start hint")
                touchInteractionHint.start()
                appSettings.setValue("firstUseChatsCounter", firstUseCounter + 1)
            }
        }
    }

    SilicaListView {
        id: view
        anchors.fill: page
        PullDownMenu {
            visible: connection.connectionStatus == Connection.LoggedIn
            MenuItem {
                text: qsTr("Create broadcast")
                onClicked: {
                    banner.na()
                }
            }
            MenuItem {
                text: qsTr("Create group chat")
                onClicked: {
                    banner.na()
                }
            }
        }
        header: PageHeader {
            title: qsTr("Conversations")
        }
        model: contactsModel
        delegate: listDelegate
        ViewPlaceholder {
            enabled: view.count == 0
            text: qsTr("No conversations. Go to Contacts page to start new conversations.")
        }
        VerticalScrollDecorator {}
    }

    InteractionHintLabel {
        text: qsTr("Swipe right to access menu")
        anchors.bottom: parent.bottom
        opacity: touchInteractionHint.running ? 1.0 : 0.0
        Behavior on opacity { FadeAnimation { duration: 1000 } }
    }

    TouchInteractionHint {
        id: touchInteractionHint
        direction: TouchInteraction.Right
        anchors.verticalCenter: parent.verticalCenter
        loops: 6
    }

    Component {
        id: listDelegate
        ListItem {
            id: item
            width: ListView.view.width
            contentHeight: Theme.itemSizeMedium
            ListView.onRemove: animateRemoval(item)
            menu: contextMenu
            property bool muted: false
            property bool secure: false
            property bool isGroup: model.jid.indexOf("@g.us") > 0
            property bool isBroadcast: model.jid.indexOf("@broadcast") > 0
            property bool isPersonal: model.jid.indexOf("@s.whatsapp.net") > 0

            function removeContact() {
                banner.na()
                return
                var chatJid = model.jid
                remorseAction(qsTr("Delete", "Delete contact remorse action text"),
                function() {
                    ContactsBaseModel.deleteContact(chatJid)
                })
            }

            function clearChat() {
                banner.na()
                return
                var chatJid = model.jid
                remorseAction(qsTr("Clear chat history", "Delete contact remorse action text"),
                function() {
                    ContactsBaseModel.clearChat(chatJid)
                })
            }

            function leaveGroup() {
                banner.na()
                return
                var chatJid = model.jid
                remorseAction(qsTr("Leave group %1", "Group leave remorse action text").arg(model.nickname),
                function() {
                    connection.groupLeave(chatJid)
                    ContactsBaseModel.deleteContact(chatJid)
                })
            }

            function removeGroup() {
                banner.na()
                return
                var chatJid = model.jid
                remorseAction(qsTr("Delete group %1", "Group delete remorse action text").arg(model.nickname),
                function() {
                    connection.groupRemove(chatJid)
                    ContactsBaseModel.deleteContact(chatJid)
                })
            }

            function removeBroadcast() {
                banner.na()
                return
                var chatJid = model.jid
                remorseAction(qsTr("Delete broadcast %1", "Broadcast delete remorse action text").arg(model.nickname),
                function() {
                    connection.deleteBroadcast(chatJid)
                    ContactsBaseModel.deleteContact(chatJid)
                })
            }

            Rectangle {
                id: presence
                height: ava.height
                anchors.left: parent.left
                anchors.right: ava.left
                anchors.verticalCenter: ava.verticalCenter
                color: model.blocked ? Theme.rgba("red", 0.6) : (connection.connectionStatus === Connection.LoggedIn ? (model.available ? Theme.rgba(Theme.highlightColor, 0.6) : "transparent") : "transparent")
                border.width: model.blocked ? 1 : 0
                border.color: (connection.connectionStatus === Connection.LoggedIn && model.blocked) ? Theme.rgba(Theme.highlightColor, 0.6) : "transparent"
                smooth: true
            }

            AvatarHolder {
                id: ava
                source: model.avatar.length > 0 ? model.avatar[0] : model.phoneavatar
                emptySource: {
                    if (item.isPersonal) {
                        return "../../images/avatar-empty.png"
                    }
                    else if (item.isGroup) {
                        return "../../images/avatar-empty-group.png"
                    }
                    else if (item.isBroadcast) {
                        return "../../images/avatar-broadcast.png"
                    }
                    else {
                        return ""
                    }
                }
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                width: Theme.iconSizeLarge
                height: Theme.iconSizeLarge

                /*Rectangle {
                    id: unreadCount
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: model.unread > 0
                    anchors.right: parent.right
                    anchors.top: parent.top

                    Label {
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeExtraSmall
                        text: model.unread
                        color: Theme.primaryColor
                    }
                }

                Rectangle {
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: item.muted
                    anchors.left: parent.left
                    anchors.top: parent.top

                    Image {
                        source: "image://theme/icon-m-speaker-mute"
                        smooth: true
                        width: Theme.iconSizeSmall
                        height: Theme.iconSizeSmall
                        anchors.centerIn: parent
                    }
                }

                Rectangle {
                    width: Theme.iconSizeSmall
                    height: Theme.iconSizeSmall
                    smooth: true
                    radius: Theme.iconSizeSmall / 4
                    border.width: 1
                    border.color: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    visible: item.secure
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom

                    Image {
                        source: "image://theme/icon-s-secure"
                        smooth: true
                        width: Theme.iconSizeSmall
                        height: Theme.iconSizeSmall
                        anchors.centerIn: parent
                    }
                }*/
            }

            Column {
                id: dataColumn

                anchors.left: ava.right
                anchors.leftMargin: Theme.paddingMedium
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingSmall
                clip: true

                Row {
                    height: nickname.height
                    spacing: Theme.paddingMedium
                    clip: true

                    Label {
                        id: nickname
                        width: dataColumn.width - parent.spacing - lastMessage.width - Theme.paddingLarge
                        font.pixelSize: Theme.fontSizeMedium
                        text: Emoji.waTextToHtml(model.nickname)
                        wrapMode: Text.NoWrap
                        truncationMode: TruncationMode.Fade
                        elide: Text.ElideNone
                        color: item.highlighted ? Theme.highlightColor : Theme.primaryColor
                        textFormat: Text.StyledText
                    }

                    Label {
                        id: lastMessage
                        anchors.verticalCenter: parent.verticalCenter
                        text: formatLastSeen(model.lastmessage)
                        color: (model.unread > 0 || item.highlighted) ? Theme.highlightColor : Theme.primaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        font.weight: Font.Light
                    }
                }

                Row {
                    height: message.height
                    Item {
                        id: deliveryItem
                        //anchors.verticalCenter: parent.verticalCenter
                        width: visible ? msgSentTick.width * 1.5 : 0
                        height: msgSentTick.height
                        visible: model.lastmodel.author === connection.myJid

                        property bool isPersonal: model.lastmodel.jid.indexOf("@s.whatsapp.net") > 0
                        property int deliveredCount: Object.keys(model.lastmodel.deliveryreceipts).length
                        property int readCount: Object.keys(model.lastmodel.readreceipts).length
                        property bool delivered: isPersonal ? (deliveredCount > 0) : (deliveredCount >= model.lastmodel.recepients)
                        property bool received: isPersonal ? (readCount > 0) : (readCount >= model.lastmodel.recepients)

                        GlassItem {
                            id: msgSentTick
                            width: Theme.fontSizeSmall
                            height: Theme.fontSizeSmall
                            anchors.right: parent.right
                            falloffRadius: 0.3
                            radius: 0.4
                            color: (model.lastmodel.status === 0) ? "#80ff0000" : (deliveryItem.received ? "#8000ffff" : (deliveryItem.delivered ? "#8000ff00" : "#80ffff00"))
                        }

                        GlassItem {
                            id: msgDeliveredTick
                            width: Theme.fontSizeSmall
                            height: Theme.fontSizeSmall
                            anchors.left: parent.left
                            falloffRadius: 0.3
                            radius: 0.4
                            color: deliveryItem.received ? "#8000ffff" : (deliveryItem.delivered ? "#8000ff00" : "#80ffff00")
                            visible: deliveryItem.delivered || deliveryItem.received
                        }
                    }

                    Label {
                        id: message
                        font.pixelSize: Theme.fontSizeSmall
                        width: dataColumn.width - parent.spacing * 2 - deliveryItem.width - unreadCount.width - Theme.paddingLarge
                        text: {
                            switch (model.lastmodel.msgtype) {
                            case ConversationBaseModel.TextMessage:     return Emoji.waTextToHtml(model.lastmodel.data.replace("\n", " "))
                            case ConversationBaseModel.ImageMessage:    return qsTr("Image message")
                            case ConversationBaseModel.AudioMessage:    return qsTr("Audio message")
                            case ConversationBaseModel.VideoMessage:    return qsTr("Video message")
                            case ConversationBaseModel.LocationMessage: return qsTr("Location message")
                            case ConversationBaseModel.VCardMessage:    return qsTr("Contact message")
                            default:                                    return model.lastmodel.data
                            }
                        }
                        wrapMode: Text.NoWrap
                        truncationMode: TruncationMode.Fade
                        elide: Text.ElideNone
                        color: item.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        textFormat: Text.StyledText
                        font.weight: Font.Light
                    }

                    Rectangle {
                        id: unreadCount
                        anchors.verticalCenter: parent.verticalCenter
                        width: visible ? Theme.iconSizeSmall : 0
                        height: Theme.iconSizeSmall
                        color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                        visible: model.unread > 0
                        smooth: true

                        Label {
                            anchors.centerIn: parent
                            font.pixelSize: Theme.fontSizeExtraSmall
                            text: model.unread
                            color: Theme.primaryColor
                        }
                    }
                }
            }

            onPressAndHold: {
                console.log(model.nickname)
                //console.log(JSON.stringify(model.lastmodel))
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Conversation.qml"), {"jid": model.jid})
            }

            Component {
                id: contextMenu
                ContextMenu {
                    MenuItem {
                        text: qsTr("Profile", "Contact context menu profile item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        onClicked: {
                            if (model.jid.indexOf("-") > 0) {
                                pageStack.push(Qt.resolvedUrl("GroupProfile.qml"), {"jid": model.jid})
                            }
                            else {
                                if (model.jid === connection.myJid) {
                                    pageStack.push(Qt.resolvedUrl("Account.qml"))
                                }
                                else {
                                    pageStack.push(Qt.resolvedUrl("UserProfile.qml"), {"jid": model.jid})
                                }
                            }
                        }
                    }

                    MenuItem {
                        text: qsTr("Muting", "Contacts context menu muting item")
                        onClicked: {
                            banner.na()
                            return
                            pageStack.push(Qt.resolvedUrl("MutingSelector.qml"), {"jid": model.jid})
                        }
                    }

                    MenuItem {
                        text: item.secure ? qsTr("Un-hide contact") : qsTr("Hide contact")
                        onClicked: {
                            banner.na()
                            return
                            updateHidden(model.jid)
                        }
                    }

                    MenuItem {
                        text: qsTr("Delete group", "Contact context menu delete group item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        visible: model.owner === connection.myJid
                        onClicked: {
                            banner.na()
                            return
                            removeGroup()
                        }
                    }

                    MenuItem {
                        text: qsTr("Delete broadcast", "Contact context menu delete group item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        visible: model.jid.indexOf("@broadcast") >= 0
                        onClicked: {
                            banner.na()
                            return
                            removeBroadcast()
                        }
                    }

                    MenuItem {
                        text: qsTr("Leave group", "Contact context menu leave group item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        visible: model.jid.indexOf("-") > 0
                        onClicked: {
                            banner.na()
                            return
                            leaveGroup()
                        }
                    }

                    MenuItem {
                        text: qsTr("Clear chat history", "Contact context menu delete contact item")
                        enabled: connection.connectionStatus === Connection.LoggedIn
                        onClicked: {
                            banner.na()
                            return
                            clearChat()
                        }
                    }

                    /*MenuItem {
                        text: model.jid.indexOf("-") > 0
                        //% "Contact context menu contact mute item"
                                ? (model.blocked ? qsTr("Unmute")
                        //% "Contact context menu contact unmute item"
                                                 : qsTr("Mute"))
                        //% "Contact context menu contact block item"
                                : (model.blocked ? qsTr("Unblock")
                        //% "Contact context menu contact unblock item"
                                                 : qsTr("Block"))
                        enabled: Mitakuuluu.connectionStatus === Mitakuuluu.LoggedIn
                        onClicked: {
                            if (model.jid.indexOf("-") > 0)
                                Mitakuuluu.muteOrUnmuteGroup(model.jid)
                            else
                                Mitakuuluu.blockOrUnblockContact(model.jid)
                        }
                    }*/
                }
            }
        }
    }

    ContactsFilterModel {
        id: contactsModel
        showActive: true
        showUnknown: true
    }
}





