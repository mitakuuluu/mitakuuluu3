import QtQuick 2.1
import Sailfish.Silica 1.0
import "../js/global.js" as Util

Wallpaper {
    id: wallpaper

    anchors.centerIn: parent

    property real yOffset

    property int angle: (360 - page.rotation) % 360
    rotation: angle
    state: angle

    property Item page
    Component.onCompleted: {
        if (!page) {
            page = Util.findPage(wallpaper)
        }
    }

    states: [
        State {
            name: "0"
            PropertyChanges {
                target: wallpaper
                verticalOffset: -Screen.height / 3
                yCompensation: yOffset
                horizontalOffset: -page.parent.x
                xCompensation: 0
                width: parent.width
                height: parent.height
            }
        },
        State {
            name: "180"
            PropertyChanges {
                target: wallpaper
                verticalOffset: -Screen.height / 3 - (Screen.height -  parent.height)
                yCompensation: yOffset
                horizontalOffset: -page.parent.x
                xCompensation: 0
                width: parent.width
                height: parent.height
            }
        },
        State {
            name: "270"
            PropertyChanges {
                target: wallpaper
                verticalOffset: -Screen.height / 3 - page.parent.y
                yCompensation: 0
                horizontalOffset: -Screen.width + parent.height
                xCompensation: -yOffset
                width: parent.height
                height: parent.width
            }
        },
        State {
            name: "90" // TODO: not working atm
            PropertyChanges {
                target: wallpaper
                verticalOffset: -Screen.height / 3 - page.parent.y
                yCompensation: 0
                horizontalOffset: -wallpaper.offset
                xCompensation: 0
                width: parent.height
                height: parent.width
            }
        }
    ]
}
