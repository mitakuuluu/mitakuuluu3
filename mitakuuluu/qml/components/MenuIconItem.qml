import QtQuick 2.1
import Sailfish.Silica 1.0

BackgroundItem {
    id: root

    property alias label: labelItem.text
    property alias iconSource: iconItem.source
    property alias extraItem: extraPlaceholder

    width: parent ? parent.width : Screen.width
    height: Theme.itemSizeSmall

    Label {
        id: labelItem
        anchors {
            left: parent.left
            right: iconItem.status == Image.Ready ? iconItem.left : parent.right
            margins: Theme.paddingLarge
            verticalCenter: parent.verticalCenter
        }
        color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
    }

    Item {
        id: extraPlaceholder
        x: labelItem.x + labelItem.paintedWidth + Theme.paddingLarge
        width: labelItem.width - labelItem.paintedWidth
        height: parent.height
    }

    Image {
        id: iconItem
        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
            margins: Theme.paddingSmall
        }

        property string _highlightSource
        property color highlightColor: Theme.highlightColor

        function updateHighlightSource() {
            if (state === "") {
                if (source != "") {
                    var tmpSource = iconItem.source.toString()
                    var index = tmpSource.lastIndexOf("?")
                    if (index !== -1) {
                        tmpSource = tmpSource.substring(0, index)
                    }
                    _highlightSource = tmpSource + "?" + highlightColor
                } else {
                    _highlightSource = ""
                }
            }
        }

        onHighlightColorChanged: updateHighlightSource()
        onSourceChanged: updateHighlightSource()
        Component.onCompleted: updateHighlightSource()

        states: State {
            when: root.highlighted && iconItem._highlightSource != ""
            PropertyChanges {
                target: iconItem
                source: iconItem._highlightSource
            }
        }
    }
}
