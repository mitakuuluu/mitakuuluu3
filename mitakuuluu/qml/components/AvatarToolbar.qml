import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0

Item {
    id: root

    property string jid

    anchors.bottom: parent.bottom
    height: Theme.itemSizeMedium
    width: parent.width
    clip: true

    Item {
        id: movingClip
        height: parent.height
        width: parent.width - editbtn.width
        clip: true

        Row {
            id: avaControls

            anchors.bottom: parent.bottom
            height: parent.height
            x: enabled ? (parent.width - width) : parent.width
            enabled: false

            Behavior on x {
                SmoothedAnimation { duration: 300 }
            }
            spacing: 0

            ToolbarButton {
                iconSource: "image://theme/icon-m-camera" + (down ? ("?" + Theme.highlightColor) : "")
                onClicked: {
                    avaControls.enabled = false
                    banner.na()
                }
            }

            ToolbarButton {
                iconSource: "image://theme/icon-m-image" + (down ? ("?" + Theme.highlightColor) : "")
                onClicked: {
                    avaControls.enabled = false
                    var picker = pageStack.push(Qt.resolvedUrl("../pages/AvatarPicker.qml"), {"title": qsTr("Select avatar"), "jid": jid})
                    picker.avatarSourceChanged.connect(function() { connection.sendSetAvatar(jid, picker.avatarSource) })
                }
            }

            ToolbarButton {
                iconSource: "image://theme/icon-m-delete" + (down ? ("?" + Theme.highlightColor) : "")
                onClicked: {
                    avaControls.enabled = false
                    connection.sendSetAvatar(jid, "")
                }
            }
        }
    }

    ToolbarButton {
        id: editbtn

        anchors.right: parent.right

        iconSource: "image://theme/icon-m-" + (avaControls.enabled ? "close" : "edit") + (down ? ("?" + Theme.highlightColor) : "")

        onClicked: {
            avaControls.enabled = !avaControls.enabled
        }
    }
}
