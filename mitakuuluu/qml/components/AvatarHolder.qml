import QtQuick 2.1
import Sailfish.Silica 1.0

Item {
    id: root

    width: 64
    height: 64

    property alias source: img.source

    property alias color: dummy.color
    property alias emptySource: empty.source
    property bool backgroundVisible: img.status !== Image.Ready

    Image {
        id: img
        anchors.fill: root
        sourceSize.width: width
        sourceSize.height: height
        smooth: true
        fillMode: Image.PreserveAspectCrop
        asynchronous: true
        cache: true
        opacity: backgroundVisible ? 0.0 : 1.0
        Behavior on opacity {
            SmoothedAnimation {}
        }
    }

    Rectangle {
        id: dummy
        anchors.fill: root
        color: Theme.rgba(Theme.highlightColor, Theme.highlightBackgroundOpacity)
        opacity: backgroundVisible ? 1.0 : 0.0
        Behavior on opacity {
            SmoothedAnimation {}
        }

        Image {
            id: empty
            anchors.fill: parent
            sourceSize.width: width
            sourceSize.height: height
            smooth: true
            fillMode: Image.PreserveAspectCrop
            asynchronous: true
            cache: false
            clip: true
            enabled: visible
            visible: backgroundVisible
        }
    }
}
