import QtQuick 2.1
import Sailfish.Silica 1.0
import "../components"
import "../js/global.js" as Util

Item {
    id: header

    property alias iconSource: realIcon.source
    property alias nameText: nameLabel.text
    property alias detailsText: detailsLabel.text

    signal clicked

    property bool clickable: content.enabled
    property bool iconClickable: iconArea.enabled
    property bool expanded: false

    property Item page

    Component.onCompleted: {
        if (!page) {
            page = Util.findPage(header)
        }
    }

    width: parent ? parent.width : Screen.width
    height: page && page.isLandscape ? Theme.itemSizeSmall : Theme.itemSizeLarge

    BackgroundItem {
        id: content
        anchors {
            left: icon.right
            leftMargin: Theme.paddingMedium
            top: header.top
            bottom: header.bottom
            right: header.right
            rightMargin: Theme.paddingLarge
        }

        onClicked: header.clicked()

        Column {
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width

            Label {
                id: nameLabel
                color: content.highlighted ? Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                width: parent.width
                truncationMode: TruncationMode.Fade
            }

            Label {
                id: detailsLabel
                color: content.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                width: parent.width
                truncationMode: TruncationMode.Fade
                visible: text.length > 0
            }
        }
    }

    Item {
        id: icon
        anchors {
            left: header.left
            leftMargin: Theme.pageStackIndicatorWidth
            top: header.top
            topMargin: Theme.paddingSmall
        }
        width: header.height - Theme.paddingMedium
        height: width
    }

    Image {
        id: realIcon
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter
        cache: true
        smooth: true

        state: expanded ? "expanded" : "normal"
        states: [
            State {
                name: "expanded"
                PropertyChanges { target: realIcon; width: page.width}
                PropertyChanges { target: realIcon; height: page.height}
                PropertyChanges { target: realIcon; x: 0}
                PropertyChanges { target: realIcon; y: 0}
            },
            State {
                name: "normal"
                PropertyChanges { target: realIcon; width: icon.width}
                PropertyChanges { target: realIcon; height: icon.height}
                PropertyChanges { target: realIcon; x: icon.x}
                PropertyChanges { target: realIcon; y: icon.y}
            }
        ]

        transitions: [
            Transition {
                from: "expanded"
                to: "normal"
                PropertyAnimation { target: realIcon; properties: "x,y,width,height"; duration: 300}
            },
            Transition {
                from: "normal"
                to: "expanded"
                PropertyAnimation { target: realIcon; properties: "x,y,width,height"; duration: 300}
            }
        ]

        MouseArea {
            id: iconArea
            anchors.fill: realIcon
            onClicked: {
                expanded = !expanded
            }
        }
    }
}
