import QtQuick 2.1
import Sailfish.Silica 1.0

TextArea {
    id: textArea

    property alias action: actionBtn
    property alias actionImage: actionImg

    Component.onCompleted: {
       _editor.textFormat = Text.StyledText
    }

    width: parent ? parent.width : Screen.width

    y: Theme.paddingLarge
    height: Math.min(200, implicitHeight) - Theme.paddingLarge
    background: Item {
        width: textArea.width
        height: textArea.height
    }

    MouseArea {
        id: actionBtn
        parent: textArea._backgroundItem

        width: Theme.itemSizeExtraSmall
        height: Theme.itemSizeExtraSmall
        enabled: false
        anchors {
            top: parent.top
            right: parent.right
            rightMargin: Theme.paddingMedium
        }

        Image {
            id: actionImg
            anchors.fill: parent
            visible: actionBtn.enabled && textArea.text.length == 0
        }
    }
}
