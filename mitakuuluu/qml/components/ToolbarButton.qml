import QtQuick 2.1
import Sailfish.Silica 1.0

MouseArea {
    id: root

    property alias iconSource: icon.source

    property bool down: pressed && containsMouse

    width: Theme.itemSizeMedium
    height: Theme.itemSizeMedium

    Rectangle {
        anchors.fill: parent
        color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity * (root.down ? 2 : 1))
    }

    Image {
        id: icon
        anchors.centerIn: parent
    }
}
