import QtQuick 2.1
import Sailfish.Silica 1.0
import org.mitakuuluu3 1.0
import "pages"
import "components"

ApplicationWindow
{
    id: appWindow

    _defaultPageOrientations: Orientation.Portrait | Orientation.Landscape
    initialPage: accountSettings.value("login", "") !== "" && accountSettings.value("password", "") !== ""
                 ? mainPageComp
                 : registrationPageComp
    cover: Qt.resolvedUrl("cover/CoverPage.qml")

    property bool loggedIn: connection.connectionStatus == Connection.LoggedIn

    property string connectionString: {
        switch (connection.connectionStatus) {
            case Connection.Disconnected:   return qsTr("Disconnected")
            case Connection.Connecting:     return qsTr("Connecting")
            case Connection.Connected:      return qsTr("Autorization")
            case Connection.Initialization: return qsTr("Initialization")
            case Connection.LoggedIn:       return qsTr("Logged In")
            default:                        return qsTr("Unknown")
        }
    }

    property string unreadString: {
        switch (ContactsBaseModel.totalUnread) {
            case 0:  return qsTr("No unread messages")
            case 1:  return qsTr("You have unread message")
            default: return qsTr("You have %n unread messages", "", ContactsBaseModel.totalUnread)
        }
    }

    function formatLastSeen(timestamp) {
        var now = new Date().getTime() / 1000
        var datestamp = new Date(timestamp * 1000)
        if (now - timestamp < 86400) {
            return Format.formatDate(datestamp, Format.TimeValue)
        }
        else if (now - timestamp < 604800) {
            return Qt.formatDate(datestamp, "ddd") + ", " + Format.formatDate(datestamp, Format.TimeValue)
        }
        else {
            return Format.formatDate(datestamp, Formatter.TimepointRelative)
        }
    }

    Component {
        id: registrationPageComp
        Registration { }
    }

    Component {
        id: mainPageComp
        LeftMenu { }
    }

    Popup {
        id: banner
        function na() {
            notify(qsTr("Sorry, but this feature is not implemented yet :("))
        }
    }
}
