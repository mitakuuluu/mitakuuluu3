#include "contactsfiltermodel.h"

ContactsFilterModel::ContactsFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    _showActive(false),
    _showUnknown(false),
    _filterContacts(false),
    _initComplete(false),
    _hideGroups(false)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    setFilterRole(Qt::UserRole + 1);
    setSortRole(Qt::UserRole + 1);
    setDynamicSortFilter(true);

    _contactsModel = ContactsBaseModel::GetInstance();
}

void ContactsFilterModel::classBegin()
{
}

void ContactsFilterModel::componentComplete()
{
    changeFilterRole();
    setSourceModel(_contactsModel);
    sort(0);
    _initComplete = true;
}

QVariantMap ContactsFilterModel::get(int itemIndex)
{
    QModelIndex sourceIndex = mapToSource(index(itemIndex, 0, QModelIndex()));
    QVariantMap data = _contactsModel->get(sourceIndex.row());
    return data;
}

void ContactsFilterModel::resort()
{
    sort(0);
}

void ContactsFilterModel::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    Q_UNUSED(topLeft);
    Q_UNUSED(bottomRight);
    //sort(0);
}

bool ContactsFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    QString jid = sourceModel()->data(index, Qt::UserRole + 1).toString();
    if (_filterContacts) {
        bool isHidden = sourceModel()->data(index, Qt::UserRole + 16).toBool();
        if (isHidden)
            return false;
    }
    if (_showActive && (!jid.contains("-") && !jid.contains("@broadcast"))) {
        uint lastmessage = sourceModel()->data(index, Qt::UserRole + 14).toUInt();
        if (lastmessage == 0)
            return false;
    }
    if (!_showUnknown) {
        int contacttype = sourceModel()->data(index, Qt::UserRole + 6).toInt();
        if (contacttype == 0)
            return false;
    }
    if (_hideGroups) {
        if (jid.contains("-") || jid.contains("@broadcast"))
            return false;
    }

    if (filterRegExp().isEmpty())
        return true;
    else {
        QString nickname = sourceModel()->data(index, Qt::UserRole + 4).toString();
        return nickname.contains(filterRegExp());
    }
}

bool ContactsFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    if (_showActive) {
        uint leftlastmessage = sourceModel()->data(left, Qt::UserRole + 14).toUInt();
        uint rightlastmessage = sourceModel()->data(right, Qt::UserRole + 14).toUInt();
        if (leftlastmessage > rightlastmessage)
            return true;
        else if (rightlastmessage > leftlastmessage)
            return false;
    }

    QString leftString = sourceModel()->data(left, Qt::UserRole + 4).toString();
    QString rightString = sourceModel()->data(right, Qt::UserRole + 4).toString();

    return leftString.toLower().localeAwareCompare(rightString.toLower()) < 0;
}

QString ContactsFilterModel::filter()
{
    return filterRegExp().pattern();
}

void ContactsFilterModel::setFilter(const QString &newFilter)
{
    setFilterFixedString(newFilter);
    if (_initComplete) {
        changeFilterRole();
    }
    Q_EMIT filterChanged();
}

bool ContactsFilterModel::showActive()
{
    return _showActive;
}

void ContactsFilterModel::setShowActive(bool value)
{
    _showActive = value;
    if (_initComplete) {
        changeFilterRole();
    }
}

bool ContactsFilterModel::showUnknown()
{
    return _showUnknown;
}

void ContactsFilterModel::setShowUnknown(bool value)
{
    _showUnknown = value;
    if (_initComplete) {
        changeFilterRole();
    }
}

bool ContactsFilterModel::hideGroups()
{
    return _hideGroups;
}

void ContactsFilterModel::setHideGroups(bool value)
{
    _hideGroups = value;
    if (_initComplete) {
        changeFilterRole();
    }
}

bool ContactsFilterModel::filterContacts()
{
    return _filterContacts;
}

void ContactsFilterModel::setFilterContacts(bool value)
{
    _filterContacts = value;
    if (_initComplete) {
        changeFilterRole();
    }
}

int ContactsFilterModel::count()
{
    return rowCount();
}

void ContactsFilterModel::changeFilterRole()
{
    int role = Qt::UserRole + 1;
    if (_showUnknown)
        role += 1;
    if (_showActive)
        role += 2;
    if (_hideGroups)
        role += 4;
    if (_filterContacts)
        role += 8;
    setFilterRole(role);
}
