#include "countriesmodel.h"

#include <QDebug>

CountriesModel::CountriesModel(QObject *parent) :
    QAbstractListModel(parent)
{
    _keys << "code";
    _keys << "name";
    _keys << "cc";
    _keys << "mcc";
    _keys << "plen";
    _keys << "unk1";
    _keys << "unk2";
    _keys << "regx1";
    _keys << "fmt";
    _keys << "regx2";
    _keys << "unk3";
    int role = 0;
    foreach (const QString &rolename, _keys) {
        _roles[role++] = rolename.toLatin1();
    }

    _simIndex = -1;
    _simMcc.clear();

    loadCSV();
}

CountriesModel *CountriesModel::GetInstance(QObject *parent)
{
    static CountriesModel* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new CountriesModel(parent);
    }
    return lsSingleton;
}

int CountriesModel::count() const
{
    return _modelData.size();
}

int CountriesModel::simIndex() const
{
    return _simIndex;
}

QString CountriesModel::simMcc() const
{
    return _simMcc;
}

void CountriesModel::setSimMcc(const QString &mcc)
{
    _simMcc = mcc;
    Q_EMIT simMccChanged();

    if (_modelData.size() > 0) {
        for (int i = 0; i < _modelData.size(); i++) {
            if (_modelData.at(i).at(3).compare(mcc) == 0) {
                _simIndex = i;
                Q_EMIT simIndexChanged();
                break;
            }
        }
    }
}

int CountriesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _modelData.size();
}

QVariant CountriesModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _modelData.count())
        return QVariant();
    return _modelData[row][role];
}

QVariantMap CountriesModel::get(int index)
{
    if (index < 0 || index >= _modelData.count())
        return QVariantMap();
    QVariantMap result;
    for (int i = 0; i < _keys.size(); i++) {
        result.insert(_keys[i], _modelData[index][i]);
    }
    return result;
}

void CountriesModel::loadCSV()
{
    QFile input("/usr/share/mitakuuluu3/data/countries/countries.tsv");
    if (input.exists() && input.open(QFile::ReadOnly | QFile::Text)) {
        beginResetModel();
        QTextStream in(&input);
        while (!in.atEnd()) {
            QStringList line = in.readLine().split("\t");
            if (_simIndex < 0) {
                if (line.at(3).compare(_simMcc) == 0) {
                    _simIndex = _modelData.size();
                    Q_EMIT simIndexChanged();
                }
            }
            _modelData.append(line);
        }
        Q_EMIT countChanged();
        endResetModel();
        if (input.isOpen()) {
            input.close();
        }
    }
}
