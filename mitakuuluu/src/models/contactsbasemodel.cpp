#include "contactsbasemodel.h"
#include <cmath>
#include <QStandardPaths>

#include "src/models/messagesbasemodel.h"
#include "src/settings/accountsettings.h"

#include <QDebug>

ContactsBaseModel::ContactsBaseModel(QObject *parent) :
    QAbstractListModel(parent),
    _busy(true)
{
    qDebug() << "Constructing base contacts model";

    _keys << "jid"; // Qt::UserRole + 1
    _keys << "pushname";
    _keys << "name";
    _keys << "nickname";
    _keys << "message";
    _keys << "known";
    _keys << "owner";
    _keys << "subowner";
    _keys << "timestamp";
    _keys << "subtimestamp"; // Qt::UserRole + 10
    _keys << "avatar";
    _keys << "unread";
    _keys << "available";
    _keys << "lastmessage";
    _keys << "muting";
    _keys << "hidden";
    _keys << "participants";
    _keys << "admins";
    _keys << "blocked";
    _keys << "typing";
    _keys << "lastmodel"; // Qt::UserRole + 20
    _keys << "phoneavatar";
    int role = Qt::UserRole + 1;
    foreach (const QString &rolename, _keys) {
        _roles[role++] = rolename.toLatin1();
    }

    _totalUnread = 0;

    dbExecutor = QueryExecutor::GetInstance();

    connection = MainConnection::GetInstance();
    QObject::connect(connection, SIGNAL(contactsSynced(QVariantMap)), this, SLOT(contactsSynced(QVariantMap)));
    QObject::connect(connection, SIGNAL(contactsStatuses(QVariantMap)), this, SLOT(contactsStatuses(QVariantMap)));
    QObject::connect(connection, SIGNAL(contactsPictureIds(QVariantMap)), this, SLOT(contactsPictureIds(QVariantMap)));
    QObject::connect(connection, SIGNAL(contactPictureHidden(QString,QString)), this, SLOT(contactPictureHidden(QString,QString)));
    QObject::connect(connection, SIGNAL(contactPicture(QString,QByteArray,QString)), this, SLOT(contactPicture(QString,QByteArray,QString)));
    QObject::connect(connection, SIGNAL(contactPictureId(QString,QString,QString)), this, SLOT(contactPictureId(QString,QString,QString)));
    QObject::connect(connection, SIGNAL(contactStatus(QString,QString,QString)), this, SLOT(contactStatus(QString,QString,QString)));
    QObject::connect(connection, SIGNAL(contactLastSeen(QString,uint)), this, SLOT(contactLastSeen(QString,uint)));
    QObject::connect(connection, SIGNAL(contactLastSeenHidden(QString,QString)), this, SLOT(contactLastSeenHidden(QString,QString)));
    QObject::connect(connection, SIGNAL(connectionStatusChanged()), this, SLOT(connectionStatusChanged()));
    QObject::connect(connection, SIGNAL(contactAvailable(QString)), this, SLOT(contactAvailable(QString)));
    QObject::connect(connection, SIGNAL(contactUnavailable(QString,QString)), this, SLOT(contactUnavailable(QString,QString)));
    QObject::connect(connection, SIGNAL(groupsReceived(QVariantMap)), this, SLOT(groupsReceived(QVariantMap)));
    QObject::connect(connection, SIGNAL(retryMessage(QString,QString)), this, SLOT(retryMessage(QString,QString)));
    QObject::connect(connection, SIGNAL(blacklistReceived(QStringList)), this, SLOT(blacklistReceived(QStringList)));
    QObject::connect(connection, SIGNAL(broadcastsReceived(QVariantMap)), this, SLOT(broadcastsReceived(QVariantMap)));

    QObject::connect(connection, SIGNAL(groupSubjectChanged(QString,QString,QString,QString)), this, SLOT(groupSubjectChanged(QString,QString,QString,QString)));
    QObject::connect(connection, SIGNAL(groupCreated(QString,QString,QString,QString,QString,QString,QStringList,QStringList)), this, SLOT(groupCreated(QString,QString,QString,QString,QString,QString,QStringList,QStringList)));
    QObject::connect(connection, SIGNAL(groupParticipantAdded(QString,QString)), this, SLOT(groupParticipantAdded(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantRemoved(QString,QString)), this, SLOT(groupParticipantRemoved(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantPromoted(QString,QString)), this, SLOT(groupParticipantPromoted(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantDemoted(QString,QString)), this, SLOT(groupParticipantDemoted(QString,QString)));
    QObject::connect(connection, SIGNAL(groupInfo(QString,QVariantMap)), this, SLOT(groupInfo(QString,QVariantMap)));

    QObject::connect(connection, SIGNAL(textMessageReceived(QString,QString,QString,QString,bool,QString)), this, SLOT(textMessageReceived(QString,QString,QString,QString,bool,QString)));
    QObject::connect(connection, SIGNAL(mediaMessageReceived(QString,QString,QString,QString,bool,AttributeList,QByteArray)), this, SLOT(mediaMessageReceived(QString,QString,QString,QString,bool,AttributeList,QByteArray)));
    QObject::connect(connection, SIGNAL(textMessageSent(QString,QString,QString,QString)), this, SLOT(textMessageSent(QString,QString,QString,QString)));
    QObject::connect(connection, SIGNAL(messageSent(QString,QString,QString)), this, SLOT(messageSent(QString,QString,QString)));
    QObject::connect(connection, SIGNAL(messageReceipt(QString,QString,QString,QString,QString)), this, SLOT(messageReceipt(QString,QString,QString,QString,QString)));

    QObject::connect(connection, SIGNAL(downloadCompleted(QString,QString,QString)), this, SLOT(onDownloadCompleted(QString,QString,QString)));

    // TODO: remove connections
    /*
    QObject::connect(client, SIGNAL(pictureUpdated(QString,QString)), this, SLOT(pictureUpdated(QString,QString)));
    QObject::connect(client, SIGNAL(setUnread(QString,int)), this, SLOT(setUnread(QString,int)));
    QObject::connect(client, SIGNAL(pushnameUpdated(QString, QString)), this, SLOT(pushnameUpdated(QString, QString)));
    QObject::connect(client, SIGNAL(contactAvailable(QString)), this, SLOT(presenceAvailable(QString)));
    QObject::connect(client, SIGNAL(contactUnavailable(QString)), this, SLOT(presenceUnavailable(QString)));
    QObject::connect(client, SIGNAL(contactLastSeen(QString, int)), this, SLOT(presenceLastSeen(QString, int)));
    QObject::connect(client, SIGNAL(groupInfo(QVariantMap)), this, SLOT(groupInfo(QVariantMap)));
    QObject::connect(client, SIGNAL(contactChanged(QVariantMap)), this, SLOT(contactChanged(QVariantMap)));
    QObject::connect(client, SIGNAL(contactSynced(QVariantMap)), this, SLOT(contactSynced(QVariantMap)));
    QObject::connect(client, SIGNAL(contactsChanged()), this, SLOT(contactsChanged()));
    QObject::connect(client, SIGNAL(newGroupSubject(QVariantMap)), this, SLOT(newGroupSubject(QVariantMap)));
    QObject::connect(client, SIGNAL(messageReceived(QVariantMap)), this, SLOT(messageReceived(QVariantMap)));
    QObject::connect(client, SIGNAL(contactStatus(QString, QString, int)), this, SLOT(contactStatus(QString, QString, int)));
    QObject::connect(client, SIGNAL(contactsBlocked(QStringList)), this, SLOT(contactsBlocked(QStringList)));
    QObject::connect(client, SIGNAL(contactsAvailable(QStringList)), this, SLOT(contactsAvailable(QStringList)));
    QObject::connect(client, SIGNAL(contactTyping(QString)), this, SLOT(contactTyping(QString)));
    QObject::connect(client, SIGNAL(contactPaused(QString)), this, SLOT(contactPaused(QString)));
    QObject::connect(client, SIGNAL(groupRemoved(QString)), this, SLOT(contactRemoved(QString)));
    */

    //client->getPrivacyList();
    //client->getAvailableJids();

    contactsChanged();
}

void ContactsBaseModel::reloadContact(const QString &jid)
{
    /*QVariantMap query;
    query["type"] = QueryType::ContactsReloadContact;
    query["jid"] = jid;
    query["uuid"] = uuid;
    dbExecutor->queueAction(query);*/
}

bool ContactsBaseModel::setPropertyByJid(const QString &jid, const QString &name, const QVariant &value)
{
    if (_modelData.contains(jid)) {
        if (_modelData[jid][name] == value)
            return false;

        int row = _modelData.keys().indexOf(jid);
        _modelData[jid][name] = value;
        Q_EMIT dataChanged(index(row), index(row), QVector<int>(Qt::UserRole + 1 + _keys.indexOf(name)));

        Q_EMIT propertyChanged(jid, name, value);

        return true;
    }

    return false;
}

void ContactsBaseModel::deleteContact(const QString &jid)
{
    /*if (_modelData.contains(jid)) {
        contactRemoved(jid);

        QVariantMap query;
        query["type"] = QueryType::ContactsRemove;
        query["jid"]  = jid;
        query["uuid"] = uuid;
        dbExecutor->queueAction(query);

        client->contactRemoved(jid);
    }*/
}

QVariantMap ContactsBaseModel::getModel(const QString &jid)
{
    if (_modelData.contains(jid))
        return _modelData[jid];
    return QVariantMap();
}

QVariantMap ContactsBaseModel::get(int index)
{
    if (index < 0 || index >= _modelData.count())
        return QVariantMap();
    return _modelData[_modelData.keys().at(index)];
}

QColor ContactsBaseModel::getColorForJid(const QString &jid)
{
    if (!_colors.keys().contains(jid))
        _colors[jid] = generateColor();
    QColor color = _colors[jid];
    //color.setAlpha(96);
    return color;
}

void ContactsBaseModel::clear()
{
    beginResetModel();
    _modelData.clear();
    endResetModel();
}

QColor ContactsBaseModel::generateColor()
{
    qreal golden_ratio_conjugate = 0.618033988749895;
    qreal h = (qreal)rand()/(qreal)RAND_MAX;
    h += golden_ratio_conjugate;
    h = fmod(h, 1);
    QColor color = QColor::fromHsvF(h, 0.5, 0.95);
    return color;
}

int ContactsBaseModel::getTotalUnread()
{
    return _totalUnread;
}

void ContactsBaseModel::checkTotalUnread()
{
    _totalUnread = 0;
    foreach (const QVariantMap &contact, _modelData.values()) {
        _totalUnread += contact["unread"].toInt();
    }
    Q_EMIT totalUnreadChanged();
}

QStringList ContactsBaseModel::blacklist() const
{
    return _blockedContacts;
}

QString ContactsBaseModel::activeJid() const
{
    return _activeJid;
}

void ContactsBaseModel::setActiveJid(const QString &activeJid)
{
    if (_activeJid != activeJid) {
        _activeJid = activeJid;
        qDebug() << _activeJid;

        if (!_activeJid.isEmpty()) {
            MessagesBaseModel::GetInstance()->clearMessagesForJid(_activeJid);

            setUnread(_activeJid, 0);
        }

        Q_EMIT activeJidChanged();
    }
}

bool ContactsBaseModel::isBusy()
{
    return _busy;
}

void ContactsBaseModel::contactsSynced(const QVariantMap &contacts)
{
    QMapIterator<QString, QVariant> iter(contacts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QString name = iter.value().toMap()["name"].toString();
        QString phoneavatar = iter.value().toMap()["avatar"].toString();

        if (_modelData.contains(jid)) {
            _modelData[jid]["name"] = name;
            _modelData[jid]["phoneavatar"] = phoneavatar;
            _modelData[jid]["nickname"] = name;

            int row = _modelData.keys().indexOf(jid);
            Q_EMIT dataChanged(index(row), index(row));
        }
        else {
            QVariantMap contact;
            contact["jid"] = jid;
            contact["pushname"] = QString();
            contact["name"] = name;
            contact["owner"] = QString();
            contact["nickname"] = name;
            contact["message"] = QString();
            contact["subowner"] = QString();
            contact["subtimestamp"] = 0;
            contact["timestamp"] = 0;
            contact["known"] = true;
            contact["avatar"] = QStringList();
            contact["phoneavatar"] = phoneavatar;
            contact["available"] = false;
            contact["unread"] = 0;
            contact["lastmessage"] = 0;
            contact["muting"] = false;
            contact["blocked"] = false;
            contact["hidden"] = false;
            contact["typing"] = false;
            contact["participants"] = QStringList() << jid;
            contact["admins"] = QStringList();
            contact["lastmodel"] = QVariantMap();

            beginResetModel();
            _modelData[jid] = contact;
            endResetModel();

            Q_EMIT countChanged();

            connection->getContactLastSeen(jid);
            connection->sendSubscribe(jid);
        }
    }

    if (contacts.size() > 0) {
        QVariantMap query;
        query["type"] = QueryType::ContactsSynced;
        query["contacts"] = contacts;
        dbExecutor->queueAction(query);

        connection->getContactsStatuses(contacts.keys());
        connection->getContactsPictureIds(contacts.keys());
    }
}

void ContactsBaseModel::contactsStatuses(const QVariantMap &contacts)
{
    QMapIterator<QString, QVariant> iter(contacts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QVariantMap data = iter.value().toMap();

        contactStatus(jid, data["status"].toString(), data["t"].toInt());
    }

    if (contacts.size() > 0) {
        QVariantMap query;
        query["type"] = QueryType::ContactsStatuses;
        query["contacts"] = contacts;
        dbExecutor->queueAction(query);
    }
}

void ContactsBaseModel::contactsPictureIds(const QVariantMap &contacts)
{
    QMapIterator<QString, QVariant> iter(contacts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();

        if (_modelData.contains(jid)) {
            QString avatar = getPictureForId(iter.value().toString());
            setContactAvatar(jid, avatar);
        }
    }
}

void ContactsBaseModel::contactPictureHidden(const QString &jid, const QString &code)
{
    if (_modelData.contains(jid)) {
        QString id = code == "404" ? "empty" : "hidden";
        QString avatar = QString("/usr/share/mitakuuluu3/images/avatar-%1%2.png").arg(id).arg(jid.contains("g.us") ? "-group" : "");
        setContactAvatar(jid, avatar);
    }

    if (jid == connection->myJid()) {
        QString avatar = AccountSettings::GetInstance()->value("avatar").toString();
        QFile file(avatar);
        if (file.exists()) {
            file.remove();
        }
        AccountSettings::GetInstance()->setValue("avatar", QString());
    }
}

void ContactsBaseModel::contactPicture(const QString &jid, const QByteArray &data, const QString &id)
{
    if (_modelData.contains(jid)) {
        QString avatar = getPictureForId(id);
        setContactAvatar(jid, avatar, data);
    }

    if (jid == connection->myJid() && !data.isEmpty()) {
        QString avatar = getPictureForId(id);
        QFile file(avatar);
        if (file.open(QFile::WriteOnly)) {
            file.resize(0);
            file.write(data);
            file.close();
        }
        AccountSettings::GetInstance()->setValue("avatar", avatar);
    }
}

void ContactsBaseModel::contactPictureId(const QString &jid, const QString &id, const QString &author)
{
    if (_modelData.contains(jid)) {
        QString avatar = getPictureForId(id);
        setContactAvatar(jid, avatar, QByteArray());
    }
}

void ContactsBaseModel::contactStatus(const QString &jid, const QString &status, const QString &t)
{
    contactStatus(jid, status, t.toInt());

    QVariantMap query;
    query["type"] = QueryType::ContactStatus;
    query["jid"] = jid;
    query["status"] = status;
    query["t"] = t.toInt();
    dbExecutor->queueAction(query);
}

void ContactsBaseModel::contactLastSeen(const QString &jid, uint timestamp)
{
    setPropertyByJid(jid, "timestamp", timestamp);

    QVariantMap query;
    query["type"] = QueryType::ContactLastSeen;
    query["jid"] = jid;
    query["t"] = timestamp;
    dbExecutor->queueAction(query);
}

void ContactsBaseModel::contactLastSeenHidden(const QString &jid, const QString &code)
{
    contactLastSeen(jid, code.toUInt());
}

void ContactsBaseModel::contactAvailable(const QString &jid)
{
    if (_modelData.contains(jid)) {
        setPropertyByJid(jid, "available", true);
    }
}

void ContactsBaseModel::contactUnavailable(const QString &jid, const QString &last)
{
    if (_modelData.contains(jid)) {
        _modelData[jid]["available"] = false;
        _modelData[jid]["timestamp"] = last;
        int row = _modelData.keys().indexOf(jid);
        dataChanged(index(row), index(row));

        Q_EMIT propertyChanged(jid, "available", false);
        Q_EMIT propertyChanged(jid, "timestamp", last == "deny" ? 0 : last.toUInt());

        QVariantMap query;
        query["type"] = QueryType::ContactLastSeen;
        query["jid"] = jid;
        query["t"] = last == "deny" ? 0 : last.toUInt();
        dbExecutor->queueAction(query);
    }
}

void ContactsBaseModel::groupsReceived(const QVariantMap &groups)
{
    QMapIterator<QString, QVariant> iter(groups);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        if (!_modelData.contains(jid)) {
            QVariantMap group = iter.value().toMap();

            QVariantMap contact;
            contact["jid"] = jid;
            contact["pushname"] = QString();
            contact["name"] = group["subject"];
            contact["owner"] = group["owner"];
            contact["nickname"] = group["subject"];
            contact["message"] = QString();
            contact["subowner"] = group["s_o"];
            contact["subtimestamp"] = group["s_t"];
            contact["timestamp"] = group["creation"];
            contact["known"] = true;
            contact["avatar"] = QStringList() << QString("/usr/share/mitakuuluu3/images/avatar-empty-group.png");
            contact["phoneavatar"] = QString();
            contact["available"] = false;
            contact["unread"] = 0;
            contact["lastmessage"] = QDateTime::currentDateTime().toTime_t();
            contact["muting"] = false;
            contact["blocked"] = false;
            contact["hidden"] = false;
            contact["typing"] = false;
            contact["participants"] = group["participants"];
            contact["admins"] = group["admins"];

            QVariantMap query;
            query["type"] = QueryType::MessageReceived;
            query["jid"] = jid;

            QVariantMap message;
            message["msgid"] = QString("%1-1").arg(group["creation"].toString());
            message["jid"] = jid;
            message["author"] = jid;
            message["timestamp"] = group["creation"].toUInt();
            message["data"] = tr("You joined group");
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            contact["lastmodel"] = message;

            query["message"] = message;

            dbExecutor->queueAction(query);

            beginResetModel();
            _modelData[jid] = contact;
            endResetModel();

            // TOOD: set lastmodel with some "You joined group"
        }
    }

    if (groups.size() > 0) {
        QVariantMap query;
        query["type"] = QueryType::GroupsParticipating;
        query["groups"] = groups;
        dbExecutor->queueAction(query);
    }
}

void ContactsBaseModel::groupSubjectChanged(const QString &jid, const QString &subject, const QString &s_o, const QString &s_t)
{
    if (_modelData.contains(jid)) {
        _modelData[jid]["name"] = subject;
        _modelData[jid]["nickname"] = subject;
        _modelData[jid]["subowner"] = s_o;
        _modelData[jid]["subtimestamp"] = s_t;
        int row = _modelData.keys().indexOf(jid);
        dataChanged(index(row), index(row));

        Q_EMIT propertyChanged(jid, "name", subject);
        Q_EMIT propertyChanged(jid, "nickname", subject);
        Q_EMIT propertyChanged(jid, "subowner", s_o);
        Q_EMIT propertyChanged(jid, "subtimestamp", s_t);

        QVariantMap query;
        query["type"] = QueryType::GroupSubjectChanged;
        query["jid"] = jid;
        query["name"] = subject;
        query["subowner"] = s_o;
        query["subtimestamp"] = s_t;
        dbExecutor->queueAction(query);

        QVariantMap message;
        uint timestamp = s_t.toUInt();
        message["msgid"] = QString("%1-2").arg(timestamp);
        message["jid"] = jid;
        message["author"] = jid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 changed group subject to %2").arg(getNicknameByJid(s_o)).arg(subject);
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;

        setLastMessage(jid, s_t);
        setPropertyByJid(jid, "lastmodel", message);

        query.clear();
        query["type"] = QueryType::MessageReceived;
        query["jid"] = jid;
        query["message"] = message;
        dbExecutor->queueAction(query);

        if (jid != _activeJid) {
            message["progress"] = 0;
            MessagesBaseModel::GetInstance()->notify(message);

            setUnread(jid, _modelData[jid]["unread"].toInt() + 1);
        }
    }
}

void ContactsBaseModel::blacklistReceived(const QStringList &list)
{
    _blockedContacts = list;
    Q_EMIT blacklistChanged();
}

void ContactsBaseModel::broadcastsReceived(const QVariantMap &broadcasts)
{
    QMapIterator<QString, QVariant> iter(broadcasts);
    int i = 0;
    while (iter.hasNext()) {
        iter.next();
        i++;
        QString jid = iter.key();
        if (!_modelData.contains(jid)) {
            QVariantMap broadcast = iter.value().toMap();

            QVariantMap contact;
            contact["jid"] = jid;
            contact["pushname"] = QString();
            contact["name"] = broadcast["name"];
            contact["owner"] = connection->myJid();
            contact["nickname"] = broadcast["name"];
            contact["message"] = QString();
            contact["subowner"] = QString();
            contact["subtimestamp"] = 0;
            contact["timestamp"] = 0;
            contact["known"] = true;
            contact["avatar"] = QStringList() << QString("/usr/share/mitakuuluu3/images/avatar-broadcast.png");
            contact["phoneavatar"] = QString();
            contact["available"] = false;
            contact["unread"] = 0;
            contact["lastmessage"] = QDateTime::currentDateTime().toTime_t();
            contact["muting"] = false;
            contact["blocked"] = false;
            contact["hidden"] = false;
            contact["typing"] = false;
            contact["participants"] = broadcast["participants"].toStringList();
            contact["admins"] = QStringList();

            QVariantMap query;
            query["type"] = QueryType::MessageReceived;
            query["jid"] = jid;

            QVariantMap message;
            message["msgid"] = QString("%1-%2").arg(QDateTime::currentDateTime().toTime_t()).arg(i);
            message["jid"] = jid;
            message["author"] = jid;
            message["timestamp"] = QDateTime::currentDateTime().toTime_t();
            message["data"] = tr("Broadcast list created");
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            contact["lastmodel"] = message;

            query["message"] = message;

            dbExecutor->queueAction(query);

            beginResetModel();
            _modelData[jid] = contact;
            endResetModel();

            // TOOD: set lastmodel with some "You joined group"
        }
    }

    if (broadcasts.size() > 0) {
        QVariantMap query;
        query["type"] = QueryType::BroadcastLists;
        query["broadcasts"] = broadcasts;
        dbExecutor->queueAction(query);
    }
}

void ContactsBaseModel::groupCreated(const QString &gjid, const QString &creation, const QString &creator, const QString &s_o, const QString &s_t, const QString &subject, const QStringList &participants, const QStringList &admins)
{
    if (!_modelData.contains(gjid)) {
        qDebug() << gjid << subject;
        QVariantMap contact;
        contact["jid"] = gjid;
        contact["pushname"] = QString();
        contact["name"] = subject;
        contact["owner"] = creator;
        contact["nickname"] = subject;
        contact["message"] = QString();
        contact["subowner"] = s_o;
        contact["subtimestamp"] = s_t;
        contact["timestamp"] = creation;
        contact["known"] = true;
        contact["avatar"] = QStringList() << QString("/usr/share/mitakuuluu3/images/avatar-empty-group.png");
        contact["phoneavatar"] = QString();
        contact["available"] = false;
        contact["unread"] = 0;
        contact["lastmessage"] = QDateTime::currentDateTime().toTime_t();
        contact["muting"] = false;
        contact["blocked"] = false;
        contact["hidden"] = false;
        contact["participants"] = participants;
        contact["admins"] = admins;

        QVariantMap query;
        query["type"] = QueryType::ContactAdded;
        query["contact"] = contact;
        dbExecutor->queueAction(query);

        QVariantMap message;
        message["msgid"] = QString("%1-3").arg(creation);
        message["jid"] = gjid;
        message["author"] = gjid;
        message["timestamp"] = creation.toUInt();
        message["data"] = tr("You joined group");
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;

        contact["lastmodel"] = message;
        contact["typing"] = false;

        query.clear();
        query["type"] = QueryType::MessageReceived;
        query["jid"] = gjid;
        query["message"] = message;
        dbExecutor->queueAction(query);

        beginResetModel();
        _modelData[gjid] = contact;
        endResetModel();

        if (gjid != _activeJid) {
            message["progress"] = 0;
            MessagesBaseModel::GetInstance()->notify(message);

            setUnread(gjid, _modelData[gjid]["unread"].toInt() + 1);
        }

        connection->getContactPicture(gjid);
    }
}

void ContactsBaseModel::groupParticipantAdded(const QString &gjid, const QString &jid)
{
    if (_modelData.contains(gjid)) {
        qDebug() << gjid << jid;
        QStringList participants = _modelData[gjid]["participants"].toStringList();
        if (!participants.contains(jid)) {
            participants.append(jid);
            setPropertyByJid(gjid, "participants", participants);

            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = gjid;
            query["property"] = "participants";
            query["value"] = participants;
            dbExecutor->queueAction(query);

            QVariantMap message;
            uint timestamp = QDateTime::currentDateTime().toTime_t();
            message["msgid"] = QString("%1-4").arg(timestamp);
            message["jid"] = gjid;
            message["author"] = gjid;
            message["timestamp"] = timestamp;
            message["data"] = tr("%1 joined group").arg(getNicknameByJid(jid));
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            setLastMessage(gjid, QString::number(timestamp));
            setPropertyByJid(gjid, "lastmodel", message);

            query.clear();
            query["type"] = QueryType::MessageReceived;
            query["jid"] = gjid;
            query["message"] = message;
            dbExecutor->queueAction(query);

            if (gjid != _activeJid) {
                message["progress"] = 0;
                MessagesBaseModel::GetInstance()->notify(message);

                setUnread(jid, _modelData[gjid]["unread"].toInt() + 1);
            }
        }
    }
}

void ContactsBaseModel::groupParticipantRemoved(const QString &gjid, const QString &jid)
{
    if (_modelData.contains(gjid)) {
        qDebug() << gjid << jid;
        QStringList participants = _modelData[gjid]["participants"].toStringList();
        if (participants.contains(jid)) {
            participants.removeAll(jid);
            setPropertyByJid(gjid, "participants", participants);

            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = gjid;
            query["property"] = "participants";
            query["value"] = participants;
            dbExecutor->queueAction(query);

            QVariantMap message;
            uint timestamp = QDateTime::currentDateTime().toTime_t();
            message["msgid"] = QString("%1-5").arg(timestamp);
            message["jid"] = gjid;
            message["author"] = gjid;
            message["timestamp"] = timestamp;
            message["data"] = tr("%1 left group").arg(getNicknameByJid(jid));
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            setLastMessage(gjid, QString::number(timestamp));
            setPropertyByJid(gjid, "lastmodel", message);

            query.clear();
            query["type"] = QueryType::MessageReceived;
            query["jid"] = gjid;
            query["message"] = message;
            dbExecutor->queueAction(query);

            if (gjid != _activeJid) {
                message["progress"] = 0;
                MessagesBaseModel::GetInstance()->notify(message);

                setUnread(jid, _modelData[gjid]["unread"].toInt() + 1);
            }
        }
    }
}

void ContactsBaseModel::groupParticipantPromoted(const QString &gjid, const QString &jid)
{
    if (_modelData.contains(gjid)) {
        qDebug() << gjid << jid;
        QStringList admins = _modelData[gjid]["admins"].toStringList();
        if (!admins.contains(jid)) {
            admins.append(jid);
            setPropertyByJid(gjid, "admins", admins);

            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = gjid;
            query["property"] = "admins";
            query["value"] = admins;
            dbExecutor->queueAction(query);

            QVariantMap message;
            uint timestamp = QDateTime::currentDateTime().toTime_t();
            message["msgid"] = QString("%1-6").arg(timestamp);
            message["jid"] = gjid;
            message["author"] = gjid;
            message["timestamp"] = timestamp;
            message["data"] = tr("%1 is now group admin").arg(getNicknameByJid(jid));
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            setLastMessage(gjid, QString::number(timestamp));
            setPropertyByJid(gjid, "lastmodel", message);

            query.clear();
            query["type"] = QueryType::MessageReceived;
            query["jid"] = gjid;
            query["message"] = message;
            dbExecutor->queueAction(query);

            if (gjid != _activeJid) {
                message["progress"] = 0;
                MessagesBaseModel::GetInstance()->notify(message);

                setUnread(jid, _modelData[gjid]["unread"].toInt() + 1);
            }
        }
    }
}

void ContactsBaseModel::groupParticipantDemoted(const QString &gjid, const QString &jid)
{
    if (_modelData.contains(gjid)) {
        qDebug() << gjid << jid;
        QStringList admins = _modelData[gjid]["admins"].toStringList();
        if (admins.contains(jid)) {
            admins.removeAll(jid);
            setPropertyByJid(gjid, "admins", admins);

            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = gjid;
            query["property"] = "admins";
            query["value"] = admins;
            dbExecutor->queueAction(query);

            QVariantMap message;
            uint timestamp = QDateTime::currentDateTime().toTime_t();
            message["msgid"] = QString("%1-7").arg(timestamp);
            message["jid"] = gjid;
            message["author"] = gjid;
            message["timestamp"] = timestamp;
            message["data"] = tr("%1 is not group admin anymore").arg(getNicknameByJid(jid));
            message["status"] = 1;
            message["msgtype"] = 6;
            message["params"] = QString();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["recepients"] = 0;

            setLastMessage(gjid, QString::number(timestamp)); // TODO: FIX conversion
            setPropertyByJid(gjid, "lastmodel", message);

            query.clear();
            query["type"] = QueryType::MessageReceived;
            query["jid"] = gjid;
            query["message"] = message;
            dbExecutor->queueAction(query);

            if (gjid != _activeJid) {
                message["progress"] = 0;
                MessagesBaseModel::GetInstance()->notify(message);

                setUnread(jid, _modelData[gjid]["unread"].toInt() + 1);
            }
        }
    }
}

void ContactsBaseModel::groupInfo(const QString &jid, const QVariantMap &attributes)
{
    if (_modelData.contains(jid)) {
        qDebug() << jid;
        if (setPropertyByJid(jid, "owner", attributes["creator"])) {
            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = jid;
            query["property"] = "owner";
            query["value"] = attributes["creator"];
            dbExecutor->queueAction(query);
        }
        if (setPropertyByJid(jid, "participants", attributes["participants"])) {
            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = jid;
            query["property"] = "participants";
            query["value"] = attributes["participants"];
            dbExecutor->queueAction(query);
        }
        if (setPropertyByJid(jid, "admins", attributes["admins"])) {
            QVariantMap query;
            query["type"] = QueryType::ContactSetProperty;
            query["jid"] = jid;
            query["property"] = "admins";
            query["value"] = attributes["admins"];
            dbExecutor->queueAction(query);
        }
    }
}

void ContactsBaseModel::textMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const QString &data)
{
    QVariantMap query;
    query["type"] = QueryType::MessageReceived;
    query["jid"] = jid;

    QVariantMap message;
    message["msgid"] = id;
    message["jid"] = jid;
    message["author"] = author.isEmpty() ? jid : author;
    message["timestamp"] = timestamp.toUInt();
    message["data"] = data;
    message["status"] = 1;
    message["msgtype"] = 0;
    message["params"] = QString();
    message["deliveryreceipts"] = QVariantMap();
    message["readreceipts"] = QVariantMap();
    message["recepients"] = 0;

    query["message"] = message;

    dbExecutor->queueAction(query);

    if (_modelData.contains(jid)) {
        setLastMessage(jid, timestamp);

        setPropertyByJid(jid, "lastmodel", message);
    }
    else {

    }

    if (jid != _activeJid) {
        message["progress"] = 0;
        MessagesBaseModel::GetInstance()->notify(message);

        setUnread(jid, _modelData[jid]["unread"].toInt() + 1);
    }
}

void ContactsBaseModel::mediaMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const AttributeList &attrs, const QByteArray &data)
{
/* MEDIA NODES EXAMPELS

Image:
<message from="79049347231@s.whatsapp.net" id="1427554311-1" notify="andrey" t="1427555015" type="media">
    <media caption="Wallpaper" encoding="raw" file="AkFb7htKKfH7VVwv93vAAHmuWFHg-WVdJshH_l6Io5Je.jpg" filehash="NgFVCh7vB3wckwQXsKK1jlMxm4unK4Fe0/zy2mwKS6g=" height="959" ip="184.173.204.39" mimetype="image/jpeg" size="129997" type="image" url="https://mmi615.whatsapp.net/d/D173b0oxX1QuLxPaGybzIFUWwscABRJajZ0psQ/AkFb7htKKfH7VVwv93vAAHmuWFHg-WVdJshH_l6Io5Je.jpg" width="1280">
        data:hex:ffd8ffe000104a46494600010100000100010000ffdb00430006040506050406060506070706080a100a0a09090a140e0f0c1017141818171416161a1d251f1a1b231c1616202c20232627292a29191f2d302d283025282928ffdb0043010707070a080a130a0a13281a161a2828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828ffc0001108003c005103012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f006c792726ac264d40bd2ac478ae735489e31c55a838208e7daabc557ad503b853904fb5520d095632e37221c74a922528723381d78a98068498f24124e71d08ed53471ed28ee9f267d339f6fe5f9d508a538de37018ed8aa7283939ad4b84404ed18040c0ed545e32431f4a61733df35566048e0e0d5c947e7555f8078a04d917cde828a939a28d08d4aca79e6a78aaa86e6a68dab3344cbf036d39c55db50cee0a839c8e4566c4dc55a8662bf758afd0d08573544bb896c019ed5a96375035bb43728a14f21b1dfa738fc6b12274583cc2c37038033d3eb4e47251b1d0000e4d5a22e4eced19271c8f5aa771296c81c0c9e0548aea64c39cfeb54e771b9803c52b8c8243d6ab354923f5a87775a68570dc7d0d14b93451a13af73315fe7a9e335481e49a9836054b45a66844f8ab024c9e4f159b1cbc66a6496901a4b2750a4e31dfbd491cec14aab9009acd1271d6a459b19cd2e6048b6643d6a291f39a84ca0d46d252b8c576a88b609a6b383919a8cb0e455262689770f5a2abe3de8a7733b1411f76706a47395eb50a8c135229a6cd522ca1c0e4fe3522b71eb5547a5363621f19ce4d4858b81ce48ed5279a00c93c55504f1cd2839fcea58d22cf9b91c5217f435594e07e34c9d8a818a92ad62c92064f7a87796c9e82991924724f3499a2e2b136ea2a0f30fb5147391ca7ffd9
    </media>
</message>

Video:
<message from="79049347231@s.whatsapp.net" id="1427554311-2" notify="andrey" t="1427555107" type="media">
    <media abitrate="12" acodec="aac" asampfmt="flt" asampfreq="8000" caption="Clockwork" duration="5" encoding="raw" file="AiVsFpwb35jloDSflMl2grKjxybjtqjurxNayhHhwzUu.mp4" filehash="DeEtHzCamQNeYNpmiOYDcT0lbw2BOq5wg4wGj6vrRNg=" fps="31" height="288" ip="184.173.195.51" mimetype="video/mp4" seconds="4" size="772958" type="video" url="https://mmv644.whatsapp.net/d/grCpJmyWetoMNdJUIKjldlUWwyMABRJakw5zrw/AiVsFpwb35jloDSflMl2grKjxybjtqjurxNayhHhwzUu.mp4" vbitrate="726" vcodec="h264" width="352">
        data:hex:ffd8ffe000104a46494600010100000100010000ffdb00430006040506050406060506070706080a100a0a09090a140e0f0c1017141818171416161a1d251f1a1b231c1616202c20232627292a29191f2d302d283025282928ffdb0043010707070a080a130a0a13281a161a2828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828ffc00011080064006403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00e595b8229c0fa54bf27468303fd96cff00850a909cff00ad5fa8cff2ae6b336b918268dd9a93c9427227403fda1b7f9d28b563f719587fb2699574439a3273521b6957f8093513c7229f99187d45002b36077a8f7714849c1e292a530b9207f5a766a1069ca69a61724cfe74869bbbe9466989bb8514dcd14856340276cd2888f61c55c483e727a54822abb10669057823f1a3cb0c3a03f8569f92315134583c03cd1a8148065e15997d81c506494702461f80357047c66b93f136bb3c7a88d27468964bdda1a595c12b083d38eec720fa723ae78560bd8e844ac460846fa8a6b18c8f9e143f4245724961acbee79f57baf3900dca9b631cff00b210823f135235eeb7a5be6e425fdb8c6e560a927fc05970a7e8c067d68d014ce9fca808ff0056ebf46cd37c983fbf203e8573fca9ba5dedb6a962b7564e5e324ab29186461d5587661e9fd0835696327a8228b0f98a660439d930ff0081022982093a6f8cff00bad571a2ebc5426020f4c8a2c1cc5736d3e4e133f43454c60c9fbb455226e749e581da9cb1e7b55a1183ce3269eb1e3b0abb0ae513115e99a708b2338abde56697caa560b99b24442935e61a3ceb6fe3ad5d2e5c2c9713028cfd3003003f0257f0cfa57af34395238ae0bc6fe0f92fa4379640f9e3aa8e09f71fe1fe49625ec684d6f27d9961d3ad20650a70cede5c6a7278c004f5ed8c7bd2cd6f33da9332c283a143d0fe39e9583a0789eef4e8ae2db5f864b836f17ee48dab2eec81b58315c8f7c923d08e956f3c7864882c1a6ccb2052371903e7f0c000fbf3f43459116626900e93f100d9c64f917f0b6e5ff6932558fe008aeffcaca935ca781b47bdbbd525d7b564f2e478fc9b68b9f91339279e4fd7be4f6c577861f971d2a6c691323c8273bbf0a6bc61477ad6f278c1a8da1e3a55243327c9eb455f31004f4a29d80db54a708f3dea68c647b548a839aab19f31008714f58ea754c835284041e07e54ac3b94c45d7bd218411cd5d09d69e62181c5520316ef4ab5bb502e6de19b1d04881b1f4c8aad1681a742e1e2b0b55707219615047d0e38ae87cbff0038a3cb1d6959019c20c546d1107d6b4cc63d2a368f9a92919a533c639a6345c1abd2260123ad44149ea714215ccd6b604f53456814e4d15570b96d3d00c548b51a678ab50c5bc8e734ccee2a60738a701cf3de94ae383da94022958771547ad3d54e791ed4d407760d5c84c3bb6e4838e0b5304ca9b486208a691d78abd3c0464ae0af639aaadc1c1ed4ae522065e2a36156090a3daa06a81a217e41c8e6a22315337435131e2801981e94535b93d07e3453e60255ed8ab10b329f90e0fad4099e2a78bad6a665c8a4670c0804f5071e9ffd6cd0b1b3066dbf28c678e9496cbbe554071bcedcfa678cd6acac642c172817a12ca013c0ee738e2848933100e78a78e187a839e280724938e4fd2a58a3691b8042646e6ecb93de958ab8a6601148396cf4619aab26371c0e335627b77888de073903072320e0d472050f273955279f5f4a868a456947007e3511e952c9efd6a26e054d8644ff00748ee2a06a95bbd42f40c617009eb4526ccf24d1458352d0ab4a83ca56e739c51456c664f6e3f7a47aab7fe826a4d831f4a28a7d0942a0ea6b434ab649ae407cf1133f1ea08e3e945142298b75cab6705a198aa3600206c63f4eaa3f2aceb8014845e1463f5a28a96112a30c939ec714d9540268a2b32d6c5793eed407bd1454b18d1de8a28a067fffd9
    </media>
</message>

Audio:
<message from="79049347231@s.whatsapp.net" id="1427554311-3" notify="andrey" t="1427555149" type="media">
    <media abitrate="12" acodec="amrnb" asampfmt="flt" asampfreq="8000" duration="7" encoding="raw" file="Ah2EAKyy0h1eD2g7pjoRW5qKKtlotsxxKqdr1W_8G-Rd.3ga" filehash="lD5QiglLnEFNuK8TXi5xI6qQyzZDueViy5EBfaAI+Js=" ip="174.37.199.195" mimetype="audio/3gpp" seconds="7" size="112599" type="audio" url="https://mms893.whatsapp.net/d/lAHIg468c0hoHNHyfDkK7VUWw00ABRJalZobDA/Ah2EAKyy0h1eD2g7pjoRW5qKKtlotsxxKqdr1W_8G-Rd.3ga" />
</message>

Location (point):
<message from="79049347231@s.whatsapp.net" id="1427554311-4" notify="andrey" t="1427555197" type="media">
    <media encoding="raw" latitude="55.1908686" longitude="61.2998874" type="location">
        data:hex:ffd8ffe000104a46494600010100000100010000ffdb00430006040506050406060506070706080a100a0a09090a140e0f0c1017141818171416161a1d251f1a1b231c1616202c20232627292a29191f2d302d283025282928ffdb0043010707070a080a130a0a13281a161a2828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828ffc00011080064006403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fa0e3b21a7082792696f2e132239ae5d6536c180dcaafb431071d5b3d07d2a2b5b78e18161b64315bae481925989249249e79249c9e6afdbaadbc0e2e48489f8119ed514919864d84ee52328dea3fc6b2650d501400a30052d145480841c82a70c3a11daa791b72add20c1fbb22d434f824114877ffab7e1b3fce9a01256dec0afca47434f9889615b9518390ae3d79c51f6611e4cee3c91d31d5bda992ca65c0c6c8c7dd41fd6801aaa49c28e6acc7185258f2c6a281c2920f43deacd340145145020a28a280219a30c0b0e587eb51c18913ecd21c7789bd0fa7f9ed4b1978580951915bd68b88ff894e39ce4763eb40c6c50c8e4861e585e198f4fc298a720f39193838c647ad4d23b5cdb647df8cfce83f8bdff00ad449f3e3691cf4a4014c96448a36795d511464b31c003dcd3ebcd3e206aeadaa5dc373bbec5a6c42468fa8772bb8b11df03007be6b3a93e4573ab078578aa9c89dbabf43bdb3d474ebb97cbb3bdb59e41fc314aac47e00d5eaf9e3c2fe32b6f145ccd6eb6b2da4f02f9b1c81f2c390372903e520915ee5e16bf9752d06ceeae42f9eea4395e84a92a4fe38cd4d3a9ccecd6a7463700b0f055212bc5e86ad4f0499f95ba8e9ef5051f4ad91e697691885192702a35986cc9fbc3b7ad40ee5ce5bf2a7711234ed9f94003de8a8a8a57192c326ecc33b1647e8c7a834a8c62668a6ea3a1f51501008c1e9562302e2db3392be59c093a6450808a0ddf6906004907047fb3ef4eba8bc993728fdd39ff00be5bd2a652366c8879717fe3c6a08b10968261fb97e3e87d7fcf7a6033af03ad79f7c4bd2fcb64d56345681d4417431d074563edced3f515e8455a194a3f24743fde150dedac5776b2dbccbba1990a30f63c1acea439e2e27560f12f0b59545fd23c1343d16d6daf05be87671c77776c106dc9cfb9f451c93ec2bdd348b08b4bd32daca0e638502e4ff11ee7f13935c57c34d1fecfa86ab7171f3cb6929b34279c11cb1fae36f3f5af41ac70f069734b767a39d62e356a2a54be18fe6c28a28ae83c40a28a4a005a29c914d22ee441b4f42c719a29d805822f3577c9c423ff001eff00eb54d2b0c02e0051f75292790ba473c67e4030ca7f84d5666e72c49269ec0399998e49fa7b54b813c641c6e14df295137dcb1407eea2f5a4917c875646dd1b0cab7f4a402c7fbe4fb3cad891798dcff2a8d09cb238da41c30f43eb524c8244de990473c7507d686cdcc65d40fb44630cbfde14c0ab6f6b15ab4de4a0432c8657c127731ea7f41535229046474a5a91b6dee14514b1a34adb63193dcf6140869382060927a01dea6112c403dcf24fdd88739faff9c51948011166493a349e9ed51b0f98b1258b73b8d3d8093cf66c970727a007a0a2a2a28b80e86431331dbb9186197d69cb2c51f304043e382e738fd4d364428d8ea3b1a6d0021cb31672598f7352c0eb8304bfeadbee9fee9a8e9080460f4a00914b412b23f51fa8f5a245689c4b111ede9f4fa53d3fd223f2d8fef906518ff10a6c0fd51c719c60f634c0498295fb445feadbefaff74fad46480324f1522b1b69493cc6df787a8f5fad3cc70db01264ca18feed7b0a560191c2597cc94f9708ee78269e5f747841e4db8f4e0b5465cc8fbe73b88fbaa3a0a1d8961bc1673f750534029395c0f9231c0029a8460ab1f94f7f434db9df014336df9bf841e4525908e73229f9263ca9cf1f4a07caed71c410483d451479eb1e526405d78e7b51482cfb01249e4e68a293349bb0242d1480e69684ee0272082a70c0e41a9a40278ccd1ae1d78917fad454a8ed1389173d7691ebed4d08923612c7b49f9ba834d870435acdc2b1f90ff75bfcf229658f65d32c5e8187b6734b74aa5464fcded4c080b189984980ebc1ff001152193ecb6ab22f334dc827b0a4950de404e3fd2621cffb43fcfeb4965225cc22da6c657943fd291492b5c8e245b88d8b31331ea49a7186382224b624ecddf3ed532471c4e40015cf18aab7b1b09377241e07b5052d5dba17edfc9bc88493468641f29cd159aab7110c28201e7a668a7725c75d09c9a4a28ac25b94b601d69f4515502643e550a57031915259a816a67eb21c9e7a0a28ad56e4f41cc4ac4cdd5b19c9aa92310acdd4f5e68a29304695a4291c619465980249ea6b33548c4574ad1fca586e38f5cf5a28a6f62a1f1125efef2ca0b83c4bc722a5858bc4acdd48a28a03a141e691dd8ee23071814514522d2563ffd9
    </media>
</message>

Location (place):
<message from="79049347231@s.whatsapp.net" id="1427554311-5" notify="andrey" t="1427555229" type="media">
    <media encoding="raw" latitude="55.190575" longitude="61.299274" name="Рифарм, Аптека, ЗАО
Комсомольский проспект, 101а, Челябинск, Челябинская область, Россия, 454000" type="location" url="https://plus.google.com/118018086544861474177/about?hl=ru">
        data:hex:ffd8ffe000104a46494600010100000100010000ffdb00430006040506050406060506070706080a100a0a09090a140e0f0c1017141818171416161a1d251f1a1b231c1616202c20232627292a29191f2d302d283025282928ffdb0043010707070a080a130a0a13281a161a2828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828ffc00011080064006403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f7fb4821b1f3cd9c970ed36d135ddc4a5e5902e76804f45193f993d4926d436e368f30158fb20eadfe02a58a108d97c3ca3a0ecbfe26964936679dce6b2286bdc4b1c830a046bc7960751514c82260d1e0c1272a4763e95390268f23ef0a8a02a375bcbc4521c0ff0065bfcfeb40111603a9a754b3158435bc40ee23e776ea41a8a90052c6ed13965018118653de928a403a494ba79688228bfbabde98381c52d1400f89f637fb27ad5a1cf4aa552c1263e43d3b534c0b1451453115e49828da9f4e3bfd293ca58d77dd16c9e91a9e69ec16d3000df3b0fbc4700557e4b1662598f52690c94810859a262d037073fc34b3c62442c390473ef4c8641133071985f8607b7bd39a39637f2a30cca7953edee69800dd7316d247da621907fbe3fcfeb5019a38e179252a88064b39c05fad59016d416ff5b3f4f40b5e5ff10f598a5d5ef1650e2cb4f884af1750f215dc588ee40c01ef9359d49f22b9d583c2bc554e44edd59df5a6a96179218ed2fad67907558a55623f006aee7030bd3b9ee6be77f0bf8cadbc517335badacb693c0be6c7207cb0e71b9481f29048af72f0b5fcba968367757217cf7521caf42549527f1c66a29d5e66e2d6a7463300b0f055212bc5e86ad14515a9e6851451400f12be3ef7e945328a77025888953ecf21f98731bff4a8790c55c61d782282030ebf88ed53a85bb00337973af0481f787f9fca8dc08a38da662898ff00698f4156776d8c47131589060be793f4a4df1a86893e5443823bb1a81dcbfb0ec29ec04892267605c21e2bcebe27e906de55d55115ade4510dcf1f82b1fcf69fa8aef943302511997d40c8a27821d46c66b5b91ba3950a1f70462b3a90f69171674e0f12f0b59555d37f43c0343d1ad6d6f05be87671c77776c106dc9cfb9f451c93ec2bdd749d3e2d334cb6b3b7cf970a05c9ea4f73f89c9fc6b8bf863a3241a9eacf7c0b3dbca6c637e3e56182c7ea7e5e7fc6bbfc3452347272c3affb43d6b2c3536a3cd2dd9e96758b8d5a8a952f863f9b0a28a2b73c40a28a4240a005a2a44b495c6e6758f3d148c9a29d80880c5073c1070c0e41f434b41041c1073e9492b6c3b933a9b951344bfbd1f2ba67ad3441b577dd1d89fdc0725aa2c60e54b29f5524518e72492dea4e4d31129b99372b20088bd10771ef44ea0113c47f74e727fd9351d3a19044c438cc2fc303dbde8b80c82ce058e7304611a573248013f331ea7f4152a7fa447e539c4e9ca31fe21eff00d69195ada6033953f74fa8f4fad2cc9b809633823904763eb4c1b6f722539ce410c0e083d8d2d48ffbf8ccd18c4c9c4883bfb8a6c5119577b37970f5dc7827e94ac03515a47d918dcddfd07d6a740b03958ff7b71dd8f45a4dfb90a403ca80756eed4c18dbb506c8c77f5a76011d63662652f23f76a29be628fbbb31fed7268a007c6ade670391d69fe70f33a71d09ef4d791d64e7b76a631dcc49ef4b6011b1b8ed181da8a4a2900b451450049091227d9e4e07fcb36f43e9491334723249c11c30feb5191918fe5537fc7cc7ff004f118fa6e14d00d70d04a248f9f6f51e94b787cc31ca1b740c38ff0064d103875d8dd0f4f6a488889da29b9864e0fb1f5fc6980cdc027cc7e55ed526d44412dd1e0fdc885569b7dbdc2c72f28ac0e7fbc3352ea10c92ceb2210d1b2fca73d282947b90cf0c92ca5c44101e8a3b5156edfccf287983e6a28b0d546b4222493927268a29a4d4376252b8a4d368a2b26db2d2b0ab4e5058803a9a68e3eb403cd5a76d192d5c563b73bb8c7ad58b78ca324cf95c0385ee692260227b8750ce87681e9ef4e6946dde4ee735aa2445458b73bf2c49207a64e6a099bcc38619cf01477a51be6936a0dcfdfd0549b92d8958f124fd19cf45ff003e94807bda3cd64ab291e72fdd24e7f0355ac27d84db4f9009c0cff09ab1632c9e7b212d203c927f84ff009ed46a769e68f36219900e40fe21fe34fcd0d3e8cad752cf0cc50a8c762067228ad1b3f305bafda31bff005c7bd14587cd6e850a651456531c429c0628a294024368a28a8289ec4fefca755753b81f6a831893cb0481e66ccf7c51456eb6466f72d5d9fb3ac70c3f22be72475fceaab7ca985e3903f5a28a6c48b577fb858e18be546ce48ea7f1a974f766848639d8db41f6a28a7d43a199ace52f3866e541ebd3ad1451498d1fffd9
    </media>
</message>

Contact:
<message from="79049347231@s.whatsapp.net" id="1427554311-6" notify="andrey" t="1427555264" type="media">
    <media encoding="text" type="vcard">
        <vcard name="T Mavica">
            data:hex:424547494e3a56434152440a56455253494f4e3a332e300a4e3a542e4d61766963613b3b3b3b0a464e3a54204d61766963610a6974656d312e54454c3a2b38353236313339393531310a6974656d312e582d41424c6162656c3ad094d0bed0bcd0b0d188d0bdd0b8d0b90a454e443a5643415244
        </vcard>
    </media>
</message>

*/

    /*
    enum MessageType {
        TextMessage = 0,
        ImageMessage,
        VideoMessage,
        AudioMessage,
        LocationMessage,
        VCardMessage,
        InformationMessage
    };
    */

    QVariantMap query;
    query["type"] = QueryType::MessageReceived;
    query["jid"] = jid;

    QVariantMap message;
    message["msgid"] = id;
    message["jid"] = jid;
    message["author"] = author.isEmpty() ? jid : author;
    message["timestamp"] = timestamp.toUInt();
    message["data"] = data.toBase64();
    message["status"] = 1;
    QString mediaType = attrs["type"].toString();
    if (mediaType == "image") {
        message["msgtype"] = 1;
    }
    else if (mediaType == "video") {
        message["msgtype"] = 2;
    }
    else if (mediaType == "audio") {
        message["msgtype"] = 3;
    }
    else if (mediaType == "location") {
        message["msgtype"] = 4;
    }
    else if (mediaType == "vcard") {
        message["msgtype"] = 5;
    }
    message["params"] = QString(QJsonDocument::fromVariant(attrs).toJson(QJsonDocument::Compact));
    message["deliveryreceipts"] = QVariantMap();
    message["readreceipts"] = QVariantMap();
    message["recepients"] = 0;

    query["message"] = message;

    message["params"] = attrs;

    dbExecutor->queueAction(query);

    if (_modelData.contains(jid)) {
        setLastMessage(jid, timestamp);

        setPropertyByJid(jid, "lastmodel", message);
    }
    else {

    }

    if (jid != _activeJid) {
        message["progress"] = 0;
        MessagesBaseModel::GetInstance()->notify(message);

        setUnread(jid, _modelData[jid]["unread"].toInt() + 1);
    }
}

void ContactsBaseModel::textMessageSent(const QString &jid, const QString &id, const QString &timestamp, const QString &data)
{
    QVariantMap query;
    query["type"] = QueryType::MessageSent;
    query["jid"] = jid;

    QVariantMap message;
    message["msgid"] = id;
    message["jid"] = jid;
    message["author"] = connection->myJid();
    message["timestamp"] = timestamp.toUInt();
    message["data"] = data;
    message["status"] = 0;
    message["msgtype"] = 0;
    message["params"] = QString();
    message["deliveryreceipts"] = QVariantMap();
    message["readreceipts"] = QVariantMap();
    message["recepients"] = _modelData[jid]["participants"].toList().count();

    query["message"] = message;

    dbExecutor->queueAction(query);

    if (_modelData.contains(jid)) {
        setLastMessage(jid, timestamp);

        setPropertyByJid(jid, "lastmodel", message);
    }
    else {

    }
}

void ContactsBaseModel::messageSent(const QString &jid, const QString &msgId, const QString &timestamp)
{
    qDebug() << jid << msgId << timestamp;
    QVariantMap query;
    query["type"] = QueryType::MessageStatus;
    query["jid"] = jid;
    query["msgid"] = msgId;
    query["timestamp"] = timestamp;
    query["status"] = 1;
    dbExecutor->queueAction(query);

    if (_modelData.contains(jid)) {
        QVariantMap message = _modelData[jid]["lastmodel"].toMap();
        if (message["msgid"].toString() == msgId) {
            message["status"] = 1;
            setPropertyByJid(jid, "lastmodel", message);
        }
        else {
            qDebug() << "not lastmessage" << jid << msgId;
        }
    }
    else {
        qDebug() << "no message" << jid << msgId;
    }
}

void ContactsBaseModel::messageReceipt(const QString &jid, const QString &msgId, const QString &participant, const QString &timestamp, const QString &type)
{
    if (_modelData.contains(jid)) {
        qDebug() << jid << msgId << participant << type;

        QString receiptType = type == "read" ? "readreceipts" : "deliveryreceipts";
        QVariantMap receipt = _modelData[jid][receiptType].toMap();
        QString from = participant.isEmpty() ? jid : participant;
        receipt[from] = timestamp.toUInt();

        QVariantMap query;
        query["type"] = QueryType::MessageReceipt;
        query["jid"] = jid;
        query["msgid"] = msgId;
        query["receiptType"] = receiptType;
        query["receipt"] = QString(QJsonDocument::fromVariant(receipt).toJson(QJsonDocument::Compact));
        dbExecutor->queueAction(query);

        QVariantMap message = _modelData[jid]["lastmodel"].toMap();
        if (message["msgid"].toString() == msgId) {
            message[receiptType] = receipt;
            setPropertyByJid(jid, "lastmodel", message);
        }
        else {
            qDebug() << "not lastmessage" << jid << msgId;
        }
    }
    else {
        qDebug() << "no message" << jid << msgId;
    }
}

void ContactsBaseModel::retryMessage(const QString &msgId, const QString &jid)
{
    QVariantMap query;
    query["type"] = QueryType::RetryMessage;
    query["msgid"] = msgId;
    query["jid"] = jid;
    query["object"] = qVariantFromValue(this);
    query["method"] = QUERYREPLY(onRetryMessage);
    dbExecutor->queueAction(query);
}

void ContactsBaseModel::connectionStatusChanged()
{
    if (connection->connectionStatus() == MainConnection::LoggedIn) {
        if (_modelData.size() == 0) {
            //connection->getGroups("participating");
        }
    }
}

bool ContactsBaseModel::getAvailable(const QString &jid)
{
    return _availableContacts.contains(jid);
}

bool ContactsBaseModel::getBlocked(const QString &jid)
{
    return _blockedContacts.contains(jid);
}

QString ContactsBaseModel::getNicknameBy(const QString &jid, const QString &message, const QString &name, const QString &pushname)
{
    QString nickname;
    if (jid.contains("-")) {
        nickname = name;
    }
    else if (name == jid.split("@").first() || name.isEmpty()) {
        if (!pushname.isEmpty())
            nickname = pushname;
        else
            nickname = jid.split("@").first();
        qDebug() << jid;
    }
    else {
        nickname = name;
    }
    return nickname;
}

void ContactsBaseModel::pictureUpdated(const QString &jid, const QString &path)
{
    if (_modelData.contains(jid)) {
        setPropertyByJid(jid, "avatar", path);
    }
}

void ContactsBaseModel::groupInfo(const QVariantMap &data)
{
    QString jid = data["jid"].toString();
    if (_modelData.contains(jid)) {
        QString participants = data["participants"].toStringList().join(";");
        if (_modelData[jid]["nickname"] == data["message"]
             && _modelData[jid]["pushname"] == data["pushname"]
             && _modelData[jid]["owner"] == data["owner"]
             && _modelData[jid]["message"] == data["message"]
             && _modelData[jid]["subowner"] == data["subowner"]
             && _modelData[jid]["subtimestamp"] == data["subtimestamp"]
             && _modelData[jid]["timestamp"] == data["timestamp"]
             && _modelData[jid]["phoneavatar"] == data["phoneavatar"]
             && _modelData[jid]["participants"].toString() == participants)
            return;
        qDebug() << "change group info for" << jid;

        if (_modelData[jid]["nickname"] != data["message"]) {
            _modelData[jid]["nickname"] = data["message"];
            Q_EMIT propertyChanged(jid, "nickname", data["message"]);
        }
        if (_modelData[jid]["pushname"] != data["pushname"]) {
            _modelData[jid]["pushname"] = data["pushname"];
            Q_EMIT propertyChanged(jid, "pushname", data["pushname"]);
        }
        if (_modelData[jid]["owner"] != data["owner"]) {
            _modelData[jid]["owner"] = data["owner"];
            Q_EMIT propertyChanged(jid, "owner", data["owner"]);
        }
        if (_modelData[jid]["message"] != data["message"]) {
            _modelData[jid]["message"] = data["message"];
            Q_EMIT propertyChanged(jid, "message", data["message"]);
        }
        if (_modelData[jid]["subowner"] != data["subowner"]) {
            _modelData[jid]["subowner"] = data["subowner"];
            Q_EMIT propertyChanged(jid, "subowner", data["subowner"]);
        }
        if (_modelData[jid]["subtimestamp"] != data["subtimestamp"]) {
            _modelData[jid]["subtimestamp"] = data["subtimestamp"];
            Q_EMIT propertyChanged(jid, "subtimestamp", data["subtimestamp"]);
        }
        if (_modelData[jid]["timestamp"] != data["timestamp"]) {
            _modelData[jid]["timestamp"] = data["timestamp"];
            Q_EMIT propertyChanged(jid, "timestamp", data["timestamp"]);
        }
        if (_modelData[jid]["phoneavatar"] != data["phoneavatar"]) {
            _modelData[jid]["phoneavatar"] = data["phoneavatar"];
            Q_EMIT propertyChanged(jid, "phoneavatar", data["phoneavatar"]);
        }
        if (_modelData[jid]["participants"].toString() != participants) {
            _modelData[jid]["participants"] = participants;
            Q_EMIT propertyChanged(jid, "participants", participants);
        }
        _modelData[jid]["typing"] = false;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
    }
    else {
        qDebug() << "create group info for" << jid;
        beginResetModel();

        _modelData[jid] = data;
        _modelData[jid]["blocked"] = false;
        _modelData[jid]["available"] = false;
        _modelData[jid]["typing"] = false;
        _modelData[jid]["nickname"] = data["message"];
        checkLastMessage(jid);

        endResetModel();

        Q_EMIT countChanged();
    }
}

void ContactsBaseModel::contactChanged(const QVariantMap &data)
{
    QVariantMap contact = data;
    QString jid = contact["jid"].toString();
    qDebug() << "contact changed" << jid;

    QString name = contact["name"].toString();
    QString message = contact["message"].toString();
    QString pushname = contact["pushname"].toString();
    QString nickname = ContactsBaseModel::getNicknameBy(jid, message, name, pushname);

    contact["nickname"] = nickname;
    contact["typing"] = false;
    bool available = getAvailable(jid);
    contact["available"] = available;
    bool blocked = false;
    if (!jid.contains("-"))
        blocked = getBlocked(jid);
    contact["blocked"] = blocked;
    contact["lastmodel"] = _modelData["lastmodel"];

    if (_modelData.contains(jid)) {
        if (_modelData[jid] == contact)
            return;
        _modelData[jid] = contact;
        checkLastMessage(jid);

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
    }
    else {
        beginResetModel();
        _modelData[jid] = contact;
        checkLastMessage(jid);
        endResetModel();

        Q_EMIT countChanged();
    }
}

void ContactsBaseModel::contactSynced(const QVariantMap &data)
{
    QVariantMap contact = data;
    QString jid = contact["jid"].toString();
    if (_modelData.contains(jid)) {
        if (_modelData[jid]["name"] == contact["name"]
                && (contact["avatar"].toString().isEmpty()
                    || contact["avatar"] == _modelData[jid]["phoneavatar"]))
            return;
        QString name = contact["name"].toString();
        QString pushname = _modelData[jid]["pushname"].toString();

        qDebug() << "contact synced:" << name << pushname << jid;

        _modelData[jid]["nickname"] = ContactsBaseModel::getNicknameBy(jid, "", name, pushname);
        _modelData[jid]["phoneavatar"] = contact["avatar"];

        bool blocked = getBlocked(jid);
        _modelData[jid]["blocked"] = blocked;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
    }
}

void ContactsBaseModel::contactStatus(const QString &jid, const QString &message, int timestamp)
{
    if (_modelData.contains(jid)) {
        if (_modelData[jid]["subtimestamp"].toInt() == timestamp)
            return;
        Q_EMIT statusChanged(jid, message, timestamp);
        qDebug() << "contact status for" << jid << message;
        _modelData[jid]["message"] = message;
        _modelData[jid]["subtimestamp"] = timestamp;
        int row = _modelData.keys().indexOf(jid);
        dataChanged(index(row), index(row));

        Q_EMIT propertyChanged(jid, "message", message);
        Q_EMIT propertyChanged(jid, "subtimestamp", timestamp);
    }

    if (jid == connection->myJid()) {
        AccountSettings::GetInstance()->setValue("message", message);
    }
}

void ContactsBaseModel::newGroupSubject(const QVariantMap &data)
{
    QString jid = data["jid"].toString();
    if (_modelData.contains(jid)) {
        if (_modelData[jid]["message"] == data["message"]
             && _modelData[jid]["subowner"] == data["subowner"]
             && _modelData[jid]["subtimestamp"] == data["subtimestamp"])
            return;
        QString message = data["message"].toString();
        QString subowner = data["subowner"].toString();
        QString subtimestamp = data["subtimestamp"].toString();

        _modelData[jid]["message"] = message;
        _modelData[jid]["nickname"] = message;
        _modelData[jid]["subowner"] = subowner;
        _modelData[jid]["subtimestamp"] = subtimestamp;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));

        Q_EMIT propertyChanged(jid, "message", message);
        Q_EMIT propertyChanged(jid, "nickname", message);
        Q_EMIT propertyChanged(jid, "subowner", subowner);
        Q_EMIT propertyChanged(jid, "subtimestamp", subtimestamp);
    }
}

void ContactsBaseModel::contactsChanged()
{
    _busy = true;
    Q_EMIT busyChanged();

    QVariantMap query;
    query["type"] = QueryType::ContactsGetAll;
    query["object"] = qVariantFromValue(this);
    query["method"] = QUERYREPLY(onContactsGetAll);
    dbExecutor->queueAction(query);
}

int ContactsBaseModel::getParticipantIndex(const QString &cjid, const QString &pjid)
{
    if (_modelData.contains(cjid)) {
        int index = _modelData.value(cjid).value("participants").toString().split(";").indexOf(pjid) + 1;
        return index;
    }
    return 1;
}

void ContactsBaseModel::onContactsGetAll(const QVariantMap &reply)
{
    QVariantList contacts = reply["contacts"].toList();
    if (contacts.size() > 0) {
        beginResetModel();
        _modelData.clear();
        foreach (const QVariant &item, contacts) {
            QVariantMap data = item.toMap();
            QString jid = data["jid"].toString();
            QString pushname = data["pushname"].toString();
            QString name = data["name"].toString();
            QString message = data["message"].toString();
            QString nickname = ContactsBaseModel::getNicknameBy(jid, message, name, pushname);
            data["nickname"] = nickname;
            data["blocked"] = false;
            data["available"] = false;
            data["typing"] = false;
            data["avatar"] = data["avatar"].toString().split(";");
            data["participants"] = data["participants"].toString().split(";");
            data["admins"] = data["admins"].toString().split(";");
            _modelData[jid] = data;
        }
        endResetModel();

        Q_EMIT countChanged();
    }
    else {
        Q_EMIT contactsEmpty();
    }

    checkTotalUnread();

    _busy = false;
    Q_EMIT busyChanged();
}

void ContactsBaseModel::onRetryMessage(const QVariantMap &reply)
{
    qDebug() << reply;

    if (reply.contains("data")) {
        connection->sendRetryMessage(reply["jid"].toString(), reply["msgid"].toString(), reply["data"].toString());
    }
}

void ContactsBaseModel::onDownloadCompleted(const QString &jid, const QString &dest, const QString &msgid)
{
    if (_modelData.contains(jid)) {
        QVariantMap query;
        query["type"] = QueryType::MediaDownloadComplete;
        query["jid"] = jid;
        query["msgid"] = msgid;
        query["local"] = dest;
        dbExecutor->queueAction(query);
    }
    else {

    }
}

void ContactsBaseModel::addParticipant(const QString &gjid, const QString &jid)
{
    /*if (_modelData.contains(gjid)) {
        QStringList participants = _modelData[gjid]["participants"].toString().split(";");
        if (!participants.contains(jid)) {
            participants.append(jid);
            QString value = participants.join(";");
            setPropertyByJid(gjid, "participants", value);

            QVariantMap query;
            query["type"] = QueryType::ConversationParticipantsChanged;
            query["jid"] = gjid;
            query["participants"] = value;
            query["uuid"] = uuid;
            dbExecutor->queueAction(query);
        }
    }*/
}

void ContactsBaseModel::removeParticipant(const QString &gjid, const QString &jid)
{
    /*if (_modelData.contains(gjid)) {
        QStringList participants = _modelData[gjid]["participants"].toString().split(";");
        if (participants.contains(jid)) {
            participants.removeAll(jid);
            QString value = participants.join(";");
            setPropertyByJid(gjid, "participants", value);

            QStringList admins = _modelData[gjid]["pushname"].toString().split(";");
            if (admins.contains(jid)) {
                admins.removeAll(jid);
                setPropertyByJid(gjid, "pushname", admins.join(";"));

                QVariantMap query;
                query["type"] = QueryType::ConversationAdminChanged;
                query["jid"] = gjid;
                query["admins"] = admins.join(";");
                query["uuid"] = uuid;
                dbExecutor->queueAction(query);
            }

            QVariantMap query;
            query["type"] = QueryType::ConversationParticipantsChanged;
            query["jid"] = gjid;
            query["participants"] = value;
            query["uuid"] = uuid;
            dbExecutor->queueAction(query);
        }
    }*/
}

void ContactsBaseModel::adminSetParticipant(const QString &gjid, const QString &jid)
{
    /*if (_modelData.contains(gjid)) {
        QStringList participants = _modelData[gjid]["participants"].toString().split(";");
        if (participants.contains(jid)) {
            QStringList admins = _modelData[gjid]["pushname"].toString().split(";");
            admins.append(jid);
            setPropertyByJid(gjid, "pushname", admins.join(";"));

            QVariantMap query;
            query["type"] = QueryType::ConversationAdminChanged;
            query["jid"] = gjid;
            query["admins"] = admins.join(";");
            query["uuid"] = uuid;
            dbExecutor->queueAction(query);
        }
    }*/
}

void ContactsBaseModel::adminRemoveParticipant(const QString &gjid, const QString &jid)
{
    /*if (_modelData.contains(gjid)) {
        QStringList participants = _modelData[gjid]["participants"].toString().split(";");
        if (participants.contains(jid)) {
            QStringList admins = _modelData[gjid]["pushname"].toString().split(";");
            if (admins.contains(jid)) {
                admins.removeAll(jid);
                setPropertyByJid(gjid, "pushname", admins.join(";"));

                QVariantMap query;
                query["type"] = QueryType::ConversationAdminChanged;
                query["jid"] = gjid;
                query["admins"] = admins.join(";");
                query["uuid"] = uuid;
                dbExecutor->queueAction(query);
            }
        }
    }*/
}

void ContactsBaseModel::setLastMessage(const QString &jid, const QString &timestamp)
{
    setPropertyByJid(jid, "lastmessage", timestamp.toUInt());

    QVariantMap query;
    query["type"] = QueryType::ContactSetLastmessage;
    query["jid"] = jid;
    query["lastmessage"] = timestamp.toUInt();
    dbExecutor->queueAction(query);
}

void ContactsBaseModel::groupLocked(const QString &gjid)
{
    /*if (_modelData.contains(gjid)) {
        setPropertyByJid(gjid, "phoneavatar", "locked");

        QVariantMap query;
        query["jid"] = gjid;
        query["type"] = QueryType::ContactsUpdatePhoneavatar;
        query["phoneavatar"] = "locked";
        query["uuid"] = uuid;
        dbExecutor->queueAction(query);
    }*/
}

void ContactsBaseModel::groupUnlocked(const QString &gjid)
{
    /*if (_modelData.contains(gjid)) {
        setPropertyByJid(gjid, "phoneavatar", QString());

        QVariantMap query;
        query["jid"] = gjid;
        query["type"] = QueryType::ContactsUpdatePhoneavatar;
        query["phoneavatar"] = QString();
        query["uuid"] = uuid;
        dbExecutor->queueAction(query);
    }*/
}

void ContactsBaseModel::checkLastMessage(const QString &jid)
{
    /*if (_modelData.contains(jid)) {
        qDebug() << "checkLastMessage" << jid << _modelData[jid]["lastmessage"];
        qint32 timestamp = _modelData[jid]["lastmessage"].toLongLong();
        if (timestamp > 0) {
            QVariantMap query;
            query["type"] = QueryType::ConversationGetMessage;
            query["uuid"] = uuid;
            query["jid"] = jid;
            query["timestamp"] = _modelData[jid]["lastmessage"];
            dbExecutor->queueAction(query);
        }
        else {
            setPropertyByJid(jid, "lastmodel", QVariantMap());
        }
    }*/
}

void ContactsBaseModel::setLastMessageStatus(const QString &jid, const QString &msgId, qint64 status)
{
    qDebug() << "setLastMessageStatus" << jid << QString::number(status);
    if (_modelData.contains(jid)) {
        QVariantMap lastmodel = _modelData[jid]["lastmodel"].toMap();
        if (lastmodel["msgId"].toString() == msgId) {
            lastmodel["status"] = status;
            setPropertyByJid(jid, "lastmodel", lastmodel);
        }
    }
}

void ContactsBaseModel::setContactAvatar(const QString &jid, const QString &avatar, const QByteArray &data)
{
    QStringList avatars = _modelData[jid]["avatar"].toStringList();
    if (avatars.size() > 0 && avatars.first() == avatar)
        return;
    qDebug() << "contact picture for" << jid << avatar << "data:" << data.size();
    QFile file(avatar);
    if (avatar.startsWith(QDir::homePath()) && !file.exists() && data.isEmpty()) {
        connection->getContactPicture(jid);
    }
    else {
        if (!data.isEmpty() && !file.exists() && file.open(QFile::WriteOnly)) {
            qDebug() << "saving avatar data to file:" << avatar;
            file.write(data);
            file.close();
        }
        else if (file.exists()) {
            qDebug() << "avatar exists:" << avatar;
        }
        if (avatars.contains(avatar)) {
            avatars.removeAll(avatar);
        }
        avatars.insert(0, avatar);
        qDebug() << jid << "avatars" << avatars;

        setPropertyByJid(jid, "avatar", avatars);

        QVariantMap query;
        query["type"] = QueryType::ContactAvatars;
        query["jid"] = jid;
        query["avatar"] = avatars.join(";");
        dbExecutor->queueAction(query);
    }
}

QString ContactsBaseModel::getPictureForId(const QString &id)
{
    return QString("%1/avatars/%2").arg(QStandardPaths::writableLocation(QStandardPaths::DataLocation))
                                   .arg(id);
}

void ContactsBaseModel::setUnread(const QString &jid, int count)
{
    qDebug() << jid << count;

    setPropertyByJid(jid, "unread", count);

    checkTotalUnread();

    QVariantMap query;
    query["type"] = QueryType::ContactSetUnread;
    query["jid"] = jid;
    query["unread"] = count;
    dbExecutor->queueAction(query);
}

void ContactsBaseModel::pushnameUpdated(const QString &jid, const QString &pushName)
{
    if (_modelData.contains(jid) && (pushName != jid.split("@").first())) {
        if (_modelData[jid]["pushname"].toString() == pushName)
            return;
        setPropertyByJid(jid, "pushname", pushName);

        QString nickname = _modelData[jid]["nickname"].toString();
        QString pushname = _modelData[jid]["pushname"].toString();
        QString message = _modelData[jid]["message"].toString();
        QString name = _modelData[jid]["name"].toString();

        nickname = ContactsBaseModel::getNicknameBy(jid, message, name, pushname);

        _modelData[jid]["nickname"] = nickname;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "nickname", nickname);
        Q_EMIT nicknameChanged(jid, nickname);
    }
}

void ContactsBaseModel::presenceAvailable(const QString &jid)
{
    if (!_availableContacts.contains(jid))
        _availableContacts.append(jid);
    setPropertyByJid(jid, "available", true);
}

void ContactsBaseModel::presenceUnavailable(const QString &jid)
{
    if (_availableContacts.contains(jid))
        _availableContacts.removeAll(jid);
    setPropertyByJid(jid, "available", false);
}

void ContactsBaseModel::presenceLastSeen(const QString jid, int timestamp)
{
    setPropertyByJid(jid, "timestamp", timestamp);
    setPropertyByJid(jid, "available", timestamp == 0);
}

void ContactsBaseModel::messageReceived(const QVariantMap &data)
{
    QString jid = data["jid"].toString();
    contactPaused(jid);
    int lastmessage = data["timestamp"].toInt();
    if (_modelData.contains(jid)) {
        _modelData[jid]["lastmessage"] = lastmessage;
        _modelData[jid]["typing"] = false;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "lastmessage", lastmessage);
    }
    else {
        contactsChanged();
    }
}

void ContactsBaseModel::contactsBlocked(const QStringList &jids)
{
    _blockedContacts = jids;
    foreach (const QString &jid, _modelData.keys()) {
        if (_modelData[jid]["blocked"].toBool() == jids.contains(jid))
            continue;
        if (!jid.contains("-")) {
            if (jids.contains(jid))
                _modelData[jid]["blocked"] = true;
            else
                _modelData[jid]["blocked"] = false;
        }
        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "blocked", _modelData[jid]["blocked"]);
    }
}

void ContactsBaseModel::contactsAvailable(const QStringList &jids)
{
    _availableContacts = jids;
    foreach (const QString &jid, _modelData.keys()) {
        if (_modelData[jid]["available"].toBool() == jids.contains(jid))
            continue;
        if (jids.contains(jid))
            _modelData[jid]["available"] = true;
        else
            _modelData[jid]["available"] = false;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "available", _modelData[jid]["available"]);
    }
}

void ContactsBaseModel::contactTyping(const QString &jid)
{
    if (_modelData.contains(jid)) {
        _modelData[jid]["typing"] = true;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "typing", true);
    }
}

void ContactsBaseModel::contactPaused(const QString &jid)
{
    if (_modelData.contains(jid)) {
        _modelData[jid]["typing"] = false;

        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "typing", false);
    }
}

void ContactsBaseModel::contactRemoved(const QString &jid)
{
    int row = _modelData.keys().indexOf(jid);
    beginRemoveRows(QModelIndex(), row, row);
    _modelData.remove(jid);
    endRemoveRows();
}

void ContactsBaseModel::dbResults(const QVariant &result)
{
    /*QVariantMap reply = result.toMap();
    int vtype = reply["type"].toInt();

    //if (vtype == QueryType::ContactsSaveModel) {
    //    contactChanged(reply);
    //}

    if (reply["uuid"].toString() != uuid)
        return;
    switch (vtype) {
    case QueryType::ContactsReloadContact: {
        QVariantMap contact = reply["contact"].toMap();
        contactChanged(contact);
        break;
    }
    case QueryType::ContactsGetAll: {
        QVariantList records = reply["contacts"].toList();
        qDebug() << "Received QueryGetContacts reply. Size:" << QString::number(records.size());
        if (records.size() > 0) {
            beginResetModel();
            _modelData.clear();
            foreach (const QVariant &c, records) {
                QVariantMap data = c.toMap();
                QString jid = data["jid"].toString();
                QString pushname = data["pushname"].toString();
                QString name = data["name"].toString();
                QString message = data["message"].toString();
                //qDebug() << "jid:" << jid << pushname << name << message;
                bool blocked = false;
                if (!jid.contains("-"))
                    blocked = client->getBlocked(jid);
                data["blocked"] = blocked;
                bool available = getAvailable(jid);
                data["available"] = available;
                QString nickname = ContactsBaseModel::getNicknameBy(jid, message, name, pushname);
                data["nickname"] = nickname;
                data["typing"] = false;
                _modelData[jid] = data;
                if (!_colors.keys().contains(jid))
                    _colors[jid] = generateColor();
                if (data["avatar"].toString().isEmpty())
                    requestAvatar(jid);
                checkLastMessage(jid);
            }
            endResetModel();
        }
        checkTotalUnread();
        break;
    }
    case QueryType::ContactsClearConversation: {
        reloadContact(reply["jid"].toString());
        Q_EMIT conversationClean(reply["jid"].toString());
        break;
    }
    case QueryType::ContactsCreateBroadcast: {
        Q_EMIT broadcastCreated(reply["jid"].toString(), reply["jids"].toStringList());
        //reloadContact(reply["jid"].toString());
        break;
    }
    case QueryType::ConversationGetMessage: {
        QVariantMap messageModel = reply["message"].toMap();
        setPropertyByJid(reply["jid"].toString(), "lastmodel", messageModel);
        break;
    }
    }*/
}

int ContactsBaseModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _modelData.size();
}

QVariant ContactsBaseModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _modelData.count())
        return QVariantMap();
    return _modelData[_modelData.keys()[row]][_roles[role]];
}

void ContactsBaseModel::importConversations(const QString &type, const QString &login)
{
    qDebug() << type;
    if (type == "android") {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "android_conv_import");
        db.setDatabaseName(QString("%1/.local/share/mitakuuluu3/mitakuuluu3/msgstore.db").arg(QDir::homePath()));
        if (db.open()) {
            QSqlQuery msg(db);
            msg.prepare("SELECT * FROM messages;");
            msg.exec();

            QVariantMap conversations;
            while (msg.next()) {
                QVariantMap message;
                for (int i = 0; i < msg.record().count(); i ++) {
                    message[msg.record().fieldName(i)] = msg.value(i);
                }
                if (message["media_wa_type"].toInt() == 0) {
                    QString jid = message["key_remote_jid"].toString();
                    QVariantList messages = conversations[jid].toList();
                    messages.append(message);
                    conversations[jid] = messages;
                }
            }
            db.close();
            QFile(db.databaseName()).remove();

            QVariantMap query;
            query["type"] = QueryType::ConversationsImport;
            query["conversations"] = conversations;
            query["source"] = type;
            query["login"] = login;
            dbExecutor->queueAction(query);
        }
        else {
            qDebug() << "can't open android database:" << db.lastError().text();
        }
    }
    else if (type == "whatsup") {
        QString dbPath = QString("%1/.whatsup/%2.db").arg(QDir::homePath()).arg(login);
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "whatsup_conv_import");
        db.setDatabaseName(dbPath);
        if (db.open()) {
            QSqlQuery msg(db);
            msg.prepare("SELECT * FROM messages;");
            msg.exec();

            QVariantMap conversations;
            while (msg.next()) {
                QVariantMap message;
                for (int i = 0; i < msg.record().count(); i ++) {
                    message[msg.record().fieldName(i)] = msg.value(i);
                }
                if (message["type"].toInt() < 2 && message["media"].toString().isEmpty()) {
                    QString jid = message["conversation_jid"].toString();
                    QVariantList messages = conversations[jid].toList();
                    messages.append(message);
                    conversations[jid] = messages;
                }
            }
            db.close();

            QVariantMap query;
            query["type"] = QueryType::ConversationsImport;
            query["conversations"] = conversations;
            query["source"] = type;
            query["login"] = login;
            dbExecutor->queueAction(query);
        }
        else {
            qDebug() << "can't open whatsup database:" << db.lastError().text();
        }
    }
    else if (type == "mitakuuluu") {
        QString dbPath = QString("%1/.local/share/harbour-mitakuuluu2/harbour-mitakuuluu2/database.db").arg(QDir::homePath());
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "mitakuuluu_conv_import");
        db.setDatabaseName(dbPath);
        if (db.open()) {
            QVariantMap conversations;
            foreach (const QString &table, db.tables()) {
                if (table.startsWith("u")) {
                    QSqlQuery msg(db);
                    msg.prepare(QString("SELECT * FROM %1;").arg(table));
                    msg.exec();

                    QVariantList messages;
                    QString jid;
                    while (msg.next()) {
                        QVariantMap message;
                        for (int i = 0; i < msg.record().count(); i ++) {
                            message[msg.record().fieldName(i)] = msg.value(i);
                        }
                        if (message["watype"].toInt() == 0) {
                            messages.append(message);
                        }
                        jid = message["jid"].toString();
                    }
                    conversations[jid] = messages;
                }
            }
            db.close();

            QVariantMap query;
            query["type"] = QueryType::ConversationsImport;
            query["conversations"] = conversations;
            query["source"] = type;
            query["login"] = login;
            dbExecutor->queueAction(query);
        }
        else {
            qDebug() << "can't open whatsup database:" << db.lastError().text();
        }
    }
}

int ContactsBaseModel::count()
{
    return _modelData.count();
}


void ContactsBaseModel::renameContact(const QString &jid, const QString &name)
{
    if (_modelData.contains(jid)) {
        if (_modelData[jid]["name"].toString() == name)
            return;

        _modelData[jid]["name"] = name;
        QString pushname = _modelData[jid]["pushname"].toString();

        QString nickname;
        if (name == jid.split("@").first()) {
            if (!pushname.isEmpty())
                nickname = pushname;
            else
                nickname = name;
        }
        else {
            nickname = name;
        }
        _modelData[jid]["nickname"] = nickname;
        int row = _modelData.keys().indexOf(jid);
        Q_EMIT dataChanged(index(row), index(row));
        Q_EMIT propertyChanged(jid, "nickname", nickname);

        /*QVariantMap query = _modelData[jid];
        query["type"] = QueryType::ContactsSaveModel;
        dbExecutor->queueAction(query);*/
    }
}

void ContactsBaseModel::requestAvatar(const QString &jid)
{
    //client->getPicture(jid);
}

void ContactsBaseModel::clearChat(const QString &jid)
{
    /*setPropertyByJid(jid, "lastmessage", 0);

    QVariantMap query;
    query["type"] = QueryType::ContactsClearConversation;
    query["jid"] = jid;
    query["uuid"] = uuid;
    dbExecutor->queueAction(query);*/
}

QString ContactsBaseModel::createBroadcast(const QStringList &jids, const QString &name)
{
    /*qint64 timestamp = QDateTime::currentDateTime().toTime_t();
    QString jid = QString("%1@broadcast").arg(timestamp);

    QVariantMap broadcast;
    broadcast["jid"] = jid;
    broadcast["pushname"] = QString();
    broadcast["name"] = name;
    broadcast["owner"] = client->myJid;
    broadcast["nickname"] = name;
    broadcast["message"] = QString();
    broadcast["subowner"] = QString();
    broadcast["subtimestamp"] = 0;
    broadcast["timestamp"] = timestamp;
    broadcast["contacttype"] = 1;
    broadcast["avatar"] = QString();
    broadcast["phoneavatar"] = QString();
    broadcast["available"] = false;
    broadcast["unread"] = 0;
    broadcast["lastmessage"] = 0;
    broadcast["muting"] = false;
    broadcast["blocked"] = false;
    broadcast["hidden"] = false;
    broadcast["typing"] = false;
    broadcast["participants"] = jids.join(";");
    broadcast["lastmodel"] = QVariantMap();
    beginResetModel();
    _modelData[jid] = broadcast;
    endResetModel();

    QVariantMap query;
    query["type"] = QueryType::ContactsCreateBroadcast;
    query["uuid"] = uuid;
    query["jids"] = jids;
    query["name"] = name;
    query["jid"] = jid;
    dbExecutor->queueAction(query);

    checkLastMessage(jid);

    return jid;*/
}

void ContactsBaseModel::renameBroadcast(const QString &jid, const QString &name)
{
    /*if (_modelData.contains(jid)) {
        setPropertyByJid(jid, "name", name);
        setPropertyByJid(jid, "nickname", name);

        QVariantMap query;
        query["type"] = QueryType::ContactsCreateBroadcast;
        query["uuid"] = uuid;
        query["name"] = name;
        query["jid"] = jid;
        dbExecutor->queueAction(query);
    }*/
}

bool ContactsBaseModel::havePhone(const QString &phone)
{
    foreach (const QString &jid, _modelData.keys()) {
        QString id = jid.split("@").first();
        if (id.contains(phone) || phone.contains(id)) {
            return true;
        }
    }
    return false;
}

bool ContactsBaseModel::haveContact(const QString &jid)
{
    return _modelData.contains(jid);
}

QString ContactsBaseModel::getNicknameByJid(const QString &jid)
{
    if (_modelData.contains(jid)) {
        return _modelData[jid]["nickname"].toString();
    }
    else {
        if (connection->myJid() == jid) {
            return "You";
        }
        else {
            qWarning() << jid << "unknown";
            return "Unknown";
        }
    }
}

QString ContactsBaseModel::getAvatarByJid(const QString &jid)
{
    if (_modelData.contains(jid)) {
        QString avatar = _modelData[jid]["phoneavatar"].toString();
        if (avatar.isEmpty()) {
            avatar = _modelData[jid]["avatar"].toStringList().first();
        }
        return avatar;
    }
    else {
        return QString();
    }
}

QStringList ContactsBaseModel::getJids()
{
    return _modelData.keys();
}

ContactsBaseModel *ContactsBaseModel::GetInstance(QObject *parent)
{
    static ContactsBaseModel* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new ContactsBaseModel(parent);
    }
    return lsSingleton;
}
