#include "messagesbasemodel.h"
#include "src/constants.h"
#include "src/models/conversationbasemodel.h"

//! A proxy for accessing the notification manager
Q_GLOBAL_STATIC_WITH_ARGS(NotificationManagerProxy, notificationManagerProxyInstance, ("org.freedesktop.Notifications", "/org/freedesktop/Notifications", QDBusConnection::sessionBus()))

NotificationManagerProxy *notificationManager()
{
    if (!notificationManagerProxyInstance.exists()) {
        qDBusRegisterMetaType<Notification>();
        qDBusRegisterMetaType<QList<Notification> >();
    }

    return notificationManagerProxyInstance();
}

MessagesBaseModel::MessagesBaseModel(QObject *parent) :
    QAbstractListModel(parent)
{
    _keys << "jid"; // Qt::UserRole + 1
    _keys << "nickname";
    _keys << "avatar";
    _keys << "messages";
    int role = Qt::UserRole + 1;
    foreach (const QString &rolename, _keys) {
        _roles[role++] = rolename.toLatin1();
    }

    dbExecutor = QueryExecutor::GetInstance();

    connection = MainConnection::GetInstance();

    contactsBaseModel = ContactsBaseModel::GetInstance();

    qDebug() << Notification::NotificationManagerInstance();

    connect(Notification::NotificationManagerInstance(), SIGNAL(ActionInvoked(uint,QString)), this, SLOT(checkActionInvoked(uint,QString)));
    connect(Notification::NotificationManagerInstance(), SIGNAL(NotificationClosed(uint,uint)), this, SLOT(checkNotificationClosed(uint,uint)));
}

MessagesBaseModel::~MessagesBaseModel()
{
    qDebug() << "destruct";
    foreach (uint notificationId, _notificationsData.keys()) {
        Notification::closeNotification(notificationId);
    }
}

MessagesBaseModel *MessagesBaseModel::GetInstance(QObject *parent)
{
    static MessagesBaseModel* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new MessagesBaseModel(parent);
    }
    return lsSingleton;
}

int MessagesBaseModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return _modelIndex.size();
}

QVariant MessagesBaseModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _modelData.size()) {
        qDebug() << "invalid row:" << row;
        return QVariant();
    }
    else {
        return _modelData[row][_roles[role]];
    }
}

void MessagesBaseModel::notify(const QVariantMap &message)
{
    QString jid = message["jid"].toString();
    QString msgid = message["msgid"].toString();
    QVariantMap model = ContactsBaseModel::GetInstance()->getModel(jid);
    if (model.isEmpty()) {
        return;
    }
    QString avatar = model["phoneavatar"].toString();
    if (avatar.isEmpty()) {
        avatar = model["avatar"].toStringList().first();
    }
    avatar = avatar.replace("file://", "");
    qDebug() << "avatar:" << avatar;
    QString nickname = model["nickname"].toString();

    uint replacesId = 0;
    int count = 0;
    int i = -1;
    if (_modelIndex.contains(jid)) {
        i = _modelIndex.indexOf(jid);
        QHashIterator<uint, QString> notifIter(_notificationsData);
        while (notifIter.findNext(jid)) {
            replacesId = notifIter.key();
        }
        _notificationsData.remove(replacesId);
        count = _modelData[i]["messages"].toList().size();
    }

    Notification notification;
    notification.setCategory("org.mitakuuluu3.private");
    notification.setIcon(avatar);
    if (count > 0) {
        notification.setSummary(tr("%n new messages", "", count + 1));
        notification.setPreviewSummary(tr("%n new messages", "", count + 1));
    }
    else {
        QString data = message["data"].toString();
        switch (message["msgtype"].toInt()) {
            case ConversationBaseModel::ImageMessage:
                data = tr("Image message");
                break;
            case ConversationBaseModel::AudioMessage:
                data = tr("Audio message");
                break;
            case ConversationBaseModel::VideoMessage:
                data = tr("Video message");
                break;
            case ConversationBaseModel::LocationMessage:
                data = tr("Location message");
                break;
            case ConversationBaseModel::VCardMessage:
                data = tr("Contact message");
            default:
                data = message["data"].toString();
                break;
        }
        notification.setSummary(data);
        notification.setPreviewSummary(data);
    }
    notification.setBody(nickname);
    notification.setPreviewBody(nickname);
    //notification.setItemCount(count + 1);
    notification.setRemoteDBusCallServiceName(DBUS_SERVICE);
    notification.setRemoteDBusCallObjectPath(DBUS_PATH);
    notification.setRemoteDBusCallInterface(DBUS_INTERFACE);
    notification.setRemoteDBusCallMethodName("notificationCallback");
    notification.setRemoteDBusCallArguments(QVariantList() << jid << msgid);
    if (replacesId != 0) {
        notification.setReplacesId(replacesId);
    }
    notification.publish();
    uint notificationId = notification.replacesId();

    if (count == 0) {
        beginInsertRows(QModelIndex(), _modelIndex.size(), _modelIndex.size());
    }

    _notificationsData[notificationId] = jid;
    if (count == 0) {
        QVariantMap model;
        model["jid"] = jid;
        model["nickname"] = nickname;
        model["avatar"] = avatar;
        QVariantList data;
        data << message;
        model["messages"] = data;
        _modelData.append(model);
        _modelIndex.append(jid);
    }
    else {
        QVariantList data = _modelData[i]["messages"].toList();
        data.append(message);
        _modelData[i]["messages"] = data;
    }

    if (count == 0) {
        endInsertRows();
    }
    else {
        dataChanged(index(i), index(i), QVector<int>(1, Qt::UserRole + 4));
    }
}

void MessagesBaseModel::clearMessagesForJid(const QString &jid)
{
    if (_modelIndex.contains(jid)) {
        int i = _modelIndex.indexOf(jid);

        beginResetModel();

        _modelData.removeAt(i);
        _modelIndex.removeAt(i);

        QMutableHashIterator<uint, QString> notifIter(_notificationsData);
        while (notifIter.findNext(jid)) {
            Notification::closeNotification(notifIter.key());
            notifIter.remove();
        }

        endResetModel();

        if (_modelIndex.size() == 0) {
            Q_EMIT lastMessageRemoved();
        }
    }
}

void MessagesBaseModel::clear()
{
    beginResetModel();

    clearNotifications();

    _modelIndex.clear();
    _modelData.clear();

    endResetModel();
}

void MessagesBaseModel::clearNotifications()
{
    if (_notificationsData.isEmpty()) {
        return;
    }

    QMutableHashIterator<uint, QString> notifIter(_notificationsData);
    while (notifIter.hasNext()) {
        notifIter.next();
        Notification::closeNotification(notifIter.key());
        notifIter.remove();
    }
}

void MessagesBaseModel::checkActionInvoked(uint id, const QString &actionKey)
{
    qDebug() << id << actionKey;

    if (_notificationsData.contains(id)) {
        if (!_pendingInvoked.contains(id)) {
            _pendingInvoked.append(id);
        }
    }
}

void MessagesBaseModel::checkNotificationClosed(uint id, uint reason)
{
    qDebug() << id << reason;

    if (_pendingInvoked.contains(id)) {
        _pendingInvoked.removeAll(id);
        return;
    }

    if (_notificationsData.contains(id)) {
        QString jid = _notificationsData[id];
        if (_modelIndex.contains(jid)) {
            int i = _modelIndex.indexOf(jid);
            beginRemoveRows(QModelIndex(), i, i);
            _modelData.removeAt(i);
            _modelIndex.removeAt(i);
            endRemoveRows();
        }
        _notificationsData.remove(id);
    }
}
