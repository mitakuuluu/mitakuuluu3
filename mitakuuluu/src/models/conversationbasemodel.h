#ifndef CONVERSATIONBASEMODEL_H
#define CONVERSATIONBASEMODEL_H

#include <QAbstractListModel>
#include <QQmlParserStatus>
#include <QDebug>

#include "src/threadworker/queryexecutor.h"
#include "src/waclasses/mainconnection.h"
#include "src/models/contactsbasemodel.h"

class ConversationBaseModel : public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(MessageType)
    Q_ENUMS(MessageStatus)
public:
    enum MessageType {
        TextMessage = 0,
        ImageMessage,
        VideoMessage,
        AudioMessage,
        LocationMessage,
        VCardMessage,
        InformationMessage
    };

    enum MessageStatus {
        MessageSending = 0,
        MessageSent
    };

    enum ModelRole {
        MsgIdRole = Qt::UserRole + 1,
        JidRole,
        AuthorRole,
        TimestampRole,
        DataRole,
        StatusRole,
        MsgTypeRole,
        ParamsRole,
        DeliveryReceiptsRole,
        ReadReceiptsRole,
        RecepientsRole,
        ProgressRole
    };

    explicit ConversationBaseModel(QObject *parent = 0);

    Q_PROPERTY (QString jid READ jid WRITE setJid NOTIFY jidChanged)
    QString jid() const;
    void setJid(const QString &newJid);

    Q_PROPERTY (int limit READ limit WRITE setLimit NOTIFY limitChanged)
    int limit();
    void setLimit(int newLimit);

    Q_PROPERTY (int messagesCount READ messagesCount NOTIFY messagesCountChanged)
    int messagesCount();

    void classBegin();
    void componentComplete();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const { return _roles; }

public slots:
    void loadNext();

private:
    void init();
    void getMessagesCount();
    void loadConversation();
    void setPropertyById(const QString &msgId, const QString &property, const QVariant &value);

    bool _complete;

    QStringList _indexId;
    QHash<QString, QVariantMap> _modelData;
    QStringList _keys;
    QHash<int, QByteArray> _roles;

    QueryExecutor *dbExecutor;
    MainConnection *connection;

    QString _jid;
    int _limit;

    int _messagesCount;

private slots:
    void textMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const QString &data);
    void mediaMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const AttributeList &attrs, const QByteArray &data);
    void textMessageSent(const QString &jid, const QString &id, const QString &timestamp, const QString &data);
    void messageSent(const QString &jid, const QString &msgId, const QString &timestamp);
    void messageReceipt(const QString &jid, const QString &msgId, const QString &participant, const QString &timestamp, const QString &type);

    void groupSubjectChanged(const QString &jid, const QString &subject, const QString &s_o, const QString &s_t);
    void groupParticipantAdded(const QString &gjid, const QString &jid);
    void groupParticipantRemoved(const QString &gjid, const QString &jid);
    void groupParticipantPromoted(const QString &gjid, const QString &jid);
    void groupParticipantDemoted(const QString &gjid, const QString &jid);

    void onConversationLoad(const QVariantMap &reply);
    void onConversationLoadNext(const QVariantMap &reply);
    void onConversationCount(const QVariantMap &reply);

    void onDownloadCompleted(const QString &jid, const QString &dest, const QString &msgid);
    void onDownloadFailed(const QString &jid, const QString &msgid);
    void onDownloadProgress(const QString &jid, float progress, const QString &msgid);

signals:
    void jidChanged();
    void limitChanged();
    void messagesCountChanged();
    void loadingCompleted();
};

#endif // CONVERSATIONBASEMODEL_H
