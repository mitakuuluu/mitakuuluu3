#ifndef MESSAGESBASEMODEL_H
#define MESSAGESBASEMODEL_H

#include <QAbstractListModel>
#include <QPair>
#include <QMutableHashIterator>

#include "src/threadworker/queryexecutor.h"
#include "src/waclasses/mainconnection.h"
#include "src/models/contactsbasemodel.h"
#include "src/platform/notification.h"
#include "src/platform/notificationmanagerproxy.h"

typedef QPair<QString, QString> JidMessagePair;

class MessagesBaseModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MessagesBaseModel(QObject *parent = 0);
    virtual ~MessagesBaseModel();
    static MessagesBaseModel *GetInstance(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const { return _roles; }

    void notify(const QVariantMap &message);

public slots:
    void clearMessagesForJid(const QString &jid);
    void clear();
    void clearNotifications();

private:
    QStringList _keys;
    QHash<int, QByteArray> _roles;

    QList<QString> _modelIndex;
    QList<QVariantMap> _modelData;
    QHash<uint, QString> _notificationsData;

    QList<uint> _pendingInvoked;

    QueryExecutor *dbExecutor;
    MainConnection *connection;
    ContactsBaseModel *contactsBaseModel;

private slots:
    void checkActionInvoked(uint id, const QString &actionKey);
    void checkNotificationClosed(uint id, uint reason);

signals:
    void lastMessageRemoved();

};

#endif // MESSAGESBASEMODEL_H
