#include "conversationbasemodel.h"

ConversationBaseModel::ConversationBaseModel(QObject *parent) :
    QAbstractListModel(parent),
    _complete(false)
{
    _keys << "msgid"; // Qt::UserRole + 1
    _keys << "jid";
    _keys << "author";
    _keys << "timestamp";
    _keys << "data";
    _keys << "status";
    _keys << "msgtype";
    _keys << "params";
    _keys << "deliveryreceipts";
    _keys << "readreceipts";
    _keys << "recepients";
    _keys << "progress";
    _keys << "section";
    int role = Qt::UserRole + 1;
    foreach (const QString &rolename, _keys) {
        _roles[role++] = rolename.toLatin1();
    }

    dbExecutor = QueryExecutor::GetInstance();

    connection = MainConnection::GetInstance();
    QObject::connect(connection, SIGNAL(textMessageReceived(QString,QString,QString,QString,bool,QString)), this, SLOT(textMessageReceived(QString,QString,QString,QString,bool,QString)));
    QObject::connect(connection, SIGNAL(mediaMessageReceived(QString,QString,QString,QString,bool,AttributeList,QByteArray)), this, SLOT(mediaMessageReceived(QString,QString,QString,QString,bool,AttributeList,QByteArray)));
    QObject::connect(connection, SIGNAL(textMessageSent(QString,QString,QString,QString)), this, SLOT(textMessageSent(QString,QString,QString,QString)));
    QObject::connect(connection, SIGNAL(messageSent(QString,QString,QString)), this, SLOT(messageSent(QString,QString,QString)));
    QObject::connect(connection, SIGNAL(messageReceipt(QString,QString,QString,QString,QString)), this, SLOT(messageReceipt(QString,QString,QString,QString,QString)));

    QObject::connect(connection, SIGNAL(downloadCompleted(QString,QString,QString)), this, SLOT(onDownloadCompleted(QString,QString,QString)));
    QObject::connect(connection, SIGNAL(downloadFailed(QString,QString)), this, SLOT(onDownloadFailed(QString,QString)));
    QObject::connect(connection, SIGNAL(downloadProgress(QString,float,QString)), this, SLOT(onDownloadProgress(QString,float,QString)));

    QObject::connect(connection, SIGNAL(groupSubjectChanged(QString,QString,QString,QString)), this, SLOT(groupSubjectChanged(QString,QString,QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantAdded(QString,QString)), this, SLOT(groupParticipantAdded(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantRemoved(QString,QString)), this, SLOT(groupParticipantRemoved(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantPromoted(QString,QString)), this, SLOT(groupParticipantPromoted(QString,QString)));
    QObject::connect(connection, SIGNAL(groupParticipantDemoted(QString,QString)), this, SLOT(groupParticipantDemoted(QString,QString)));
}

QString ConversationBaseModel::jid() const
{
    return _jid;
}

void ConversationBaseModel::setJid(const QString &newJid)
{
    if (_jid.isEmpty()) {
        _jid = newJid;

        if (_complete) {
            init();
        }

        Q_EMIT jidChanged();
    }
}

int ConversationBaseModel::limit()
{
    return _limit;
}

void ConversationBaseModel::setLimit(int newLimit)
{
    _limit = newLimit;
    Q_EMIT limitChanged();
}

int ConversationBaseModel::messagesCount()
{
    return _messagesCount;
}

void ConversationBaseModel::classBegin()
{

}

void ConversationBaseModel::componentComplete()
{
    if (!_jid.isEmpty()) {
        init();
    }

    _complete = true;
}

void ConversationBaseModel::loadNext()
{
    QVariantMap query;
    query["type"] = QueryType::ConversationLoad;
    query["jid"] = _jid;
    query["limit"] = _limit;
    query["timestamp"] = _modelData[_indexId.last()]["timestamp"].toUInt();
    query["object"] = qVariantFromValue(this);
    query["method"] = QUERYREPLY(onConversationLoadNext);
    dbExecutor->queueAction(query);

    qDebug() << _modelData[_indexId.last()]["timestamp"].toUInt();
}

void ConversationBaseModel::init()
{
    getMessagesCount();
    loadConversation();
}

void ConversationBaseModel::getMessagesCount()
{
    QVariantMap query;
    query["type"] = QueryType::ConversationCount;
    query["jid"] = _jid;
    query["object"] = qVariantFromValue(this);
    query["method"] = QUERYREPLY(onConversationCount);
    dbExecutor->queueAction(query);
}

void ConversationBaseModel::loadConversation()
{
    QVariantMap query;
    query["type"] = QueryType::ConversationLoad;
    query["jid"] = _jid;
    query["limit"] = _limit;
    query["object"] = qVariantFromValue(this);
    query["method"] = QUERYREPLY(onConversationLoad);
    dbExecutor->queueAction(query);
}

void ConversationBaseModel::setPropertyById(const QString &msgId, const QString &property, const QVariant &value)
{
    if (_modelData.contains(msgId) && _modelData[msgId][property] != value) {
        //qDebug() << msgId << property << value;
        _modelData[msgId][property] = value;
        int row = _indexId.indexOf(msgId);
        Q_EMIT dataChanged(index(row), index(row), QVector<int>(1, Qt::UserRole + 1 + _keys.indexOf(property)));
    }
}

void ConversationBaseModel::textMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const QString &data)
{
    if (_jid == jid) {
        if (_modelData.contains(id)) {

        }
        else {
            beginInsertRows(QModelIndex(), 0, 0);

            QVariantMap message;
            message["msgid"] = id;
            message["jid"] = jid;
            message["author"] = author.isEmpty() ? jid : author;
            message["timestamp"] = timestamp.toUInt();
            message["data"] = data;
            message["status"] = MessageSent;
            message["msgtype"] = TextMessage;
            message["params"] = QVariantMap();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["progress"] = (float)0;
            message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp.toUInt()));
            _modelData[id] = message;
            _indexId.prepend(id);

            endInsertRows();

            _messagesCount += 1;
            Q_EMIT messagesCountChanged();
        }
    }
}

void ConversationBaseModel::mediaMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const AttributeList &attrs, const QByteArray &data)
{
    qDebug() << _jid << jid << id << _modelData.contains(id);
    if (_jid == jid) {
        if (_modelData.contains(id)) {

        }
        else {
            beginInsertRows(QModelIndex(), 0, 0);

            QVariantMap message;
            message["msgid"] = id;
            message["jid"] = jid;
            message["author"] = author.isEmpty() ? jid : author;
            message["timestamp"] = timestamp.toUInt();
            message["data"] = data.toBase64();
            message["status"] = MessageSent;
            QString mediaType = attrs["type"].toString();
            if (mediaType == "image") {
                message["msgtype"] = ImageMessage;
            }
            else if (mediaType == "video") {
                message["msgtype"] = VideoMessage;
            }
            else if (mediaType == "audio") {
                message["msgtype"] = AudioMessage;
            }
            else if (mediaType == "location") {
                message["msgtype"] = LocationMessage;
            }
            else if (mediaType == "vcard") {
                message["msgtype"] = VCardMessage;
            }
            message["params"] = attrs;
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["progress"] = (float)0;
            message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp.toUInt()));
            _modelData[id] = message;
            _indexId.prepend(id);

            endInsertRows();

            _messagesCount += 1;
            Q_EMIT messagesCountChanged();
        }
    }
}

void ConversationBaseModel::textMessageSent(const QString &jid, const QString &id, const QString &timestamp, const QString &data)
{
    if (_jid == jid) {
        if (_modelData.contains(id)) {

        }
        else {
            beginInsertRows(QModelIndex(), 0, 0);

            QVariantMap message;
            message["msgid"] = id;
            message["jid"] = jid;
            message["author"] = connection->myJid();
            message["timestamp"] = timestamp.toUInt();
            message["data"] = data;
            message["status"] = MessageSending;
            message["msgtype"] = TextMessage;
            message["params"] = QVariantMap();
            message["deliveryreceipts"] = QVariantMap();
            message["readreceipts"] = QVariantMap();
            message["progress"] = (float)0;
            message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp.toUInt()));
            _modelData[id] = message;
            _indexId.prepend(id);

            endInsertRows();

            _messagesCount += 1;
            Q_EMIT messagesCountChanged();
        }
    }
}

void ConversationBaseModel::messageSent(const QString &jid, const QString &msgId, const QString &timestamp)
{
    if (jid == _jid) {
        setPropertyById(msgId, "status", MessageSent);
    }
}

void ConversationBaseModel::messageReceipt(const QString &jid, const QString &msgId, const QString &participant, const QString &timestamp, const QString &type)
{
    if (jid == _jid && _modelData.contains(msgId)) {
        qDebug() << jid << msgId << participant << type;
        QString field = type == "read" ? "readreceipts" : "deliveryreceipts";
        QVariantMap receipts = _modelData[msgId][field].toMap();
        receipts[participant.isEmpty() ? jid : participant] = timestamp.toUInt();
        setPropertyById(msgId, field, receipts);
    }
}

void ConversationBaseModel::groupSubjectChanged(const QString &jid, const QString &subject, const QString &s_o, const QString &s_t)
{
    if (_jid == jid) {
        beginInsertRows(QModelIndex(), 0, 0);

        QVariantMap message;
        uint timestamp = s_t.toUInt();
        QString id = QString("%1-2").arg(timestamp);
        message["msgid"] = id;
        message["jid"] = jid;
        message["author"] = jid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 changed group subject to %2").arg(ContactsBaseModel::GetInstance()->getNicknameByJid(s_o)).arg(subject);
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;
        message["progress"] = (float)0;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp));
        _modelData[id] = message;
        _indexId.prepend(id);

        endInsertRows();

        _messagesCount += 1;
        Q_EMIT messagesCountChanged();
    }
}

void ConversationBaseModel::groupParticipantAdded(const QString &gjid, const QString &jid)
{
    if (_jid == jid) {
        beginInsertRows(QModelIndex(), 0, 0);

        QVariantMap message;
        uint timestamp = QDateTime::currentDateTime().toTime_t();
        QString id = QString("%1-4").arg(timestamp);
        message["msgid"] = id;
        message["jid"] = gjid;
        message["author"] = gjid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 joined group").arg(ContactsBaseModel::GetInstance()->getNicknameByJid(jid));
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;
        message["progress"] = (float)0;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp));
        _modelData[id] = message;
        _indexId.prepend(id);

        endInsertRows();

        _messagesCount += 1;
        Q_EMIT messagesCountChanged();
    }
}

void ConversationBaseModel::groupParticipantRemoved(const QString &gjid, const QString &jid)
{
    if (_jid == jid) {
        beginInsertRows(QModelIndex(), 0, 0);

        QVariantMap message;
        uint timestamp = QDateTime::currentDateTime().toTime_t();
        QString id = QString("%1-5").arg(timestamp);
        message["msgid"] = id;
        message["jid"] = gjid;
        message["author"] = gjid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 left group").arg(ContactsBaseModel::GetInstance()->getNicknameByJid(jid));
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;
        message["progress"] = (float)0;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp));
        _modelData[id] = message;
        _indexId.prepend(id);

        endInsertRows();

        _messagesCount += 1;
        Q_EMIT messagesCountChanged();
    }
}

void ConversationBaseModel::groupParticipantPromoted(const QString &gjid, const QString &jid)
{
    if (_jid == jid) {
        beginInsertRows(QModelIndex(), 0, 0);

        QVariantMap message;
        uint timestamp = QDateTime::currentDateTime().toTime_t();
        QString id = QString("%1-6").arg(timestamp);
        message["msgid"] = id;
        message["jid"] = gjid;
        message["author"] = gjid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 is now group admin").arg(ContactsBaseModel::GetInstance()->getNicknameByJid(jid));
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;
        message["progress"] = (float)0;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp));
        _modelData[id] = message;
        _indexId.prepend(id);

        endInsertRows();

        _messagesCount += 1;
        Q_EMIT messagesCountChanged();
    }
}

void ConversationBaseModel::groupParticipantDemoted(const QString &gjid, const QString &jid)
{
    if (_jid == jid) {
        beginInsertRows(QModelIndex(), 0, 0);

        QVariantMap message;
        uint timestamp = QDateTime::currentDateTime().toTime_t();
        QString id = QString("%1-7").arg(timestamp);
        message["msgid"] = id;
        message["jid"] = gjid;
        message["author"] = gjid;
        message["timestamp"] = timestamp;
        message["data"] = tr("%1 is not group admin anymore").arg(ContactsBaseModel::GetInstance()->getNicknameByJid(jid));
        message["status"] = 1;
        message["msgtype"] = 6;
        message["params"] = QString();
        message["deliveryreceipts"] = QVariantMap();
        message["readreceipts"] = QVariantMap();
        message["recepients"] = 0;
        message["progress"] = (float)0;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(timestamp));
        _modelData[id] = message;
        _indexId.prepend(id);

        endInsertRows();

        _messagesCount += 1;
        Q_EMIT messagesCountChanged();
    }
}

void ConversationBaseModel::onConversationLoad(const QVariantMap &reply)
{
    QVariantList messages = reply["messages"].toList();
    if (messages.size() == 0) {
        return;
    }

    beginResetModel();

    _modelData.clear();
    _indexId.clear();

    QListIterator<QVariant> iter(messages);
    while (iter.hasNext()) {
        QVariantMap message = iter.next().toMap();
        QString msgId = message["msgid"].toString();
        message["progress"] = (float)0;

        _modelData[msgId] = message;
        _indexId.append(msgId);
    }

    endResetModel();
}

void ConversationBaseModel::onConversationLoadNext(const QVariantMap &reply)
{
    QVariantList messages = reply["messages"].toList();
    if (messages.size() == 0) {
        return;
    }

    beginInsertRows(QModelIndex(), _modelData.size(), _modelData.size() + messages.size() - 1);

    QListIterator<QVariant> iter(messages);
    while (iter.hasNext()) {
        QVariantMap message = iter.next().toMap();
        QString msgId = message["msgid"].toString();
        message["progress"] = (float)0;

        _modelData[msgId] = message;
        _indexId.append(msgId);
    }

    endInsertRows();

    Q_EMIT loadingCompleted();
}

void ConversationBaseModel::onConversationCount(const QVariantMap &reply)
{
    _messagesCount = reply["count"].toInt();
    Q_EMIT messagesCountChanged();
}

void ConversationBaseModel::onDownloadCompleted(const QString &jid, const QString &dest, const QString &msgid)
{
    if (_jid == jid) {
        QVariantMap params = _modelData[msgid]["params"].toMap();
        params["local"] = dest;
        setPropertyById(msgid, "params", params);
    }
}

void ConversationBaseModel::onDownloadFailed(const QString &jid, const QString &msgid)
{
    if (_jid == jid) {
        setPropertyById(msgid, "progress", (float)0);
    }
}

void ConversationBaseModel::onDownloadProgress(const QString &jid, float progress, const QString &msgid)
{
    if (_jid == jid) {
        setPropertyById(msgid, "progress", progress);
    }
}

int ConversationBaseModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return _modelData.size();
}

QVariant ConversationBaseModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _modelData.size())
        return QVariant();
    QString msgId = _indexId[row];
    QVariant value = _modelData[msgId][_roles[role]];
    return value;
}
