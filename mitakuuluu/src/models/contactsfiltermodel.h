#ifndef CONTACTSFILTERMODEL_H
#define CONTACTSFILTERMODEL_H

#include "contactsbasemodel.h"

#include <QSortFilterProxyModel>
#include <QQmlParserStatus>

class ContactsFilterModel : public QSortFilterProxyModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(bool showActive READ showActive WRITE setShowActive FINAL)
    Q_PROPERTY(bool showUnknown READ showUnknown WRITE setShowUnknown FINAL)
    Q_PROPERTY(bool hideGroups READ hideGroups WRITE setHideGroups FINAL)
    Q_PROPERTY(int count READ count NOTIFY countChanged FINAL)
    Q_PROPERTY(bool filterContacts READ filterContacts WRITE setFilterContacts FINAL)
public:
    explicit ContactsFilterModel(QObject *parent = 0);
    void classBegin();
    void componentComplete();

public slots:
    Q_INVOKABLE QVariantMap get(int itemIndex);
    Q_INVOKABLE void resort();

private slots:
    void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    bool _initComplete;

    QString filter();
    void setFilter(const QString &newFilter);

    bool _showActive;
    bool showActive();
    void setShowActive(bool value);

    bool _showUnknown;
    bool showUnknown();
    void setShowUnknown(bool value);

    bool _hideGroups;
    bool hideGroups();
    void setHideGroups(bool value);

    bool _filterContacts;
    bool filterContacts();
    void setFilterContacts(bool value);

    int count();

    void changeFilterRole();

    ContactsBaseModel *_contactsModel;

signals:
    void filterChanged();
    void contactsModelChanged();
    void countChanged();

};

#endif // CONTACTSFILTERMODEL_H
