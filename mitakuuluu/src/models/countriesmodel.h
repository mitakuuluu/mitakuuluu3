#ifndef COUNTRIESMODEL_H
#define COUNTRIESMODEL_H

#include <QAbstractListModel>
#include <QFile>
#include <QTextStream>

class CountriesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit CountriesModel(QObject *parent = 0);
    static CountriesModel *GetInstance(QObject *parent = 0);

    Q_PROPERTY(int count READ count NOTIFY countChanged)
    int count() const;

    Q_PROPERTY(int simIndex READ simIndex NOTIFY simIndexChanged)
    int simIndex() const;

    Q_PROPERTY(QString simMcc READ simMcc WRITE setSimMcc NOTIFY simMccChanged)
    QString simMcc() const;
    void setSimMcc(const QString &mcc);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const { return _roles; }

    Q_INVOKABLE QVariantMap get(int index);

private:
    QStringList _keys;
    QHash<int, QByteArray> _roles;
    QList<QStringList> _modelData;

    int _simIndex;
    QString _simMcc;

private slots:
    void loadCSV();

signals:
    void countChanged();
    void simIndexChanged();
    void simMccChanged();

};

#endif // COUNTRIESMODEL_H
