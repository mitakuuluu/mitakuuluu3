#ifndef MESSAGESFILTERMODEL_H
#define MESSAGESFILTERMODEL_H

#include <QSortFilterProxyModel>
#include <QQmlParserStatus>

#include "messagesbasemodel.h"

class MessagesFilterModel : public QSortFilterProxyModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
public:
    explicit MessagesFilterModel(QObject *parent = 0);

    void classBegin();
    void componentComplete();

public slots:

private:
    MessagesBaseModel *_messagesModel;

protected:
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

signals:

};

#endif // MESSAGESFILTERMODEL_H
