#include "conversationfiltermodel.h"

ConversationFilterModel::ConversationFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    _complete(false),
    _sourceModel(NULL)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    setFilterRole(ConversationBaseModel::DataRole);
    setSortRole(ConversationBaseModel::TimestampRole);
    setDynamicSortFilter(true);
}

void ConversationFilterModel::classBegin()
{

}

void ConversationFilterModel::componentComplete()
{
    if (_sourceModel) {
        init();
    }
    _complete = true;
}

bool ConversationFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    uint lefttimestamp = sourceModel()->data(left, ConversationBaseModel::TimestampRole).toUInt();
    uint righttimestamp = sourceModel()->data(right, ConversationBaseModel::TimestampRole).toUInt();

    return lefttimestamp > righttimestamp;
}

ConversationBaseModel *ConversationFilterModel::getModel()
{
    return _sourceModel;
}

void ConversationFilterModel::setModel(ConversationBaseModel *newModel)
{
    if (!_sourceModel) {
        _sourceModel = newModel;

        if (_complete) {
            init();
        }

        Q_EMIT modelChanged();
    }
}

void ConversationFilterModel::init()
{
    setSourceModel(_sourceModel);
    sort(0);
}
