#ifndef CONVERSATIONFILTERMODEL_H
#define CONVERSATIONFILTERMODEL_H

#include <QSortFilterProxyModel>
#include <QQmlParserStatus>

#include "conversationbasemodel.h"

class ConversationFilterModel : public QSortFilterProxyModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
public:
    explicit ConversationFilterModel(QObject *parent = 0);

    void classBegin();
    void componentComplete();

    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

    Q_PROPERTY(ConversationBaseModel *model READ getModel WRITE setModel NOTIFY modelChanged)
    ConversationBaseModel *getModel();
    void setModel(ConversationBaseModel *newModel);

private:
    void init();

    bool _complete;
    ConversationBaseModel *_sourceModel;

public slots:

signals:
    void modelChanged();

};

#endif // CONVERSATIONFILTERMODEL_H
