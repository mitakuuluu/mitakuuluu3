#include "messagesfiltermodel.h"

MessagesFilterModel::MessagesFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
    setSortRole(Qt::UserRole + 2);
    setDynamicSortFilter(true);

    _messagesModel = MessagesBaseModel::GetInstance();
}

void MessagesFilterModel::classBegin()
{

}

void MessagesFilterModel::componentComplete()
{
    setSourceModel(_messagesModel);
    sort(0);
}

bool MessagesFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QString leftName = sourceModel()->data(left, Qt::UserRole + 2).toString();
    QString rightName = sourceModel()->data(right, Qt::UserRole + 2).toString();

    return leftName.localeAwareCompare(rightName) < 0;
}
