#ifndef CONTACTSBASEMODEL_H
#define CONTACTSBASEMODEL_H

#include <QObject>
#include <QHash>
#include <QVariantMap>
#include <QStringList>
#include <QAbstractListModel>

#include <QColor>

#include <QDebug>

#include "src/threadworker/queryexecutor.h"
#include "src/waclasses/mainconnection.h"
#include "src/platform/notifier.h"

class ContactsBaseModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int totalUnread READ getTotalUnread NOTIFY totalUnreadChanged)
    Q_PROPERTY(QStringList blacklist READ blacklist NOTIFY blacklistChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(bool busy READ isBusy NOTIFY busyChanged)
    Q_PROPERTY(QString activeJid READ activeJid WRITE setActiveJid NOTIFY activeJidChanged)
public:
    explicit ContactsBaseModel(QObject *parent = 0);

    static ContactsBaseModel *GetInstance(QObject *parent = 0);
    static QString getNicknameBy(const QString &jid, const QString &message, const QString &name, const QString &pushname);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const { return _roles; }

    Q_INVOKABLE void importConversations(const QString &type, const QString &login = QString());

    bool isBusy();

public slots:
    int count();
    void reloadContact(const QString &jid);
    bool setPropertyByJid(const QString &jid, const QString &name, const QVariant &value);
    void deleteContact(const QString &jid);
    QVariantMap getModel(const QString &jid);
    QVariantMap get(int index);
    QColor getColorForJid(const QString &jid);
    void renameContact(const QString &jid, const QString &name);
    void requestAvatar(const QString &jid);
    void clearChat(const QString &jid);
    QString createBroadcast(const QStringList &jids, const QString &name = QString());
    void renameBroadcast(const QString &jid, const QString &name);
    bool havePhone(const QString &phone);
    bool haveContact(const QString &jid);
    QString getNicknameByJid(const QString &jid);
    QString getAvatarByJid(const QString &jid);
    QStringList getJids();

    void clear();
    void contactsChanged();

    int getParticipantIndex(const QString &cjid, const QString &pjid);

private:
    void addParticipant(const QString &gjid, const QString &jid);
    void removeParticipant(const QString &gjid, const QString &jid);
    void adminSetParticipant(const QString &gjid, const QString &jid);
    void adminRemoveParticipant(const QString &gjid, const QString &jid);
    void groupLocked(const QString &gjid);
    void groupUnlocked(const QString &gjid);

    void checkLastMessage(const QString &jid);
    void setLastMessageStatus(const QString &jid, const QString &msgId, qint64 status);
    void setContactAvatar(const QString &jid, const QString &avatar, const QByteArray &data = QByteArray());
    void setLastMessage(const QString &jid, const QString &timestamp);

    QString getPictureForId(const QString &id);

    QColor generateColor();
    QHash<QString, QColor> _colors;

    QStringList _keys;

    QHash<QString, QVariantMap> _modelData;
    QHash<int, QByteArray> _roles;

    QueryExecutor *dbExecutor;
    MainConnection *connection;

    bool getAvailable(const QString &jid);
    bool getBlocked(const QString &jid);

    int _totalUnread;
    int getTotalUnread();
    void checkTotalUnread();

    QStringList _blockedContacts;
    QStringList blacklist() const;

    bool _busy;

    QString _activeJid;
    QString activeJid() const;
    void setActiveJid(const QString &activeJid);

    QStringList _availableContacts;

signals:
    void propertyChanged(const QString &pjid, const QString &pname, const QVariant &pvalue);
    void nicknameChanged(const QString &pjid, const QString &nickname);
    void statusChanged(const QString &pjid, const QString &message, int ptimestamp);
    void totalUnreadChanged();
    void deleteEverythingSuccessful();
    void conversationClean(const QString &pjid);
    void broadcastCreated(const QString &bjid, const QStringList &bjids);
    void blacklistChanged();
    void contactsEmpty();
    void countChanged();
    void busyChanged();
    void activeJidChanged();

private slots:
    void contactsSynced(const QVariantMap &contacts);
    void contactsStatuses(const QVariantMap &contacts);
    void contactsPictureIds(const QVariantMap &contacts);
    void contactPictureHidden(const QString &jid, const QString &code);
    void contactPicture(const QString &jid, const QByteArray &data, const QString &id);
    void contactPictureId(const QString &jid, const QString &id, const QString &author);
    void contactStatus(const QString &jid, const QString &status, const QString &t);
    void contactLastSeen(const QString &jid, uint timestamp);
    void contactLastSeenHidden(const QString &jid, const QString &code);
    void contactAvailable(const QString &jid);
    void contactUnavailable(const QString &jid, const QString &last);
    void groupsReceived(const QVariantMap &groups);
    void blacklistReceived(const QStringList &list);
    void broadcastsReceived(const QVariantMap &broadcasts);
    void groupSubjectChanged(const QString &jid, const QString &subject, const QString &s_o, const QString &s_t);
    void groupCreated(const QString &gjid, const QString &creation, const QString &creator, const QString &s_o, const QString &s_t, const QString &subject, const QStringList &participants, const QStringList &admins);
    void groupParticipantAdded(const QString &gjid, const QString &jid);
    void groupParticipantRemoved(const QString &gjid, const QString &jid);
    void groupParticipantPromoted(const QString &gjid, const QString &jid);
    void groupParticipantDemoted(const QString &gjid, const QString &jid);
    void groupInfo(const QString &jid, const QVariantMap &attributes);

    void textMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const QString &data);
    void mediaMessageReceived(const QString &jid, const QString &id, const QString &timestamp, const QString &author, bool offline, const AttributeList &attrs, const QByteArray &data);
    void textMessageSent(const QString &jid, const QString &id, const QString &timestamp, const QString &data);
    void messageSent(const QString &jid, const QString &msgId, const QString &timestamp);

    void messageReceipt(const QString &jid, const QString &msgId, const QString &participant, const QString &timestamp, const QString &type);
    void retryMessage(const QString &msgId, const QString &jid);

    void onContactsGetAll(const QVariantMap &reply);
    void onRetryMessage(const QVariantMap &reply);

    void onDownloadCompleted(const QString &jid, const QString &dest, const QString &msgid);

    void connectionStatusChanged();

    void pictureUpdated(const QString &jid, const QString &path);
    void groupInfo(const QVariantMap &data);
    void contactChanged(const QVariantMap &data);
    void contactSynced(const QVariantMap &data);
    void contactStatus(const QString &jid, const QString &message, int timestamp);
    void newGroupSubject(const QVariantMap &data);
    void setUnread(const QString &jid, int count);
    void pushnameUpdated(const QString &jid, const QString &pushName);
    void presenceAvailable(const QString &jid);
    void presenceUnavailable(const QString &jid);
    void presenceLastSeen(const QString jid, int timestamp);
    void messageReceived(const QVariantMap &data);
    void dbResults(const QVariant &result);
    void contactsBlocked(const QStringList &jids);
    void contactsAvailable(const QStringList &jids);
    void contactTyping(const QString &jid);
    void contactPaused(const QString &jid);
    void contactRemoved(const QString &jid);
};

#endif // CONTACTSBASEMODEL_H
