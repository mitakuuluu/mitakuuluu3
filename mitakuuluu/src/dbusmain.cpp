#include "dbusmain.h"
#include "src/providers/mediapreviewprovider.h"
#include "src/providers/imagebytesprovider.h"
#include "src/providers/videothumbnailprovider.h"
#include "src/waclasses/mainconnection.h"
#include "src/waclasses/registration.h"
#include "src/waclasses/emoji.h"
#include "src/models/countriesmodel.h"
#include "src/settings/appsettings.h"
#include "src/settings/accountsettings.h"
#include "src/settings/serverproperties.h"
#include "src/platform/simparameters.h"
#include "src/models/contactsfiltermodel.h"
#include "src/models/conversationbasemodel.h"
#include "src/models/conversationfiltermodel.h"
#include "src/platform/nativewindowhelper.h"
#include "src/models/messagesbasemodel.h"
#include "src/models/messagesfiltermodel.h"

#include <QStandardPaths>
#include <QDebug>

DBusMain::DBusMain(QObject *parent) :
    QObject(parent)
{
    view = NULL;
    notify = NULL;

    QObject::connect(this, SIGNAL(doShowUIAsync(QStringList)), this, SLOT(showUI(QStringList)));
}

void DBusMain::start()
{
    if (QDBusConnection::sessionBus().registerService(DBUS_SERVICE)) {
        QDBusConnection::sessionBus().registerObject(DBUS_PATH, this, QDBusConnection::ExportScriptableSlots);
        qDebug() << "DBus registered successfully!";

        qmlRegisterType<CountriesModel>("org.mitakuuluu3", 1, 0, "CountriesModel");
        qmlRegisterType<SimParameters>("org.mitakuuluu3", 1, 0, "SimParameters");
        qmlRegisterType<Registration>("org.mitakuuluu3", 1, 0, "Registration");
        qmlRegisterUncreatableType<MainConnection>("org.mitakuuluu3", 1, 0, "Connection", "Connection is static uncreatable type");

        qmlRegisterType<ContactsFilterModel>("org.mitakuuluu3", 1, 0, "ContactsFilterModel");
        qmlRegisterType<ConversationBaseModel>("org.mitakuuluu3", 1, 0, "ConversationBaseModel");
        qmlRegisterType<ConversationFilterModel>("org.mitakuuluu3", 1, 0, "ConversationFilterModel");

        qmlRegisterType<MessagesFilterModel>("org.mitakuuluu3", 1, 0, "MessagesFilterModel");

        QString dataFolder = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(dataFolder + "/avatars").exists()) {
            QDir(dataFolder).mkpath(dataFolder + "/avatars");
        }

        if (!qGuiApp->arguments().contains("daemon")) {
            showUI();
        }
    }
    else {
        qWarning() << "Application already running. Gracefully asking it to activate window...";
        QDBusInterface iface(DBUS_SERVICE, DBUS_PATH, DBUS_INTERFACE);
        iface.call(QDBus::NoBlock, "showUI");
        qGuiApp->quit();
    }
}

void DBusMain::showUIAsync(const QString &jid)
{
    qDebug() << jid;
    pendingJid = jid;

    notify->hide();
}

void DBusMain::showUI(const QStringList &params)
{
    if (notify) {
        //notify->hide();
    }

    if (!view) {
        qDebug() << "Construct view";
        view = SailfishApp::createView();
        QObject::connect(view, SIGNAL(destroyed()), this, SLOT(onViewDestroyed()));
        QObject::connect(view, SIGNAL(closing(QQuickCloseEvent*)), this, SLOT(onViewClosing(QQuickCloseEvent*)));
        view->setTitle("Mitakuuluu");
        bindClassesToView(view);
        if (params.size() > 0) {
            qDebug() << params;
        }
        view->rootContext()->setContextProperty("notificationActive", params.size() > 0 ? params.first() : QString());
        view->rootContext()->setContextProperty("qGuiApp", qGuiApp);
        view->rootContext()->setContextProperty("appSettings", AppSettings::GetInstance(this));
        view->rootContext()->setContextProperty("accountSettings", AccountSettings::GetInstance(this));
        view->rootContext()->setContextProperty("serverProperties", ServerProperties::GetInstance(this));
        view->rootContext()->setContextProperty("connection", MainConnection::GetInstance(this));
        view->setSource(SailfishApp::pathTo("qml/main.qml"));
        view->showFullScreen();
    }
    else {
        qDebug() << "Activating view";
        view->raise();
        view->requestActivate();
        if (params.size() > 0) {
            qDebug() << params;
            Q_EMIT notificationActivate(params.first());
        }
    }
}

void DBusMain::notificationCallback(const QString &jid, const QString &msgid)
{
    if (!AppSettings::GetInstance()->value("notificationsPreview", true).toBool()) {
        showUI(QStringList() << jid);
        return;
    }

    if (!notify) {
        qDebug() << "Construct notify";
        notify = SailfishApp::createView();

        QColor color;
        color.setRedF(0.0);
        color.setGreenF(0.0);
        color.setBlueF(0.0);
        color.setAlphaF(0.0);
        notify->setColor(color);
        notify->setClearBeforeRendering(true);

        NativeWindowHelper *nwh = new NativeWindowHelper(notify);
        QObject::connect(notify, SIGNAL(destroyed()), this, SLOT(onNotifyDestroyed()));
        QObject::connect(notify, SIGNAL(closing(QQuickCloseEvent*)), this, SLOT(onNotifyClosing(QQuickCloseEvent*)));
        QObject::connect(notify, SIGNAL(visibleChanged(bool)), this, SLOT(onNotifyVisibleChanged(bool)));
        notify->setTitle("Mitakuuluu notifier");
        bindClassesToView(notify);
        notify->rootContext()->setContextProperty("nativeWindow", nwh);
        notify->rootContext()->setContextProperty("window", notify);
        notify->rootContext()->setContextProperty("MessagesBaseModel", MessagesBaseModel::GetInstance(this));
        notify->setSource(SailfishApp::pathTo("qml/overlay.qml"));
        notify->create();
        nwh->setReady();
        nwh->setWindowCategory("notification");
        notify->showFullScreen();
        MessagesBaseModel::GetInstance()->clearNotifications();
    }
    else {
        qDebug() << "notification added";
        notify->raise();
        notify->requestActivate();
    }
}

void DBusMain::onViewDestroyed()
{
    qDebug() << "View destroyed";
    QObject::disconnect(view, 0, 0, 0);
    view = NULL;
    if (qGuiApp->quitOnLastWindowClosed() && !notify) {
        qGuiApp->quit();
    }
}

void DBusMain::onViewClosing(QQuickCloseEvent *)
{
    qDebug() << "View closed";
    view->destroy();
    view->deleteLater();
}

void DBusMain::onNotifyDestroyed()
{
    qDebug() << "Notify destroyed";
    QObject::disconnect(notify, 0, 0, 0);
    notify = NULL;
    if (qGuiApp->quitOnLastWindowClosed() && !view) {
        qGuiApp->quit();
    }
    else {
        MessagesBaseModel::GetInstance()->clear();

        if (!pendingJid.isEmpty()) {
            showUI(QStringList() << pendingJid);
            pendingJid.clear();
        }
    }
}

void DBusMain::onNotifyClosing(QQuickCloseEvent *)
{
    qDebug() << "Notify closed";
    notify->destroy();
    notify->deleteLater();
}

void DBusMain::onNotifyVisibleChanged(bool isVisible)
{
    if (!isVisible) {
        notify->deleteLater();
    }
}

void DBusMain::bindClassesToView(QQuickView *view)
{
    view->rootContext()->setContextProperty("main", this);
    view->rootContext()->setContextProperty("connection", MainConnection::GetInstance(this));
    view->rootContext()->setContextProperty("ContactsBaseModel", ContactsBaseModel::GetInstance(this));
    view->rootContext()->setContextProperty("Emoji", Emoji::GetInstance(this));
    view->engine()->addImageProvider("preview", new MediaPreviewProvider);
    view->engine()->addImageProvider("base64", new ImageBytesProvider);
    view->engine()->addImageProvider("thumbnail", new VideoThumbnailProvider);
}
