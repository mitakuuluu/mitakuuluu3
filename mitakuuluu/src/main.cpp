#include <QGuiApplication>
#include <sailfishapp.h>
#include <QScopedPointer>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QTextStream>

#include <grp.h>
#include <pwd.h>
#include <systemd/sd-journal.h>

#include "dbusmain.h"

static QFile logfile;
static QTextStream logstream;

const char* msgTypeToString(QtMsgType type)
{
    switch (type) {
    case QtDebugMsg:
        return "D";
    case QtWarningMsg:
        return "W";
    case QtCriticalMsg:
        return "C";
    case QtFatalMsg:
        return "F";
        //abort();
    default:
        return "D";
    }
}

QString fullLog(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    return QString("[%1 %2] %3:%4\n(%5 %6)\n%7\n").arg(msgTypeToString(type))
                                                  .arg(QDateTime::currentDateTime().toString("hh:mm:ss"))
                                                  .arg(context.file)
                                                  .arg(context.line)
                                                  .arg(context.category)
                                                  .arg(context.function)
                                                  .arg(message);
}

QString infoLog(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    return QString("%1 [%2] %3:%4\n%5\n%6\n").arg(QDateTime::currentDateTime().toString("hh:mm:ss"))
                                             .arg(msgTypeToString(type))
                                             .arg(context.file)
                                             .arg(context.line)
                                             .arg(context.function)
                                             .arg(message);
}

QString simpleLog(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    return QString("[%1] %2:%3\n%4\n%5\n").arg(msgTypeToString(type))
                                          .arg(context.file)
                                          .arg(context.line)
                                          .arg(context.function)
                                          .arg(message);
}

void writeLog(const QString &message)
{
    if (logfile.isOpen())
    {
        logstream << message;
    }
}

void printLog(const QString &message, QtMsgType type)
{
    switch (type) {
        case QtDebugMsg:
            QTextStream(stdout) << message;
            return;
        default:
            QTextStream(stderr) << message;
            return;
    }
}

void fileHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    writeLog(fullLog(type, context, msg));
    printLog(infoLog(type, context, msg), type);
    if (type == QtFatalMsg) {
        abort();
    }
}

void journalLog(const QString &message, QtMsgType type)
{
    int logType = LOG_NOTICE;
    switch (type) {
        case QtDebugMsg:
            logType = LOG_DEBUG;
            break;
        case QtWarningMsg:
            logType = LOG_WARNING;
            break;
        case QtCriticalMsg:
            logType = LOG_CRIT;
            break;
        case QtFatalMsg:
            logType = LOG_ALERT;
            break;
    }
    sd_journal_send("PRIORITY=%d", logType,
                    "MESSAGE=%s", message.toUtf8().constData(),
                    "_COMM=mitakuuluu3",
                    NULL);
}

void syslogHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    journalLog(infoLog(type, context, msg), type);
    if (type == QtFatalMsg) {
        abort();
    }
}

void printHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    printLog(infoLog(type, context, msg), type);
    if (type == QtFatalMsg) {
        abort();
    }
}

int main(int argc, char *argv[])
{
    setuid(getpwnam("nemo")->pw_uid);
    setgid(getgrnam("privileged")->gr_gid);

    logfile.setFileName(QString("%1/mitakuuluu3.log").arg(QDir::homePath()));
    if (logfile.exists()) {
        logfile.open(QIODevice::Append | QIODevice::Text);
        logstream.setDevice(&logfile);
        qInstallMessageHandler(fileHandler);
        QTextStream(stdout) << "Logging to file: " << logfile.fileName() << "\n";
    }
    else if (argc > 1 && strcmp(argv[1], "daemon") == 0) {
        qInstallMessageHandler(syslogHandler);
        QTextStream(stdout) << "Logging to system journal enabled with _COMM=mitakuuluu3\n";
    }
    else {
        qInstallMessageHandler(printHandler);
    }

    QScopedPointer<QGuiApplication> application(SailfishApp::application(argc, argv));
    application->setApplicationDisplayName("Mitakuuluu");
    application->setApplicationName("mitakuuluu3");
    application->setOrganizationName("mitakuuluu3");
    application->setApplicationVersion(QString(APP_VERSION));

    QScopedPointer<DBusMain> dbus(new DBusMain());
    QTimer::singleShot(1, dbus.data(), SLOT(start()));

    int retVal = application->exec();

    qDebug() << "Application closing";

    if (logfile.isOpen()) {
        logfile.close();
    }

    return retVal;
}

