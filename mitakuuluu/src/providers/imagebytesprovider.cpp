#include "imagebytesprovider.h"

#include <QBuffer>
#include <QImageReader>

ImageBytesProvider::ImageBytesProvider(): QQuickImageProvider(QQuickImageProvider::Image)
{
}

QImage ImageBytesProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    //size->setHeight(requestedSize.height());
    //size->setWidth(requestedSize.width());

    QByteArray data = QByteArray::fromBase64(id.toLatin1());
    QBuffer buf(&data);

    QImageReader reader(&buf);
    size->setHeight(reader.size().height());
    size->setWidth(reader.size().width());
    //reader.setScaledSize(requestedSize);
    QImage img = reader.read();

    //QImage img;
    //img.loadFromData(data);

    //size->setWidth(img.width());
    //size->setHeight(img.height());

    return img;
}
