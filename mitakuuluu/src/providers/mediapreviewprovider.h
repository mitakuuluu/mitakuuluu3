#ifndef MEDIAPREVIEWPROVIDER_H
#define MEDIAPREVIEWPROVIDER_H

#include <QQuickImageProvider>
#include <QImageReader>
#include <QStandardPaths>
#include <QFile>
#include <QDir>

class MediaPreviewProvider : public QQuickImageProvider
{
public:
    MediaPreviewProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);

private:
    QString dirPath;

};

#endif // MEDIAPREVIEWPROVIDER_H
