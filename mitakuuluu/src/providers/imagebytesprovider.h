#ifndef IMAGEBYTESPROVIDER_H
#define IMAGEBYTESPROVIDER_H

#include <QQuickImageProvider>

class ImageBytesProvider : public QQuickImageProvider
{
public:
    ImageBytesProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // IMAGEBYTESPROVIDER_H
