#include "videothumbnailprovider.h"

#include <QDebug>

VideoThumbnailProvider::VideoThumbnailProvider() :
    QQuickImageProvider(QQuickImageProvider::Image)
{
    dirPath = QString("%1/preview").arg(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    if (!QDir(dirPath).exists()) {
        QDir().mkpath(dirPath);
    }
}

QImage VideoThumbnailProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QString cacheName = id;
    QFile cache(QString("%1/%2.jpg").arg(dirPath).arg(cacheName.split("/").last().split(".").first()));
    if (!cache.exists()) {
        QImage thumbnail = createThumbnail(id, requestedSize, true);
        size->setHeight(thumbnail.height());
        size->setWidth(thumbnail.width());
        thumbnail.save(cache.fileName(), "JPEG", 90);
        return thumbnail;
    }
    else {
        QImageReader reader(cache.fileName());
        size->setHeight(reader.size().height());
        size->setWidth(reader.size().width());
        QImage cacheImage = reader.read();
        return cacheImage;
    }
}

typedef QImage (*CreateThumbnailFunc)(const QString &fileName, const QSize &requestedSize, bool crop);

QImage VideoThumbnailProvider::createThumbnail(const QString &fileName, const QSize &requestedSize, bool crop)
{
    QImage image;

    static CreateThumbnailFunc createThumbnail = (CreateThumbnailFunc)QLibrary::resolve(
                QLatin1String("/usr/lib/qt5/qml/org/nemomobile/thumbnailer/thumbnailers/libvideothumbnailer.so"), "createThumbnail");

    if (createThumbnail) {
        image = createThumbnail(fileName, requestedSize, crop);

    } else {
        qWarning("Cannot generate video thumbnail, thumbnailer function not available.");
    }

    return image;
}
