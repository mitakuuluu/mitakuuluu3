#ifndef VIDEOTHUMBNAILPROVIDER_H
#define VIDEOTHUMBNAILPROVIDER_H

#include <QQuickImageProvider>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QLibrary>
#include <QImage>
#include <QImageReader>

class VideoThumbnailProvider : public QQuickImageProvider
{
public:
    VideoThumbnailProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
    QImage createThumbnail(const QString &fileName, const QSize &requestedSize, bool crop);

private:
    QString dirPath;

};

#endif // VIDEOTHUMBNAILPROVIDER_H
