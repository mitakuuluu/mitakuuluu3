#include "mediapreviewprovider.h"

MediaPreviewProvider::MediaPreviewProvider():
    QQuickImageProvider(QQuickImageProvider::Image)
{
    dirPath = QString("%1/preview").arg(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    if (!QDir(dirPath).exists()) {
        QDir().mkpath(dirPath);
    }
}

QImage MediaPreviewProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QString mediaName = QString(id).split("/").last();

    QString filePath = QString("%1/%2").arg(dirPath).arg(mediaName);
    if (!QFile(filePath).exists()) {
        QString mediaPath = QString("%1/media/%2").arg(QStandardPaths::writableLocation(QStandardPaths::DataLocation)).arg(mediaName);
        if (QFile(mediaPath).exists()) {
            QImageReader reader(mediaPath);
            reader.setScaledSize(requestedSize);
            size->setWidth(requestedSize.width());
            size->setHeight(requestedSize.height());
            QImage res = reader.read();
            res.save(filePath);
            return res;
        }
        else {
            size->setWidth(0);
            size->setHeight(0);
            return QImage();
        }
    }
    else {
        QImage res(filePath);
        size->setWidth(res.width());
        size->setHeight(res.height());
        return res;
    }
}
