#ifndef QUERYEXECUTOR_H
#define QUERYEXECUTOR_H

#include <QMap>
#include <QVariantMap>
#include <QStringList>
#include <QtSql/QtSql>

#include "threadworker.h"

#define QUERYREPLY(a) #a

namespace QueryType {
    enum EnumType {
        ContactsGetAll = 0,
        ContactsSynced,
        ContactsStatuses,
        ContactStatus,
        ContactAvatars,
        ContactLastSeen,
        ContactSetLastmessage,
        ContactSetUnread,
        ContactAdded,
        ContactSetProperty,
        GroupsParticipating,
        GroupSubjectChanged,
        BroadcastLists,
        MessageReceived,
        MessageReceipt,
        MessageSent,
        RetryMessage,
        MessageStatus,
        ConversationLoad,
        ConversationCount,
        ConversationsImport,
        MediaDownloadComplete
    };
}

class QueryExecutor : public QObject
{
    Q_OBJECT
public:
    explicit QueryExecutor(QObject *parent = 0);
    static QueryExecutor *GetInstance(QObject *parent = 0);

public slots:
    void queueAction(const QVariant &msg, int priority = 0);
    void processAction(const QVariant &msg);

private:
    void processQuery(const QVariant &msg);

    QString tableFromJid(QString jid);

    void contactsGetAll(QVariantMap &query);
    void contactsSynced(QVariantMap &query);
    void contactsStatuses(QVariantMap &query);
    void contactStatus(QVariantMap &query);
    void contactAvatars(QVariantMap &query);
    void contactLastSeen(QVariantMap &query);
    void contactSetLastmessage(QVariantMap &query);
    void contactSetUnread(QVariantMap &query);
    void contactAdded(QVariantMap &query);
    void contactSetProperty(QVariantMap &query);
    void groupsParticipating(QVariantMap &query);
    void groupSubjectChanged(QVariantMap &query);
    void broadcastLists(QVariantMap &query);
    void messageReceived(QVariantMap &query);
    void messageReceipt(QVariantMap &query);
    void messageSent(QVariantMap &query);
    void retryMessage(QVariantMap &query);
    void messageStatus(QVariantMap &query);
    void conversationLoad(QVariantMap &query);
    void conversationCount(QVariantMap &query);
    void conversationsImport(QVariantMap &query);
    void mediaDownloadComplete(QVariantMap &query);

    ThreadWorker m_worker;
    QSqlDatabase db;
};

#endif // QUERYEXECUTOR_H
