﻿#include <QThreadPool>
#include <QMetaObject>
#include <QStandardPaths>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonParseError>

#include "queryexecutor.h"

#define DATABASE_CONNECTION "queryexecutor_shared_connection"

#define CONTACTS_CREATE "id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT, pushname TEXT, name TEXT, message TEXT, known INTEGER, owner TEXT, subowner TEXT, timestamp INTEGER, subtimestamp INTEGER, avatar TEXT, phoneavatar TEXT, unread INTEGER, lastmessage INTEGER, muting INTEGER, hidden INTEGER, participants TEXT, admins TEXT"
#define CONTACTS_INSERT "NULL, :jid, :pushname, :name, :message, :known, :owner, :subowner, :timestamp, :subtimestamp, :avatar, :phoneavatar, :unread, :lastmessage, :muting, :hidden, :participants, :admins"

#define MESSAGES_CREATE "id INTEGER PRIMARY KEY AUTOINCREMENT, msgid TEXT, jid TEXT, author TEXT, timestamp INTEGER, data TEXT, status INTEGER, msgtype INTEGER, params TEXT, deliveryreceipts TEXT, readreceipts TEXT, recepients INTEGER"
#define MESSAGES_INSERT "NULL, :msgid, :jid, :author, :timestamp, :data, :status, :msgtype, :params, :deliveryreceipts, :readreceipts, :recepients"

QueryExecutor::QueryExecutor(QObject *parent) :
    QObject(parent)
{
    m_worker.setCallObject(this);

    db = QSqlDatabase::database(DATABASE_CONNECTION);
    if (!db.isOpen()) {
        qDebug() << "QE Opening database";
        db = QSqlDatabase::addDatabase("QSQLITE", DATABASE_CONNECTION);

        QString dataDir = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        QString dataFile = QString("%1/database.db").arg(dataDir);

        QDir dir(dataDir);
        if (!dir.exists())
            dir.mkpath(dataDir);
        qDebug() << "DB Dir:" << dataDir;
        db.setDatabaseName(dataFile);
        qDebug() << "DB Name:" << db.databaseName();
        if (db.open())
            qDebug() << "QE opened database" << db.connectionName();
        else
            qWarning() << "QE failed to open database";
    }
    else {
        qWarning() << "QE used existing DB connection:" << db.connectionName();
    }

    if (db.isOpen()) {
        if (!db.tables().contains("contacts")) {
            /*
            id <- AUTO
            jid
            pushname
            name
            message
            known
            owner
            subowner
            timestamp
            subtimestamp
            avatar
            phoneavatar
            unread
            lastmessage
            muting
            hidden
            participants
            admins
            */
            db.exec("CREATE TABLE contacts ("CONTACTS_CREATE");");
        }
    }
}

QueryExecutor* QueryExecutor::GetInstance(QObject *parent)
{
    static QueryExecutor* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new QueryExecutor(parent);
    }
    return lsSingleton;
}

void QueryExecutor::queueAction(const QVariant &msg, int priority) {
    m_worker.queueAction(msg, priority);
}

void QueryExecutor::processAction(const QVariant &message) {
    processQuery(message);
}

void QueryExecutor::processQuery(const QVariant &msg)
{
    QVariantMap query = msg.toMap();
    //qDebug() << "QE Processing query:" << query["type"];
    if (!query.isEmpty()) {
        switch (query["type"].toInt()) {
        case QueryType::ContactsGetAll: {
            contactsGetAll(query);
            break;
        }
        case QueryType::ContactsSynced: {
            contactsSynced(query);
            break;
        }
        case QueryType::ContactsStatuses: {
            contactsStatuses(query);
            break;
        }
        case QueryType::ContactStatus: {
            contactStatus(query);
            break;
        }
        case QueryType::ContactAvatars: {
            contactAvatars(query);
            break;
        }
        case QueryType::ContactLastSeen: {
            contactLastSeen(query);
            break;
        }
        case QueryType::ContactSetLastmessage: {
            contactSetLastmessage(query);
            break;
        }
        case QueryType::ContactSetUnread: {
            contactSetUnread(query);
            break;
        }
        case QueryType::ContactAdded: {
            contactAdded(query);
            break;
        }
        case QueryType::ContactSetProperty: {
            contactSetProperty(query);
            break;
        }
        case QueryType::GroupsParticipating: {
            groupsParticipating(query);
            break;
        }
        case QueryType::GroupSubjectChanged: {
            groupSubjectChanged(query);
            break;
        }
        case QueryType::BroadcastLists: {
            broadcastLists(query);
            break;
        }
        case QueryType::MessageReceived: {
            messageReceived(query);
            break;
        }
        case QueryType::MessageSent: {
            messageSent(query);
            break;
        }
        case QueryType::MessageReceipt: {
            messageReceipt(query);
            break;
        }
        case QueryType::RetryMessage: {
            retryMessage(query);
            break;
        }
        case QueryType::MessageStatus: {
            messageStatus(query);
            break;
        }
        case QueryType::ConversationLoad: {
            conversationLoad(query);
            break;
        }
        case QueryType::ConversationCount: {
            conversationCount(query);
            break;
        }
        case QueryType::ConversationsImport: {
            conversationsImport(query);
            break;
        }
        case QueryType::MediaDownloadComplete: {
            mediaDownloadComplete(query);
            break;
        }
        default: {
            break;
        }
        }
        if (query.contains("object")) {
            QObject *object = query["object"].value<QObject*>();
            QMetaObject::invokeMethod(object, query["method"].toByteArray().constData(), Q_ARG(QVariantMap, query));
        }
    }
}

QString QueryExecutor::tableFromJid(QString jid)
{
    QString prefix = jid.contains("@broadcast") ? "b" : "u";
    return QString(prefix + jid.split("@").first().replace("-", "g"));
}

void QueryExecutor::contactsGetAll(QVariantMap &query)
{
    QSqlQuery sql("SELECT * FROM contacts WHERE jid IS NOT NULL;", db);
    QVariantList contacts;
    while (sql.next()) {
        QVariantMap contact;
        for (int i = 0; i < sql.record().count(); i ++) {
            contact[sql.record().fieldName(i)] = sql.value(i);
        }

        QString jid = contact["jid"].toString();
        QString table = tableFromJid(jid);
        uint lastmessage = contact["lastmessage"].toUInt();
        if (db.tables().contains(table) && lastmessage > 0) {
            QSqlQuery msg(db);
            msg.prepare(QString("SELECT * FROM %1 ORDER BY timestamp DESC LIMIT 1;").arg(table));
            msg.exec();

            if (msg.next()) {
                QVariantMap message;
                for (int i = 0; i < msg.record().count(); i ++) {
                    message[msg.record().fieldName(i)] = msg.value(i);
                }
                QJsonParseError error;
                message["deliveryreceipts"] = QJsonDocument::fromJson(message["deliveryreceipts"].toString().toUtf8(), &error).toVariant().toMap();
                message["readreceipts"] = QJsonDocument::fromJson(message["readreceipts"].toString().toUtf8(), &error).toVariant().toMap();
                contact["lastmodel"] = message;
            }
        }

        contacts.append(contact);
    }
    query["contacts"] = contacts;
}

void QueryExecutor::contactsSynced(QVariantMap &query)
{
    QVariantMap contacts = query["contacts"].toMap();

    db.transaction();

    QMapIterator<QString, QVariant> iter(contacts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QString name = iter.value().toMap()["name"].toString();
        QString phoneavatar = iter.value().toMap()["avatar"].toString();

        QSqlQuery uc(db);
        uc.prepare("UPDATE contacts SET name=(:name), known=(:known), phoneavatar=(:phoneavatar) WHERE jid=(:jid);");
        uc.bindValue(":phoneavatar", phoneavatar);
        uc.bindValue(":name", name);
        uc.bindValue(":known", 1);
        uc.bindValue(":jid", jid);
        uc.exec();

        if (uc.lastError().type() != QSqlError::NoError)
            qDebug() << "[contacts] Update pushname result:" << uc.lastError();

        if (uc.numRowsAffected() == 0) {
            qDebug() << "insert new contact:" << name;
            QSqlQuery ic(db);
            ic.prepare("INSERT INTO contacts VALUES ("CONTACTS_INSERT");");
            ic.bindValue(":jid", jid);
            ic.bindValue(":pushname", QString());
            ic.bindValue(":name", name);
            ic.bindValue(":message", QString());
            ic.bindValue(":known", 1);
            ic.bindValue(":owner", "");
            ic.bindValue(":subowner", "");
            ic.bindValue(":timestamp", 0);
            ic.bindValue(":subtimestamp", 0);
            ic.bindValue(":avatar", "");
            ic.bindValue(":phoneavatar", phoneavatar);
            ic.bindValue(":unread", 0);
            ic.bindValue(":lastmessage", 0);
            ic.bindValue(":muting", 0);
            ic.bindValue(":hidden", 0);
            ic.bindValue(":participants", jid);
            ic.bindValue(":admins", QString());
            ic.exec();

            if (ic.lastError().type() != QSqlError::NoError)
                qDebug() << "[contacts] Insert error:" << ic.lastError();
        }
    }

    db.commit();
}

void QueryExecutor::contactsStatuses(QVariantMap &query)
{
    QVariantMap contacts = query["contacts"].toMap();

    db.transaction();

    QMapIterator<QString, QVariant> iter(contacts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QVariantMap data = iter.value().toMap();
        data["jid"] = jid;

        contactStatus(data);
    }

    db.commit();
}

void QueryExecutor::contactStatus(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET message=(:message), subtimestamp=(:subtimestamp) WHERE jid=(:jid);");
    sql.bindValue(":message", query["status"]);
    sql.bindValue(":subtimestamp", query["t"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::contactAvatars(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET avatar=(:avatar) WHERE jid=(:jid);");
    sql.bindValue(":avatar", query["avatar"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::contactLastSeen(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET timestamp=(:timestamp) WHERE jid=(:jid);");
    sql.bindValue(":timestamp", query["t"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::contactSetLastmessage(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET lastmessage=(:lastmessage) WHERE jid=(:jid);");
    sql.bindValue(":lastmessage", query["lastmessage"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::contactSetUnread(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET unread=(:unread) WHERE jid=(:jid);");
    sql.bindValue(":unread", query["unread"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::contactAdded(QVariantMap &query)
{
    QVariantMap contact = query["contact"].toMap();

    QSqlQuery uc(db);
    uc.prepare("UPDATE contacts SET name=(:name), known=(:known), phoneavatar=(:phoneavatar) WHERE jid=(:jid);");
    uc.bindValue(":phoneavatar", contact["phoneavatar"]);
    uc.bindValue(":name", contact["name"]);
    uc.bindValue(":known", 1);
    uc.bindValue(":jid", contact["jid"]);
    uc.exec();

    if (uc.lastError().type() != QSqlError::NoError) {
        qDebug() << "[contacts] Update pushname result:" << uc.lastError();
    }

    if (uc.numRowsAffected() == 0) {
        qDebug() << "insert new contact:" << contact["name"].toString();
        QSqlQuery ic(db);
        ic.prepare("INSERT INTO contacts VALUES ("CONTACTS_INSERT");");
        ic.bindValue(":jid", contact["jid"]);
        ic.bindValue(":pushname", contact["pushname"]);
        ic.bindValue(":name", contact["name"]);
        ic.bindValue(":message", contact["message"]);
        ic.bindValue(":known", contact["known"].toBool() ? 1 : 0);
        ic.bindValue(":owner", contact["owner"]);
        ic.bindValue(":subowner", contact["subowner"]);
        ic.bindValue(":timestamp", contact["timestamp"]);
        ic.bindValue(":subtimestamp", contact["subtimestamp"]);
        ic.bindValue(":avatar", contact["avatar"]);
        ic.bindValue(":phoneavatar", contact["phoneavatar"]);
        ic.bindValue(":unread", contact["unread"]);
        ic.bindValue(":lastmessage", contact["lastmessage"]);
        ic.bindValue(":muting", contact["muting"]);
        ic.bindValue(":hidden", contact["hidden"]);
        ic.bindValue(":participants", contact["participants"].toStringList().join(";"));
        ic.bindValue(":admins", contact["admins"].toStringList().join(";"));
        ic.exec();

        if (ic.lastError().type() != QSqlError::NoError)
            qDebug() << "[contacts] Insert error:" << ic.lastError();
    }
}

void QueryExecutor::contactSetProperty(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare(QString("UPDATE contacts SET %1=(:value) WHERE jid=(:jid);").arg(query["property"].toString()));
    sql.bindValue(":value", query["value"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();

    if (sql.lastError().type() != QSqlError::NoError) {
        qDebug() << sql.lastError().text();
    }
}

void QueryExecutor::groupsParticipating(QVariantMap &query)
{
    QVariantMap groups = query["groups"].toMap();

    db.transaction();

    QMapIterator<QString, QVariant> iter(groups);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QVariantMap group = iter.value().toMap();

        QSqlQuery uc(db);
        uc.prepare("UPDATE contacts SET name=(:name), owner=(:owner), subowner=(:subowner), subtimestamp=(:subtimestamp), timestamp=(:timestamp), participants=(:participants), admins=(:admins) WHERE jid=(:jid);");
        uc.bindValue(":name", group["subject"]);
        uc.bindValue(":owner", group["owner"]);
        uc.bindValue(":subowner", group["s_o"]);
        uc.bindValue(":subtimestamp", group["s_t"]);
        uc.bindValue(":timestamp", group["creation"]);
        uc.bindValue(":participants", group["participants"].toStringList().join(";"));
        uc.bindValue(":admins", group["admins"].toStringList().join(";"));
        uc.bindValue(":jid", jid);
        uc.exec();

        if (uc.lastError().type() != QSqlError::NoError)
            qDebug() << "[groups] Update pushname result:" << uc.lastError();

        if (uc.numRowsAffected() == 0) {
            qDebug() << "insert new group:" << group["subject"];
            QSqlQuery ic(db);
            ic.prepare("INSERT INTO contacts VALUES ("CONTACTS_INSERT");");
            ic.bindValue(":jid", jid);
            ic.bindValue(":pushname", QString());
            ic.bindValue(":name", group["subject"]);
            ic.bindValue(":message", QString());
            ic.bindValue(":known", 1);
            ic.bindValue(":owner", group["owner"]);
            ic.bindValue(":subowner", group["s_o"]);
            ic.bindValue(":timestamp", group["creation"]);
            ic.bindValue(":subtimestamp", group["s_t"]);
            ic.bindValue(":avatar", "/usr/share/mitakuuluu3/images/avatar-empty-group.png");
            ic.bindValue(":phoneavatar", "");
            ic.bindValue(":unread", 0);
            ic.bindValue(":lastmessage", QDateTime::currentDateTime().toTime_t());
            ic.bindValue(":muting", 0);
            ic.bindValue(":hidden", 0);
            ic.bindValue(":participants", group["participants"].toStringList().join(";"));
            ic.bindValue(":admins", group["admins"].toStringList().join(";"));
            ic.exec();

            if (ic.lastError().type() != QSqlError::NoError)
                qDebug() << "[groups] Insert error:" << ic.lastError();
        }
    }

    db.commit();
}

void QueryExecutor::groupSubjectChanged(QVariantMap &query)
{
    QSqlQuery sql(db);
    sql.prepare("UPDATE contacts SET name=(:name), subowner=(:subowner), subtimestamp=(:subtimestamp) WHERE jid=(:jid);");
    sql.bindValue(":name", query["name"]);
    sql.bindValue(":subowner", query["subowner"]);
    sql.bindValue(":subtimestamp", query["subtimestamp"]);
    sql.bindValue(":jid", query["jid"]);
    sql.exec();
}

void QueryExecutor::broadcastLists(QVariantMap &query)
{
    QVariantMap broadcasts = query["broadcasts"].toMap();

    db.transaction();

    QMapIterator<QString, QVariant> iter(broadcasts);
    while (iter.hasNext()) {
        iter.next();
        QString jid = iter.key();
        QVariantMap broadcast = iter.value().toMap();

        QSqlQuery uc(db);
        uc.prepare("UPDATE contacts SET name=(:name), owner=(:owner), subowner=(:subowner), subtimestamp=(:subtimestamp), timestamp=(:timestamp), participants=(:participants), admins=(:admins) WHERE jid=(:jid);");
        uc.bindValue(":name", broadcast["name"]);
        uc.bindValue(":owner", QString());
        uc.bindValue(":subowner", QString());
        uc.bindValue(":subtimestamp", 0);
        uc.bindValue(":timestamp", 0);
        uc.bindValue(":participants", broadcast["participants"].toStringList().join(";"));
        uc.bindValue(":admins", QString());
        uc.bindValue(":jid", jid);
        uc.exec();

        if (uc.lastError().type() != QSqlError::NoError)
            qDebug() << "[broadcasts] Update result:" << uc.lastError();

        if (uc.numRowsAffected() == 0) {
            qDebug() << "insert new broadcasts:" << broadcasts["name"];
            QSqlQuery ic(db);
            ic.prepare("INSERT INTO contacts VALUES ("CONTACTS_INSERT");");
            ic.bindValue(":jid", jid);
            ic.bindValue(":pushname", QString());
            ic.bindValue(":name", broadcast["name"]);
            ic.bindValue(":message", QString());
            ic.bindValue(":known", 1);
            ic.bindValue(":owner", QString());
            ic.bindValue(":subowner", QString());
            ic.bindValue(":timestamp", 0);
            ic.bindValue(":subtimestamp", 0);
            ic.bindValue(":avatar", "/usr/share/mitakuuluu3/images/avatar-broadcast.png");
            ic.bindValue(":phoneavatar", "");
            ic.bindValue(":unread", 0);
            ic.bindValue(":lastmessage", QDateTime::currentDateTime().toTime_t());
            ic.bindValue(":muting", 0);
            ic.bindValue(":hidden", 0);
            ic.bindValue(":participants", broadcast["participants"].toStringList().join(";"));
            ic.bindValue(":admins", QString());
            ic.exec();

            if (ic.lastError().type() != QSqlError::NoError)
                qDebug() << "[broadcast] Insert error:" << ic.lastError();
        }
    }

    db.commit();
}

void QueryExecutor::messageReceived(QVariantMap &query)
{
    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);

    //qDebug() << jid << table << query;

    if (!db.tables().contains(table)) {
        db.exec(QString("CREATE TABLE %1 ("MESSAGES_CREATE");").arg(table));
    }

    QSqlQuery msg(db);
    msg.prepare(QString("INSERT INTO %1 VALUES ("MESSAGES_INSERT");").arg(table));

    QVariantMap message = query["message"].toMap();
    QMapIterator<QString,QVariant> iter(message);
    while (iter.hasNext()) {
        iter.next();
        msg.bindValue(QString(":%1").arg(iter.key()), iter.value());
    }

    msg.exec();

    if (msg.lastError().type() != QSqlError::NoError)
        qDebug() << "[message] Insert error:" << msg.lastError();
}

void QueryExecutor::messageReceipt(QVariantMap &query)
{
    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);
    QString receiptType = query["receiptType"].toString();

    QSqlQuery sql(db);
    sql.prepare(QString("UPDATE %1 SET %2=(:%2) WHERE msgid=(:msgid);").arg(table).arg(receiptType));
    sql.bindValue(":msgid", query["msgid"]);
    sql.bindValue(QString(":%1").arg(receiptType), query["receipt"]);
    sql.exec();
}

void QueryExecutor::messageSent(QVariantMap &query)
{
    //qDebug() << query;

    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);

    //qDebug() << jid << table << query;

    if (!db.tables().contains(table)) {
        db.exec(QString("CREATE TABLE %1 ("MESSAGES_CREATE");").arg(table));
    }

    QSqlQuery msg(db);
    msg.prepare(QString("INSERT INTO %1 VALUES ("MESSAGES_INSERT");").arg(table));
    QVariantMap message = query["message"].toMap();
    QMapIterator<QString,QVariant> iter(message);
    while (iter.hasNext()) {
        iter.next();
        msg.bindValue(QString(":%1").arg(iter.key()), iter.value());
    }
    msg.exec();

    if (msg.lastError().type() != QSqlError::NoError)
        qDebug() << "[message] Insert error:" << msg.lastError();
}

void QueryExecutor::retryMessage(QVariantMap &query)
{
    QString msgId = query["msgid"].toString();
    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);

    QSqlQuery msg(db);
    msg.prepare(QString("SELECT data FROM %1 WHERE msgid=(:msgid);").arg(table));
    msg.bindValue(":msgid", msgId);
    msg.exec();

    if (msg.next()) {
        QString data = msg.value(0).toString();
        query["data"] = data;
    }
}

void QueryExecutor::messageStatus(QVariantMap &query)
{
    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);

    QSqlQuery sql(db);
    sql.prepare(QString("UPDATE %1 SET status=(:status) WHERE msgid=(:msgid);").arg(table));
    sql.bindValue(":status", query["status"]);
    sql.bindValue(":msgid", query["msgid"]);
    sql.exec();
}

void QueryExecutor::conversationLoad(QVariantMap &query)
{
    QString jid = query["jid"].toString();
    QString table = tableFromJid(jid);
    int limit = query["limit"].toInt();
    uint timestamp = query["timestamp"].toUInt();

    QSqlQuery msg(db);
    if (limit == 0) {
        msg.prepare(QString("SELECT * FROM %1 ORDER BY timestamp DESC;").arg(table));
    }
    else {
        if (timestamp > 0) {
            msg.prepare(QString("SELECT * FROM %1 WHERE timestamp<%3 ORDER BY timestamp DESC LIMIT %2;").arg(table).arg(limit).arg(timestamp));
        }
        else {
            msg.prepare(QString("SELECT * FROM %1 ORDER BY timestamp DESC LIMIT %2;").arg(table).arg(limit));
        }
    }
    msg.exec();

    QVariantList messages;
    while (msg.next()) {
        QVariantMap message;
        for (int i = 0; i < msg.record().count(); i ++) {
            message[msg.record().fieldName(i)] = msg.value(i);
        }
        QJsonParseError error;
        message["section"] = QDateTime::fromMSecsSinceEpoch(0).daysTo(QDateTime::fromTime_t(message["timestamp"].toUInt()));
        message["params"] = QJsonDocument::fromJson(message["params"].toString().toUtf8(), &error).toVariant().toMap();
        message["deliveryreceipts"] = QJsonDocument::fromJson(message["deliveryreceipts"].toString().toUtf8(), &error).toVariant().toMap();
        message["readreceipts"] = QJsonDocument::fromJson(message["readreceipts"].toString().toUtf8(), &error).toVariant().toMap();
        messages.append(message);
    }
    query["messages"] = messages;
}

void QueryExecutor::conversationCount(QVariantMap &query)
{
    QString table = tableFromJid(query["jid"].toString());

    QSqlQuery c(db);
    c.prepare(QString("SELECT COUNT(*) FROM %1;").arg(table));
    c.exec();

    int count = 0;
    if (c.next()) {
        count = c.value(0).toInt();
    }
    query["count"] = count;
}

void QueryExecutor::conversationsImport(QVariantMap &query)
{
    QVariantMap conversations = query["conversations"].toMap();
    QString type = query["source"].toString();
    QString myJid = QString("%1@s.whatsapp.net").arg(query["login"].toString());

    db.transaction();

    foreach (const QString &jid, conversations.keys()) {
        QString table = tableFromJid(jid);
        bool isPrivate = jid.contains("s.whatsapp.net");
        if (!db.tables().contains(table)) {
            db.exec(QString("CREATE TABLE %1 ("MESSAGES_CREATE");").arg(table));
        }

        QVariantList messages = conversations[jid].toList();
        foreach (const QVariant &messageVariant, messages) {
            QVariantMap message = messageVariant.toMap();
            QSqlQuery msg(db);
            msg.prepare(QString("INSERT OR REPLACE INTO %1 VALUES ("MESSAGES_INSERT");").arg(table));
            if (type == "android") {
                bool fromMe = message["key_from_me"].toInt() == 1;
                msg.bindValue(":msgid", message["key_id"]);
                msg.bindValue(":jid", jid);
                msg.bindValue(":author", fromMe ? myJid : (isPrivate ? jid : message["remote_resource"]));
                msg.bindValue(":timestamp", message["timestamp"].toULongLong() / 1000);
                msg.bindValue(":data", message["data"]);
                msg.bindValue(":status", fromMe ? 1 : 0);
                msg.bindValue(":msgtype", 0);
                msg.bindValue(":params", "");
                msg.bindValue(":deliveryreceipts", "");
                msg.bindValue(":readreceipts", "");
                msg.bindValue(":recepients", isPrivate ? jid : message["remote_resource"]);
            }
            else if (type == "whatsup") {
                if (message["media"].toString().isEmpty()) {
                    QString keyString = message["key"].toString();
                    //keyString = "{" + keyString.mid(4, keyString.size() - 5).replace("True", "true").replace("False", "false").replace("=", ":").replace("idd", "\"idd\"").replace("from_me", "\"from_me\"").replace("remote_jid", "\"remote_jid\"") + "}";
                    keyString = "{" + keyString.mid(4, keyString.size() - 5).replace("True", "true").replace("False", "false").replace(QRegExp("(\\w+)="), "\"\\1\":") + "}";
                    QJsonDocument doc = QJsonDocument::fromJson(keyString.toUtf8());
                    QVariantMap whatsupKeyMap = doc.toVariant().toMap();
                    bool fromMe = whatsupKeyMap["from_me"].toBool();

                    msg.bindValue(":msgid", whatsupKeyMap["idd"]);
                    msg.bindValue(":jid", jid);
                    msg.bindValue(":author", fromMe ? myJid : (isPrivate ? jid : whatsupKeyMap["remote_jid"]));
                    msg.bindValue(":timestamp", message["timestamp"]);
                    msg.bindValue(":data", QString(QByteArray::fromBase64(message["content"].toString().toUtf8())));
                    msg.bindValue(":status", fromMe ? 1 : 0);
                    msg.bindValue(":msgtype", 0);
                    msg.bindValue(":params", "");
                    msg.bindValue(":deliveryreceipts", "");
                    msg.bindValue(":readreceipts", "");
                    msg.bindValue(":recepients", isPrivate ? jid : whatsupKeyMap["remote_jid"]);
                }
                else {
                    continue;
                }
            }
            else if (type == "mitakuuluu") {
                bool fromMe = message["jid"].toString() == myJid;
                msg.bindValue(":msgid", message["msgid"]);
                msg.bindValue(":jid", message["jid"]);
                msg.bindValue(":author", message["author"]);
                msg.bindValue(":timestamp", message["timestamp"]);
                msg.bindValue(":data", message["data"]);
                msg.bindValue(":status", fromMe ? 1 : 0);
                msg.bindValue(":msgtype", 0);
                msg.bindValue(":params", "");
                msg.bindValue(":deliveryreceipts", "");
                msg.bindValue(":readreceipts", "");
                msg.bindValue(":recepients", isPrivate ? jid : message["author"]);
            }
            msg.exec();

            if (msg.lastError().type() != QSqlError::NoError) {
                qDebug() << msg.lastError().text();
            }
        }
    }

    db.commit();
}

void QueryExecutor::mediaDownloadComplete(QVariantMap &query)
{
    QString table = tableFromJid(query["jid"].toString());

    db.transaction();

    QSqlQuery q(db);
    q.prepare(QString("SELECT params FROM %1 WHERE msgid=(:msgid);").arg(table));
    q.bindValue(":msgid", query["msgid"]);
    q.exec();

    if (q.next()) {
        QString params = q.value(0).toString();
        if (!params.isEmpty()) {
            QVariantMap var = QJsonDocument::fromJson(params.toUtf8()).toVariant().toMap();
            var["local"] = query["local"];
            params = QString(QJsonDocument::fromVariant(var).toJson(QJsonDocument::Compact));

            QSqlQuery sql(db);
            sql.prepare(QString("UPDATE %1 SET params=(:params) WHERE msgid=(:msgid);").arg(table));
            sql.bindValue(":msgid", query["msgid"]);
            sql.bindValue(":params", params);
            sql.exec();
        }
    }

    db.commit();
}
