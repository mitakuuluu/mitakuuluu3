#ifndef DBUSMAIN_H
#define DBUSMAIN_H

#include <QObject>
#include <QDBusConnection>
#include <QDBusInterface>

#include <QGuiApplication>
#include <QQuickView>
#include <QtQml>
#include "sailfishapp.h"

#include "constants.h"

class DBusMain : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", DBUS_INTERFACE)
public:
    explicit DBusMain(QObject *parent = 0);

public slots:
    void start();

    Q_INVOKABLE  void showUIAsync(const QString &jid = QString());
    Q_SCRIPTABLE void showUI(const QStringList &params = QStringList());
    Q_SCRIPTABLE void notificationCallback(const QString &jid, const QString &msgid);

private slots:
    void onViewDestroyed();
    void onViewClosing(QQuickCloseEvent *);
    void onNotifyDestroyed();
    void onNotifyClosing(QQuickCloseEvent *);
    void onNotifyVisibleChanged(bool isVisible);

private:
    void bindClassesToView(QQuickView *view);

    QQuickView *view;
    QQuickView *notify;

    QString pendingJid;

signals:
    void notificationActivate(const QString &jid);
    void doShowUIAsync(const QStringList &params);

};

#endif // DBUSMAIN_H
