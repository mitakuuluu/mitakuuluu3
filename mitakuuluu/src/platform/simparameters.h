#ifndef SIMPARAMETERS_H
#define SIMPARAMETERS_H

#include <QObject>

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusPendingCallWatcher>
#include <QDBusPendingReply>

#include <QVariantMap>

class SimParameters : public QObject
{
    Q_OBJECT
public:
    explicit SimParameters(QObject *parent = 0);
    static SimParameters *GetInstance(QObject *parent = 0);

    Q_PROPERTY(QVariantMap parameters READ parameters NOTIFY parametersChanged)
    QVariantMap parameters() const;

private slots:
    void onSimParameters(QDBusPendingCallWatcher *call);

private:
    QVariantMap m_parameters;

signals:
    void parametersChanged();

};

#endif // SIMPARAMETERS_H
