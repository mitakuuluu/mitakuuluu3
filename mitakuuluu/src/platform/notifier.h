#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <QObject>
#include <QHash>

#include "src/platform/notification.h"

class Notifier : public QObject
{
    Q_OBJECT
public:
    explicit Notifier(QObject *parent = 0);
    static Notifier *GetInstance(QObject *parent = 0);

    void notify(const QVariantMap &message);

public slots:
    void removeNotifications(const QString &jid);

private:
    QHash<QString, QList<uint> > _publishedNotifications;

signals:

};

#endif // NOTIFIER_H
