#ifndef NATIVEWINDOWHELPER_H
#define NATIVEWINDOWHELPER_H

#include <QObject>
#include <QQuickView>
#include <QGuiApplication>
#include <QRect>
#include <QRegion>

#include "src/platform/qpa_native.h"

class NativeWindowHelper : public QObject
{
    Q_OBJECT
public:
    explicit NativeWindowHelper(QQuickView *parent = 0);

public slots:
    void setTouchRegion(const QRect &rect);
    void setWindowCategory(const QString &category);

    void setReady(bool isReady = true);
    void closeWindow();

private:
    void setWindowProperty(const QString &name, const QVariant &value);

    QQuickView *view;
    QPlatformNativeInterface *native;

    bool ready;
    QRect pendingRegion;
};

#endif // NATIVEWINDOWHELPER_H
