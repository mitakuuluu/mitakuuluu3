#ifndef SMSLISTENER_H
#define SMSLISTENER_H

#include <QObject>

class SMSListener : public QObject
{
    Q_OBJECT
public:
    explicit SMSListener(QObject *parent = 0);

signals:
    void codeReceived(const QString &code);

private slots:
    void onMessageAdded(const QString &text, const QVariantList &);
};

#endif // SMSLISTENER_H
