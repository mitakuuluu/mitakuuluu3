#include "smslistener.h"

#include <QDebug>
#include <QDBusConnection>

SMSListener::SMSListener(QObject *parent) :
    QObject(parent)
{
    QDBusConnection::systemBus().connect("",
                                         "",
                                         "org.ofono.MessageManager",
                                         "IncomingMessage",
                                         this,
                                         SLOT(onMessageAdded(QString,QVariantList)));
}

void SMSListener::onMessageAdded(const QString &text, const QVariantList &)
{
    QString cleanText = text;
    cleanText.replace(" ", "");
    cleanText.replace("-", "");
    if (cleanText.startsWith("WhatsAppcode"))
    {
        QString code = cleanText.mid(12, 6);
        qDebug() << "Received WhatsApp code:" << code;
        qDebug() << cleanText;

        Q_EMIT codeReceived(code);
    }
}
