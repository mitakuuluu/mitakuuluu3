#include "simparameters.h"

#include <QDebug>

SimParameters::SimParameters(QObject *parent) :
    QObject(parent)
{
    QDBusInterface *ofono = new QDBusInterface("org.ofono", "/ril_0", "org.ofono.SimManager", QDBusConnection::systemBus(), this);
    QDBusPendingCall async = ofono->asyncCall("GetProperties");
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(async, this);
    if (watcher->isFinished()) {
       onSimParameters(watcher);
    }
    else {
        QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),this, SLOT(onSimParameters(QDBusPendingCallWatcher*)));
    }
}

SimParameters *SimParameters::GetInstance(QObject *parent)
{
    static SimParameters* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new SimParameters(parent);
    }
    return lsSingleton;
}

QVariantMap SimParameters::parameters() const
{
    return m_parameters;
}

void SimParameters::onSimParameters(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QVariantMap> reply = *call;
    if (reply.isError()) {
        qWarning() << "error:" << reply.error().name() << reply.error().message();
    } else {
        m_parameters = reply.value();
        Q_EMIT parametersChanged();
    }

    call->deleteLater();
}
