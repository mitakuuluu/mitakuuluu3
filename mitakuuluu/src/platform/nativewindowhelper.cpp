#include "nativewindowhelper.h"

#include <QDebug>

NativeWindowHelper::NativeWindowHelper(QQuickView *parent) :
    QObject(parent),
    view(parent),
    ready(false)
{
    native = QGuiApplication::platformNativeInterface();
}

void NativeWindowHelper::setWindowProperty(const QString &name, const QVariant &value)
{
    if (ready && view) {
        native->setWindowProperty(view->handle(), name, value);
    }
}

void NativeWindowHelper::setTouchRegion(const QRect &rect)
{
    if (rect.width() > 0 && rect.height() > 0) {
        if (!ready) {
            pendingRegion = rect;
        }
        setWindowProperty("MOUSE_REGION", QRegion(rect));
    }
}

void NativeWindowHelper::setWindowCategory(const QString &category)
{
    setWindowProperty("CATEGORY", category);
}

void NativeWindowHelper::setReady(bool isReady)
{
    ready = isReady;
    if (ready && pendingRegion.width() > 0 && pendingRegion.height() > 0) {
        setWindowProperty("MOUSE_REGION", QRegion(pendingRegion));
    }
}

void NativeWindowHelper::closeWindow()
{
    if (view) {
        view->close();
        view->destroy();
        view->deleteLater();
    }
}
