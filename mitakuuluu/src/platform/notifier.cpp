#include "notifier.h"

#include "src/models/contactsbasemodel.h"
#include "src/constants.h"

Notifier::Notifier(QObject *parent) :
    QObject(parent)
{
}

Notifier *Notifier::GetInstance(QObject *parent)
{
    static Notifier* lsSingleton = NULL;
    if (!lsSingleton) {
        lsSingleton = new Notifier(parent);
    }
    return lsSingleton;
}

void Notifier::notify(const QVariantMap &message)
{
    QString jid = message["jid"].toString();
    QString msgid = message["msgid"].toString();
    QVariantMap model = ContactsBaseModel::GetInstance()->getModel(jid);
    QString avatar = model["phoneavatar"].toString();
    if (avatar.isEmpty()) {
        avatar = model["avatar"].toStringList().first();
    }
    avatar = avatar.replace("file://", "");
    qDebug() << "avatar:" << avatar;
    QString nickname = model["nickname"].toString();

    Notification notification;
    notification.setCategory("org.mitakuuluu3.private");
    notification.setIcon(avatar);
    notification.setSummary(message["data"].toString());
    notification.setBody(nickname);
    notification.setPreviewSummary(message["data"].toString());
    notification.setPreviewBody(nickname);
    notification.setRemoteDBusCallServiceName(DBUS_SERVICE);
    notification.setRemoteDBusCallObjectPath(DBUS_PATH);
    notification.setRemoteDBusCallInterface(DBUS_INTERFACE);
    notification.setRemoteDBusCallMethodName("notificationCallback");
    notification.setRemoteDBusCallArguments(QVariantList() << jid << msgid);
    notification.publish();
    uint notificationId = notification.replacesId();
    QList<uint> notifications;
    if (_publishedNotifications.contains(jid)) {
        notifications = _publishedNotifications[jid];
    }
    notifications.append(notificationId);
    _publishedNotifications[jid] = notifications;
}

void Notifier::removeNotifications(const QString &jid)
{
    if (_publishedNotifications.contains(jid)) {
        QList<uint> notifications = _publishedNotifications[jid];
        foreach (uint notificationId, notifications) {
            Notification::closeNotification(notificationId);
        }
        _publishedNotifications.remove(jid);
    }
}
